<?php

abstract class TestGenericController extends Zend_Test_PHPUnit_ControllerTestCase
{
    /**
     * @var SetupDb
     */
    protected $_connection;
    protected $_databaseTester;

    /**
     * @var Account_Model_Domain
     */
    public $account;

    /**
     * @var User_Model_Domain
     */
    public $user;

    /**
     * @var Space_Model_Domain
     */
    public $space;

    /**
     * @var Category_Model_Domain
     */
    public $category;
    /**
     * @var Item_Model_Domain
     */
    public $item;

    /**
     * @var ObjectCreator_Space_Kit
     */
    public $spaceKit;

    /**
     * @var ObjectCreator_Category_Kit
     */
    public $categoryKit;

    /**
     * @var ObjectCreator_Item_Kit
     */
    public $itemKit;

    public function setUp()
    {
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');

        $this->_connection = DbConnection::getInstance()
            ->setBootstrap($this->bootstrap)->getConnection()->getConnection();

//        $this->_connection->getConnection()->beginTransaction();

        $this->itemKit = new ObjectCreator_Item_Kit();

        $this->spaceKit = new ObjectCreator_Space_Kit();

        $this->categoryKit = new ObjectCreator_Category_Kit();

        parent::setUp();
    }


    public function setupAccount($active = true, $hash = NULL)
    {
        $accountKit = new ObjectCreator_Account_Kit();
        $this->account = $accountKit->create();

        $userKit = new ObjectCreator_User_Kit();
        $this->user = $userKit->create($this->account, $active, $hash);

        $space = $this->spaceKit->createDomain(
            new ObjectCreator_Space_Decorated_Default(
                $this->spaceDefaultDecorators(), TRUE
            )
        );

        $this->spaceKit = new ObjectCreator_Space_Kit();
        $this->space = $this->spaceKit->saveDomain($space);

        $category = $this->categoryKit->createDomain(
            new ObjectCreator_Category_Decorated_Default(
                $this->categoryDefaultDecorators(), TRUE
            )
        );

        $this->categoryKit = new ObjectCreator_Category_Kit();
        $this->category = $this->categoryKit->saveDomain($category);
    }

    public function tearDown()
    {
        parent::tearDown();
//        $this->_connection->getConnection()->rollBack();
    }

    protected function _setupFixture($path)
    {
        $this->_databaseTester = new Zend_Test_PHPUnit_Db_SimpleTester($this->_connection);
        $fixturePath = DB_FIXTURES_PATH . DIRECTORY_SEPARATOR . $path;
        $databaseFixture = new PHPUnit_Extensions_Database_DataSet_FlatXmlDataSet($fixturePath);
        $this->_databaseTester->setSetUpOperation(
            new PHPUnit_Extensions_Database_Operation_Composite(
                array(
                    new Zend_Test_PHPUnit_Db_Operation_Insert()
                )
            )
        );
        $this->_databaseTester->setUpDatabase($databaseFixture);
    }

    protected function _redirectToResultURL()
    {
        $headers = $this->getResponse()->getHeaders();
        $redirectUrl = $headers[0]['value'];

        $this->resetRequest()
             ->resetResponse();

        $this->dispatch($redirectUrl);
    }

    public function _createItem()
    {
        return $this->itemKit->saveDomain(
                $this->itemKit->createDomain($this->defaultDecorators())
        );
    }

    public function _createSpace()
    {
        return $this->spaceKit->saveDomain(
            $this->spaceKit->createDomain($this->spaceDefaultDecorators())
        );
    }
    public function _createCategory()
    {
        return $this->categoryKit->saveDomain(
            $this->categoryKit->createDomain($this->categoryDefaultDecorators())
        );
    }

    public function defaultDecorators()
    {
        return $this->itemKit->initDomain($this->account->getId(), $this->space->getId(), $this->category->getId());
    }

    public function spaceDefaultDecorators()
    {
        return $this->spaceKit->initDomain($this->account->getId());
    }
    public function categoryDefaultDecorators()
    {
        return $this->categoryKit->initDomain($this->account->getId(),  $this->space->getId());
    }


}