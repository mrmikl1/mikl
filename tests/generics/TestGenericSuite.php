<?php
class TestGenericSuite extends PHPUnit_Framework_TestSuite
{
    protected static function _loadControllerSuites(
        PHPUnit_Framework_TestSuite $suite,
        $moduleName,
        $controllerName
    ) {
        if (strtolower($moduleName) == 'v1') {
            $modulePath = 'api/' . $moduleName . '/';
        } else {
            $modulePath = 'modules/' . $moduleName . '/';
        }
        foreach (glob("application/" . $modulePath . "controllers/" . $controllerName . "/*.php") as $filename)
        {
            require_once $filename;

            preg_match('/\/([a-zA-Z0-9_-]*)\.php/', $filename, $match);
            $className = $moduleName . '_' . $controllerName . '_' . $match[1];
            $suite->addTestSuite($className);
        }

        return $suite;
    }
}