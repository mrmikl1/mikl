<?php

class ObjectCreator_User_Kit
{
    public function create(Account_Model_Domain $account, $active = TRUE, $forgotPasswordHash = NULL)
    {
        $postfix = uniqid();
        $user = new User_Model_Domain();
        $user->setAccountId($account->getId());
        $user->setActivationHash('Activation Hash ' . $postfix);
        $user->setActive($active);
        $user->setEmail('email' . $postfix . '@ukr.net');
        $user->setFirstName('First Name ' . $postfix);
        $user->setLastName('Last Name ' . $postfix);
        $user->setPassword(md5('password'));
        $user->setForgotPasswordHash($forgotPasswordHash);

        $userMapper = new User_Model_Mapper();
        $userMapper->save($user);
        $userMapper->save($user); // to setEdited
        return $user;
    }
}