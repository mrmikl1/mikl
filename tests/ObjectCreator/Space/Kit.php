<?php

class ObjectCreator_Space_Kit
{
    protected $_position = 0;

    public function saveDomain(Space_Model_Domain $space)
    {
        $spaceMapper = new Space_Model_Mapper();
        $spaceMapper->save($space);
        $spaceMapper->save($space); // to setEdited

        return $space;
    }

    public function initDomain($accountId)
    {
        return new ObjectCreator_Space_Decorated_Account(
            new ObjectCreator_Space_Object(), $accountId
        );
    }

    public function createDomain($decorator)
    {
        return $decorator->create(new Space_Model_Domain);
    }

    public function add($numberOfSpaces, $decorator)
    {
        for ($i = 1; $i <= $numberOfSpaces; $i++) {
            $space = $this->createDomain($decorator);
            $spaces[] = $this->saveDomain($space);
        }
        return new Application_Collection_Generic($spaces);
    }

    /**
     *
     * @param string $accountName
     * @param string $language
     * @return boolean|\Application_Collection_Generic
     */
    public function getCollection($accountName, $language = 'en')
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);

        $spaceMapper = new Space_Model_Mapper();
        $spaceCollection = $spaceMapper->getCollection($account->getId(), $language);
        return $spaceCollection;
    }

    public function find($id, Account_Model_Domain $account, $language = 'en')
    {
        $spaceMapper = new Space_Model_Mapper();
        $space = $spaceMapper->find($id, $account, $language);
        return $space;
    }



}