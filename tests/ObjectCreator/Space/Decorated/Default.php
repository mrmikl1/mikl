<?php

class ObjectCreator_Space_Decorated_Default extends ObjectCreator_Space_Decorated_Abstract
{
    protected $_default;
    public function __construct($space, $default)
    {
        parent::__construct($space);
        $this->_default = $default;
    }
    public function create(Space_Model_Domain $space)
    {
        $space = $this->_object->create($space);
        $space->setDefault($this->_default);
        return $space;
    }
}