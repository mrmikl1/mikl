<?php

abstract class ObjectCreator_Space_Decorated_Abstract extends ObjectCreator_Space_Object
{
    protected $_object;

    public function __construct($space)
    {
        $this->_object = $space;
    }

    public function create($space) {
        return $this->_object->create($space);
    }
}