<?php

class ObjectCreator_Space_Decorated_Language extends ObjectCreator_Space_Decorated_Abstract
{
    protected $_language;

    public function __construct($space, $language)
    {
        parent::__construct($space);
        $this->_language = $language;
    }
    
    public function create(Space_Model_Domain $space)
    {
        $space = $this->_object->create($space);
        $space->setLanguage($this->_language);
        return $space;
    }
}