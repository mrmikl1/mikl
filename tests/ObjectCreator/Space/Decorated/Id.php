<?php

class ObjectCreator_Space_Decorated_Id extends ObjectCreator_Space_Decorated_Abstract
{
    protected $_id;
    
    public function __construct($space, $id)
    {
        parent::__construct($space);
        $this->_id = $id;
    }

    public function create(Space_Model_Domain $space)
    {
        $space = $this->_object->create($space);
        $space->setId($this->_id);
        return $space;
    }
}