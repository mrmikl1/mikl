<?php

class ObjectCreator_Space_Object
{
    protected $_position = 0;

    public function create($space)
    {
        $postfix = uniqid();

        $space->setName('name ' . $postfix);
        $space->setDescription('description' . $postfix);
        $space->setPosition($this->_position++);
        $space->setLanguage('en');
        $space->setDefault(FALSE);

        return $space;
    }
}