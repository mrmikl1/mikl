<?php

class ObjectCreator_Account_Kit
{
    public function create()
    {
        $postfix = uniqid();
        $account = new Account_Model_Domain();
        $account->setName('Account-' . $postfix);

        $accountMapper = new Account_Model_Mapper();
        $accountMapper->save($account);

        return $account;
    }
}
