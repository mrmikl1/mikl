<?php

class ObjectCreator_Item_Kit
{
    protected $_decorateItem;

    public function saveDomain(Item_Model_Domain $item)
    {
        $itemMapper = new Item_Model_Mapper();
        $itemMapper->save($item);
        $itemMapper->save($item); // to setEdited
        $item->getTranslations();
        return $item;
    }

    public function initDomain($accountId, $spaceId, $categoryId)
    {
        return new ObjectCreator_Item_Decorated_Publishing(
                    new ObjectCreator_Item_Decorated_Status(
                        new ObjectCreator_Item_Decorated_Category(
                            new ObjectCreator_Item_Decorated_Space(
                                new ObjectCreator_Item_Decorated_Account(
                                    new ObjectCreator_Item_Object(), $accountId
                                ), $spaceId
                            ), $categoryId
                        ),Item_Model_Domain::STATUS_UNANSWERED
                    ),Item_Model_Domain::UNPUBLISHED
            );
    }

    public function createDomain($decorator)
    {
        return $decorator->create(new Item_Model_Domain);
    }

    public function add($numberOfItems, $decorator)
    {
        $items = array();
        for ($i = 1; $i <= $numberOfItems; $i++) {
            $item = $this->createDomain($decorator);
            $items[] = $this->saveDomain($item);
        }
        return new Application_Collection_Generic($items);
    }

    public function getCollection($accountName, $categoryId, $language= 'en')
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);
        $itemMapper = new Item_Model_Mapper();
        $itemCollection = $itemMapper->getCollection($account->getId(), NULL, $categoryId, $language, Item_Model_Domain::STATUS_ANSWERED, Item_Model_Domain::PUBLISHED);
        return $itemCollection;
    }
}