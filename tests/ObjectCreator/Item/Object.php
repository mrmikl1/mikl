<?php

class ObjectCreator_Item_Object
{
    protected $_position = 0;

    public function create($item)
    {
        $postfix = uniqid();

        $item->setTitle('title ' . $postfix);
        $item->setContent('content' . $postfix);
        $item->setPosition($this->_position++);
        $item->setLanguage('en');

        return $item;
    }
}