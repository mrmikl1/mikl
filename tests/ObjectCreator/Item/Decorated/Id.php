<?php

class ObjectCreator_Item_Decorated_Id extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_id;
    public function __construct($item, $id)
    {
        parent::__construct($item);
        $this->_id = $id;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setId($this->_id);
        return $item;
    }
}