<?php

class ObjectCreator_Item_Decorated_Email extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_email;
    public function __construct($item, $email)
    {
        parent::__construct($item);
        $this->_email = $email;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setEmail($this->_email);
        return $item;
    }
}