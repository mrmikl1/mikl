<?php

class ObjectCreator_Item_Decorated_Publishing extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_publishing;
    public function __construct($item, $publishing)
    {
        parent::__construct($item);
        $this->_publishing = $publishing;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setPublished($this->_publishing);
        return $item;
    }
}