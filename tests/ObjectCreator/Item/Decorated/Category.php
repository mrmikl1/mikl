<?php

class ObjectCreator_Item_Decorated_Category extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_categoryId;
    public function __construct($item, $categoryId)
    {
        parent::__construct($item);
        $this->_categoryId = $categoryId;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setCategoryId($this->_categoryId);
        return $item;
    }
}