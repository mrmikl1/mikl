<?php

class ObjectCreator_Item_Decorated_FullName extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_name;
    public function __construct($item, $name)
    {
        parent::__construct($item);
        $this->_name = $name;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setFullName($this->_name);
        return $item;
    }
}