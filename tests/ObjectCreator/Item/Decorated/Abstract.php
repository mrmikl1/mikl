<?php

abstract class ObjectCreator_Item_Decorated_Abstract extends ObjectCreator_Item_Object
{
    protected $_object;

    public function __construct($item)
    {
        $this->_object = $item;
    }

    public function create($item) {
        return $this->_object->create($item);
    }
}