<?php

class ObjectCreator_Item_Decorated_Language extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_language;
    public function __construct($item, $language)
    {
        parent::__construct($item);
        $this->_language = $language;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setLanguage($this->_language);
        return $item;
    }
}