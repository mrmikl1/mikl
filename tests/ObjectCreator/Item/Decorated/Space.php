<?php

class ObjectCreator_Item_Decorated_Space extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_spaceId;
    public function __construct($item, $spaceId)
    {
        parent::__construct($item);
        $this->_spaceId = $spaceId;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setSpaceId($this->_spaceId);
        return $item;
    }
}