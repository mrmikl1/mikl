<?php

class ObjectCreator_Item_Decorated_Account extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_accountId;
    public function __construct($item, $accountId)
    {
        parent::__construct($item);
        $this->_accountId = $accountId;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setAccountId($this->_accountId);
        return $item;
    }
}