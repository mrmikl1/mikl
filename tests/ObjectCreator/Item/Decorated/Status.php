<?php

class ObjectCreator_Item_Decorated_Status extends ObjectCreator_Item_Decorated_Abstract
{
    protected $_status;
    public function __construct($item, $status)
    {
        parent::__construct($item);
        $this->_status = $status;
    }
    public function create(Item_Model_Domain $item)
    {
        $item = $this->_object->create($item);
        $item->setStatus($this->_status);
        return $item;
    }
}