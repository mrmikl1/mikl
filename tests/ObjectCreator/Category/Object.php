<?php

class ObjectCreator_Category_Object
{
    protected $_position = 0;

    public function create($category)
    {
        $postfix = uniqid();

        $category->setName('name ' . $postfix);
        $category->setDescription('description' . $postfix);
        $category->setPosition($this->_position++);
        $category->setLanguage('en');
        $category->setDefault(FALSE);

        return $category;
    }
}