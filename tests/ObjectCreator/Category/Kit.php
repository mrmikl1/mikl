<?php

class ObjectCreator_Category_Kit
{
    protected $_position = 0;
    public function saveDomain(Category_Model_Domain $category)
    {
        $postfix = uniqid();

        $categoryMapper = new Category_Model_Mapper();
        $categoryMapper->save($category);
        $categoryMapper->save($category); // to setEdited

        return $category;
    }

    public function initDomain($accountId, $spaceId)
    {
        return new ObjectCreator_Category_Decorated_Space(
                    new ObjectCreator_Category_Decorated_Account(
            new ObjectCreator_Category_Object(), $accountId), $spaceId
        );
    }

    public function createDomain($decorator)
    {
        return $decorator->create(new Category_Model_Domain);
    }

    public function add($numberOfCategories, $decorator)
    {
        for ($i = 1; $i <= $numberOfCategories; $i++) {
            $category = $this->createDomain($decorator);
            $categories[] = $this->saveDomain($category);
        }
        return new Application_Collection_Generic($categories);
    }

    public function getCollection($accountName, Space_Model_Domain $space, $language = 'en')
    {
        $accountMapper = new Account_Model_Mapper();
        $categoryMapper = new Category_Model_Mapper();
        $categoryCollection = $categoryMapper->getCollection($accountMapper->findByName($accountName), $space, $language);
        return $categoryCollection;
    }
    public function find($id, Account_Model_Domain $account, $language = 'en')
    {
        $categoryMapper = new category_Model_Mapper();
        $category = $categoryMapper->find($id, $account, $language);
        return $category;
    }
}