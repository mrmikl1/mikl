<?php

class ObjectCreator_Category_Decorated_Space extends ObjectCreator_Category_Decorated_Abstract
{
    protected $_spaceId;
    public function __construct($category, $spaceId)
    {
        parent::__construct($category);
        $this->_spaceId = $spaceId;
    }
    public function create(Category_Model_Domain $category)
    {
        $category = $this->_object->create($category);
        $category->setSpaceId($this->_spaceId);
        return $category;
    }
}