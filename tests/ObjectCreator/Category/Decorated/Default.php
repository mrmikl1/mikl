<?php

class ObjectCreator_Category_Decorated_Default extends ObjectCreator_Category_Decorated_Abstract
{
    protected $_default = false;
    public function __construct($category, $default)
    {
        parent::__construct($category);
        $this->_default = $default;
    }
    public function create(Category_Model_Domain $category)
    {
        $category = $this->_object->create($category);
        $category->setDefault($this->_default);
        return $category;
    }
}