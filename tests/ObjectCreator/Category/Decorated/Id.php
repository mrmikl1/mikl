<?php

class ObjectCreator_Category_Decorated_Id extends ObjectCreator_Category_Decorated_Abstract
{
    protected $_id;

    public function __construct($category, $id)
    {
        parent::__construct($category);
        $this->_id = $id;
    }

    public function create(Category_Model_Domain $category)
    {
        $category = $this->_object->create($category);
        $category->setId($this->_id);
        return $category;
    }
}