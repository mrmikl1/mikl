<?php

class ObjectCreator_Category_Decorated_Language extends ObjectCreator_Category_Decorated_Abstract
{
    protected $_language;

    public function __construct($category, $language)
    {
        parent::__construct($category);
        $this->_language = $language;
    }

    public function create(Category_Model_Domain $category)
    {
        $category = $this->_object->create($category);
        $category->setLanguage($this->_language);
        return $category;
    }
}