<?php

class ObjectCreator_Category_Decorated_Account extends ObjectCreator_Category_Decorated_Abstract
{
    protected $_accountId;
    public function __construct($category, $accountId)
    {
        parent::__construct($category);
        $this->_accountId = (int)$accountId;
    }
    public function create(Category_Model_Domain $category)
    {
        $category = $this->_object->create($category);
        $category->setAccountId($this->_accountId);
        return $category;
    }
}