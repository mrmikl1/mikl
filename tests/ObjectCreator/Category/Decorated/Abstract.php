<?php

abstract class ObjectCreator_Category_Decorated_Abstract extends ObjectCreator_Category_Object
{
    protected $_object;

    public function __construct($category)
    {
        $this->_object = $category;
    }

    public function create($category) {
        return $this->_object->create($category);
    }
}