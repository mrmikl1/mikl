<?php
/**
 * Description of FullTest
 *
 * @author anton.vinokurov
 */
class FullTest extends TestGenericSuite
{
    protected static function _getClassNameCommon(
        $filename
    ) {
        $dirs = self::_parseDirs(explode('/', $filename));

        $classPath = '';
        foreach ($dirs as $dir) {
            $classPath .= $dir . '_';
        }
        return str_replace('.php_', '', $classPath);
    }

    protected static function _getClassNameController(
        $filename
    ) {
        $dirs = self::_parseDirs(explode('/', $filename));

        return str_replace(array('Suite.php', '.php'), '', $dirs[count($dirs) - 1]);
    }

    protected static function _getClassNameLibrary(
        $filename
    ) {
        $dirs = self::_parseDirs(explode('/', $filename));

        $classPath = '';
        foreach ($dirs as $dir) {
            $classPath .= $dir . '_';
        }
        $classPath = str_replace('Application_Libraries_', '', $classPath);
        return str_replace('.php_', '', $classPath);
    }

    protected static function _parseDirs($dirs)
    {
        $mapFrom = array(
            'Collections',
            'Models',
            'Services',
            'Controllers'
        );
        $mapTo = array(
            'Collection',
            'Model',
            'Service',
            'Controller'
        );
        foreach ($dirs as $key => $dir) {
            $dirs[$key] = str_replace(
                $mapFrom,
                $mapTo,
                ucfirst($dir)
            );
        }
        return $dirs;
    }

    protected static function _includeFiles(
        PHPUnit_Framework_TestSuite $suite,
        $path,
        $callback = '_getClassNameCommon',
        $nesting = 10
    ) {
        // files
        foreach (glob($path . "*.php") as $filename)
        {
            if (false !== strpos($filename, 'AccountRowMapper')) {
                continue;
            }
            require_once $filename;

            $className = forward_static_call (
                array('FullTest', $callback),
                $filename
            );

            try {
                $suite->addTestSuite($className);
            } catch (Exception $e) {
                print_r('failed to load class: ' . $className);
                exit;
            }
        }

        if ($nesting > 1) {
            // folders
            foreach (glob($path . "*", GLOB_ONLYDIR) as $dir)
            {
                self::_includeFiles(
                    $suite,
                    $dir . '/',
                    $callback,
                    $nesting - 1
                );
            }
        }

        return $suite;
    }

    public static function suite()
    {
        Zend_Session::start();

        $suite = new PHPUnit_Framework_TestSuite('PHPUnit Framework');

//        $suite = self::_includeFiles(
//            $suite,
//            'application/collections/'
//        );

//        $suite = self::_includeFiles(
//            $suite,
//            'application/models/'
//        );
//
//        $suite = self::_includeFiles(
//            $suite,
//            'application/services/'
//        );

        $suite = self::_includeFiles(
            $suite,
            'application/controllers/',
            '_getClassNameController',
            1
        );

//        $suite = self::_includeFiles(
//            $suite,
//            'application/rpc/'
//        );
//
//        $suite = self::_includeFiles(
//            $suite,
//            'application/libraries/',
//            '_getClassNameLibrary'
//        );

        return $suite;
    }
}