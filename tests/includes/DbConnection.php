<?php

class DbConnection
{
    protected static $_instance;

    protected $_connection;

    protected $_bootstrap;

    private function __construct() {}

    private function __clone() {}

    private function __wakeup() {}

    /**
     * @return DbConnection
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function getConnection($accountIdAutoIncrement = 0)
    {
        if (empty($this->_connection)) {
            $this->_connection = new SetupDb($this->getBootstrap(), $accountIdAutoIncrement);
        }

        // black magic to remain previous connection
        Zend_Db_Table::setDefaultAdapter($this->_connection->getConnection()->getConnection());

        return $this->_connection;
    }

    public function getBootstrap()
    {
        if (null === $this->_bootstrap) {
            throw new Exception('Call setBootstrap before getBootstrap');
        }
        return $this->_bootstrap;
    }

    public function setBootstrap($bootstrap)
    {
        $this->_bootstrap = $bootstrap;
        return $this;
    }


}

