<?php

/**
 * Description of initDb
 *
 * @author sergey.ostapenko
 */
class SetupDb
{
    protected $_application;
    protected $_connection;
    protected $_databaseTester;
    protected $_dbName;

    public function __construct(Zend_Application $application)
    {
        $this->setApplication($application);

        $this->_debugInfo("INITIALIZATION");

        $bootstrap = $this->getApplication()->getBootstrap();
        $resource  = $bootstrap->getPluginResource('db');
        $db        = $resource->getDbAdapter();

        $this->initDb($db);

        Zend_Db_Table_Abstract::setDefaultAdapter($db);

        $this->setConnection(
            new Zend_Test_PHPUnit_Db_Connection($db, 'test')
        );

        $this->setDatabaseTester(
            new Zend_Test_PHPUnit_Db_SimpleTester($this->getConnection())
        );

        $this->setupStructure();

    }

    public function __destruct()
    {
        $bootstrap = $this->getApplication()->getBootstrap();
        $config = new Zend_Config($bootstrap->getOptions(), true);

        if ((int) $config->resources->db->testing->AutoDrop) {
            $this->_query(
                $this->getConnection()->getConnection(),
                "DROP DATABASE " . $this->getDbName()
            );

            $this->_debugInfo("DB '{$this->getDbName()}' was DROPPED");
        }

        $this->_debugInfo("DEINITIALIZATION");
    }

    public function getApplication()
    {
        return $this->_application;
    }

    public function setApplication(Zend_Application $application)
    {
        $this->_application = $application;
        return $this;
    }

    /**
     * @return Zend_Test_PHPUnit_Db_Connection
     */
    public function getConnection()
    {
        return $this->_connection;
    }

    public function setConnection($connection)
    {
        $this->_connection = $connection;
        return $this;
    }

    public function getDatabaseTester()
    {
        return $this->_databaseTester;
    }

    public function setDatabaseTester($databaseTester)
    {
        $this->_databaseTester = $databaseTester;
        return $this;
    }

    public function setupStructure()
    {
        $explodedAdapterClass = explode(
            '_',
            get_class($this->getConnection()->getConnection())
        );
        $adapter = array_pop(
            $explodedAdapterClass
        );

        if (!defined('DB_STRUCTURE_PATH')) {
            throw new SetupDbException(
                "Can't apply DB structure: DB_STRUCTURE_PATH constant is not defined."
            );
        }

        $structureSqlPath = DB_STRUCTURE_PATH
                          . DIRECTORY_SEPARATOR
                          . $adapter
                          . DIRECTORY_SEPARATOR
                          . "structure.sql";

        if (!file_exists($structureSqlPath)) {
            throw new SetupDbException(
                "Can't apply DB structure: file '$structureSqlPath' not found."
            );
        }

        $command = '';
        try {
            $file = file($structureSqlPath);

            foreach ($file as $line) {
                $line = preg_replace('/(--.*)/i', '', trim($line));
                $command .= $line . "\n";

                if (strlen($command) > 1 && $command[strlen($command) - 2] == ';') {
                    $this->_query(
                        $this->getConnection()->getConnection(),
                        $command
                    );
                    $command = '';
                }
            }

            if (!empty($command)) {
                $this->_query($this->getConnection()->getConnection(), $command);
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage() . ' in query: ' . $command);
        }
    }

    public function initDb(Zend_Db_Adapter_Mysqli $db)
    {
        $bootstrap = $this->getApplication()->getBootstrap();
        $config = new Zend_Config($bootstrap->getOptions(), true);

        $dbPrefix     = $config->resources->db->testing->dbPrefix;
        $createDbName = $dbPrefix . uniqid();

        if (empty($dbPrefix)) {
            throw new SetupDbException("Database prefix can't be empty!");
        }

        $dbList = $this->_query(
            $db,
            "SHOW DATABASES"
        );

        while (null !== $row = $dbList->fetch_assoc()) {
            if (substr($row['Database'], 0, strlen(strtolower($dbPrefix))) !== strtolower($dbPrefix)) {
                continue;
            }

            $dbTimeRow = $this->_query(
                $db,
                "SELECT
                    MIN(create_time) AS `time`
                FROM
                    INFORMATION_SCHEMA.TABLES
                WHERE
                    table_schema = '{$row['Database']}'"
            )->fetch_assoc();

            if (empty($dbTimeRow['time'])) {
                // if database do not have tables
                // create new table
                // next time this will allow to get db creation time
                try {
                    $this->_query(
                        $db,
                        "USE {$row['Database']}"
                    );
                    $res = $this->_query(
                        $db,
                        "SHOW TABLES LIKE 'tmp_for_database_cleanup';"
                    )->fetch_assoc();

                    if (!count($res)) {
                        $this->_query(
                            $db,
                            'CREATE TABLE `tmp_for_database_cleanup` (
                                `id` INT(10) NULL
                            )'
                        );
                    }
                } catch (Exception $e) {

                }

                continue;
            }

            $date = strtotime($dbTimeRow['time']);
            if ($date < strtotime($config->resources->db->testing->deleteOldestThen)) {
                try {
                    $this->_query(
                        $db,
                        "DROP DATABASE "
                            . $db->quoteIdentifier($row['Database'])
                    );

                    $this->_debugInfo(
                        "'{$row['Database']}' created at {$dbTimeRow['time']} was DELETED"
                    );
                } catch (Exception $e) {
                    // probably db was droped by another thread
                }
            }
        }

        $this->_query(
            $db,
            "CREATE DATABASE "
                . $db->quoteIdentifier($createDbName)
        );

        $this->_debugInfo("DB '{$createDbName}' was CREATED");

        $this->_query($db, "USE " . $db->quoteIdentifier($createDbName));

        $this->_setDbName($createDbName);
    }

    private function _query(Zend_Db_Adapter_Mysqli $db, $query)
    {
        $result = $db->getConnection()->query($query);

        if ($db->getConnection()->error) {
            throw new SetupDbException($db->getConnection()->error);
        }

        return $result;
    }

    private function _debugInfo($text)
    {
        $bootstrap = $this->getApplication()->getBootstrap();
        $config = new Zend_Config($bootstrap->getOptions(), true);

        if ((int) $config->resources->db->testing->dbDebugInfo) {
            echo "{$text}\r\n";
        }
    }

    public function getDbName()
    {
        return $this->_dbName;
    }

    protected function _setDbName($dbName)
    {
        $this->_dbName = $dbName;

        Zend_Registry::set('SolrCorePrefix', $dbName . '-');
        return $this;
    }
}