<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

// Define path to DB structures directory
defined('DB_STRUCTURE_PATH')
    || define('DB_STRUCTURE_PATH', realpath(dirname(__FILE__) . '/database'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
// Define path to DB structures directory
defined('DB_FIXTURES_PATH')
    || define('DB_FIXTURES_PATH', realpath(dirname(__FILE__) . '/fixtures'));
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->registerNamespace('ObjectCreator_');
Zend_Loader_Autoloader::getInstance();
$translate = new Zend_Translate(
    array(
        'adapter' => 'array',
        'content' => array(
            'test' => 'test'
        ),
        'locale'  => 'en'
    )
);
Zend_Registry::set('Zend_Translate', $translate);
require_once 'generics/TestGenericController.php';
require_once 'generics/TestGenericSuite.php';
require_once 'includes/DbConnection.php';
require_once 'includes/SetupDb.php';
require_once 'includes/SetupDbException.php';

