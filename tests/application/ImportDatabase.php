<?php

class Test extends TestGenericController
{
    private function initDB($name)
    {
        $parameters = array(
                    'host'     => 'localhost',
                    'username' => 'root',
                    'password' => '',
                    'dbname'   => $name
                   );
        try {
            $db = Zend_Db::factory('Pdo_Mysql', $parameters);
            $db->getConnection();
        } catch (Zend_Db_Adapter_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        }
        Zend_Registry::set('db_' . $name, $db);
        return Zend_Registry::get('db_' . $name);
    }

    public function test_import()
    {
        $dbDaddy = $this->initDB('godaddy');

//        $str = "abcdefghijklmnopqrstuwxyzABCDEFJHIJKLMNOPQRSTUVWXYZ1234567890";
//        for($i=0; $i<2; $i++) {
//            $name = substr(str_shuffle($str), rand(0, strlen($str)/4), rand(strlen($str)/4, strlen($str)/10));
//            $accountData = array(
//            'firstName'         => substr(str_shuffle($str), rand(0, strlen($str)/4), rand(strlen($str)/4, strlen($str)/10)),
//            'lastName'          => substr(str_shuffle($str), rand(0, strlen($str)/4), rand(strlen($str)/4, strlen($str)/10)),
//            'email'             => 'OikUj@grofaq.com',
//            'password'          => 'pass',
//            'passwordConfirm'   => 'pass',
//            'accountName'       => $name
//            );
//            $this->curlPost($accountData, "account");
//            $accountsNames[$i] = $name;
//        }
//        $godaddyData = $dbDaddy->fetchAll("select article_breadcrumbs as data from godaddy group by article_breadcrumbs order by count(*) desc");
//        $databaseData['spaces'] = array();
//        for($i=0; $i<count($godaddyData); $i++) {
//            $arrayData = unserialize($godaddyData[$i]['data']);
//            if(empty($arrayData)) {
//                continue;
//            }
//            $space = array_shift($arrayData);
//            $databaseData['categories'][] = array_pop($arrayData);
//            if(in_array($space, $databaseData['spaces'])) continue;
//            $databaseData['spaces'][] = $space;
//        }
//        $godaddyData = $dbDaddy->fetchAll("select article_title as title, article_html as content from godaddy");
//
//        $spaces = $this->array_random($databaseData['spaces'], rand(1,2));
//        for($i = 0; $i < count($accountsNames); $i++) {
//            foreach ($spaces as $spaceName) {
//                $data = array(
//                    'name' => $spaceName);
//                $result = $this->curlPost($data, "space", $accountsNames[$i]);
//                $spaceId = $result['id'];
//                $categories = $this->array_random($databaseData['categories'], rand(1,9));
//                foreach ($categories as $categoryName) {
//                    $data = array(
//                        'name' => $categoryName,
//                        'spaceId' => $spaceId);
//                    $result = $this->curlPost($data, "category", $accountsNames[$i]);
//                    $categoryId = $result['id'];
//                    $count = count($godaddyData)>20 ? 20 : count($godaddyData);
//                    $items = $this->array_random($godaddyData, rand(5,$count));
//                    foreach ($items as $item) {
//                        $data = array(
//                            'title'=>$item['title'],
//                            'content'=>$item['content'],
//                            'categoryId' => $categoryId,
//                            'spaceId' => $spaceId);
//                        $this->curlPost($data, "item", $accountsNames[$i]);
//                    }
//                }
//            }
//        }
    }

    public function curlPost($curlData, $entity, $host = NULL, $method = CURLOPT_POST)
    {
        $CI = curl_init();
        if($host!==NULL)
            $host = $host.".";
        curl_setopt($CI, CURLOPT_URL, "http://{$host}localhost/api/v1/" . $entity);
        curl_setopt($CI, CURLOPT_HEADER, true);
        $headers = array(
            "Content-type: application/json;charset=UTF-8",
            "Accept: application/json",
            "Accept-Language: en",
            );
        if($host !== NULL) {
            array_push($headers, "Authorization: Basic " . base64_encode("OikUj@grofaq.com:pass"));
        }
        curl_setopt($CI, CURLOPT_HTTPHEADER, $headers);
        if($method == CURLOPT_POST) {
            curl_setopt($CI, $method, (bool)count($curlData));
            curl_setopt($CI, CURLOPT_POSTFIELDS, json_encode($curlData));
        }
        curl_setopt($CI, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($CI);
        curl_close($CI);
        $result = explode("\n", $result);
        return json_decode($result[count($result)-1], true);
    }

    function array_random($arr, $num = 1) {
        shuffle($arr);

        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[] = $arr[$i];
        }
        return $r;
    }
}
