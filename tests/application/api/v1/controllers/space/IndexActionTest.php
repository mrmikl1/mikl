<?php

class V1_Space_IndexActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('Accept-Language', 'en');
    }

    public function testIndexSuccessfull()
    {
        $this->setupAccount();

        $numberOfSpaces = 2;
        $this->spaceKit->add($numberOfSpaces, $this->spaceDefaultDecorators());
        $spaceCollection = $this->spaceKit->getCollection($this->account->getName());
        $spaces = $spaceCollection->toArray();
        $body = array(
            'count'  => $spaceCollection->count(),
            'spaces' => $spaces
        );

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();

        $this->assertEquals(json_encode($body),$result);

        $this->assertResponseCode(200);
    }

    public function testIndexCorrectLanguageSuccessfull()
    {
        $this->setupAccount();

        $numberOfSpaces = 2;

        $this->spaceKit->add($numberOfSpaces, $this->spaceDefaultDecorators());
        $spacesCollection = $this->spaceKit->getCollection($this->account->getName());
        $spaceDefaultEn = $spacesCollection->offsetGet(0);
        $spaceFirstEn   = $spacesCollection->offsetGet(1);
        $spaceSecondEn  = $spacesCollection->offsetGet(2);

        $defaultSpaceRuDomain = $this->spaceKit->createDomain(
                new ObjectCreator_Space_Decorated_Id(
                    new ObjectCreator_Space_Decorated_Language(
                        $this->spaceDefaultDecorators(), 'ru'
                    ), $spaceDefaultEn->getId()
                )
            );

        $spaceDefaultRu = $this->spaceKit->saveDomain($defaultSpaceRuDomain);

        $spaceFirstRuDomain = $this->spaceKit->createDomain(
                new ObjectCreator_Space_Decorated_Id(
                    new ObjectCreator_Space_Decorated_Language(
                        $this->spaceDefaultDecorators(), 'ru'
                    ), $spaceFirstEn->getId()
                )
            );
        $spaceFirstRu = $this->spaceKit->saveDomain($spaceFirstRuDomain);


        $this->request->setHeader('Accept-Language', 'ru');
        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = json_decode($this->getResponse()->getBody(), true);

        $this->assertCount($spacesCollection->count(), $result['spaces']);

        $first  = $result['spaces'][0];
        $second = $result['spaces'][1];
        $third  = $result['spaces'][2];

        $this->assertResponseCode(200);

        $this->assertEquals($first['name'], $spaceDefaultRu->getName());
        $this->assertEquals($first['language'], 'ru');
        $this->assertTrue($first['translations']['en']);
        $this->assertTrue($first['translations']['ru']);

        $this->assertEquals($second['name'], $spaceFirstRu->getName());
        $this->assertEquals($second['language'], 'ru');
        $this->assertTrue($second['translations']['en']);
        $this->assertTrue($second['translations']['ru']);

        $this->assertEquals($third['name'], $spaceSecondEn->getName());
        $this->assertEquals($third['language'], 'en');
        $this->assertTrue($third['translations']['en']);
        $this->assertFalse($third['translations']['ru']);
    }

    public function testIndexFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }
}
