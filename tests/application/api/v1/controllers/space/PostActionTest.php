<?php

class V1_Space_PostActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('accept-language', 'en');
        $this->request->setMethod("POST");
    }

    public function testPostFail_IfInvalidTranslations()
    {
        $this->setupAccount();
        $this->_createSpace();

        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $postData = array(
             'id'          => $this->space->getId(),
             'name'        => '<script>alert("spaceName")</script>',
             'description' => 'descr',
             'position'    => 2,
             'language'    => 'inlan', //incorect language code
           );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $dbSpace = $this->spaceKit->find($this->space->getId(), $this->account, 'inlan');

        $this->assertFalse($dbSpace);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"language":{"noRecordFound":"No record matching \'inlan\' was found"}}');
    }

    public function testPostNewLanguage()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'id'          => $this->space->getId(),
            'name'        => '<script>alert("spaceName")</script>',
            'description' => 'descr',
            'position'    => 2,
            'language'    => 'ru'
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(201);
        $this->assertHeader('location');

        $result = $this->getResponse()->getBody();
        $space = json_decode($result);
        $dbSpace = $this->spaceKit->find($space->id, $this->account, 'ru');

        $this->assertInstanceOf("Space_Model_Domain", $dbSpace);
    }


    public function testPostSuccessfull()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'name'        => '<script>alert("spaceName")</script>',
            'description' => 'descr',
            'position'    => 2,
            'language'    => 'ru'
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(201);
        $this->assertHeader('location');

        $result = $this->getResponse()->getBody();
        $space = json_decode($result);
        $this->assertEquals(htmlentities($postData['name']), $space->name);
        $this->assertEquals($postData['description'], $space->description);
        $this->assertEquals($postData['position'], $space->position);
        $this->assertEquals($postData['language'], $space->language);

        $this->assertEquals($this->space->getId()+1, $space->id);
        $this->assertEquals($this->space->getAccountId(), $space->accountId);
    }

    public function testPostFail_IfIncorrectHeader()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'name'        => 'spcname',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));
        $this->request->setHeader('content-type', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }



    public function testPostFail_IfEmptyNameAndAnotherLanguage()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('accept-language', 'ru');

        $postData = array(
            'name'        => '',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $body = json_decode($this->getResponse()->getBody(), TRUE);
        $name = $body['name'];
        $isEmpty = $name['isEmpty'];
        $this->assertEquals($isEmpty, "Значение обязательно для заполнения и не может быть пустым");
    }

     public function testPostFail_IfEmptyName()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'name'        => '',
            'description' => 'descr',
            'position'    => 2,
            'language'    => 'en'
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $this->assertEquals($this->getResponse()->getBody(), '{"name":{"isEmpty":"Value is required and can\'t be empty"}}');
    }



    public function testPostFail_IfAccountIsWrong()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->setupAccount();

        $postData = array(
            'name'        => 'SpaceName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testPostFail_IfNotAuth()
    {
        $this->setupAccount();

        $postData = array(
            'name'        => 'SpaceName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }
}
