<?php

class V1_Space_DeleteActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setMethod("DELETE");
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('accept-language', 'en');
    }

      public function testDeleteFails_IfLastTranslation()
    {
        $this->setupAccount();
        $space = $this->_createSpace();
        $secondSpace = $this->_createSpace();

        $secondSpaceRuDomain = $this->spaceKit->createDomain(
             new ObjectCreator_Space_Decorated_Id(
                 new ObjectCreator_Space_Decorated_Language(
                     $this->spaceDefaultDecorators(), 'ru'
                 ), $secondSpace->getId()
             )
         );

        $this->spaceKit->saveDomain($secondSpaceRuDomain);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $space->getId(),
            'module'      => 'v1',
            'language'    => 'en',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restDeleteTranslation');
        $this->dispatch($url);
        $body = $this->getResponse()->getBody();
        $this->assertEmpty($body);

        $this->assertResponseCode(400);

        $dbSpaceRu = $this->spaceKit->find($space->getId(), $this->account, "en");
        $this->assertInstanceOf("Space_Model_Domain",$dbSpaceRu);
    }


     public function testDeleteTranslation()
    {
        $this->setupAccount();
        $space = $this->_createSpace();

        $spaceRuDomain = $this->spaceKit->createDomain(
             new ObjectCreator_Space_Decorated_Id(
                 new ObjectCreator_Space_Decorated_Language(
                     $this->spaceDefaultDecorators(), 'ru'
                 ), $space->getId()
             )
         );

        $this->spaceKit->saveDomain($spaceRuDomain);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restDeleteTranslation');
        $this->dispatch($url);
        $body = $this->getResponse()->getBody();
        $this->assertEmpty($body);

        $this->assertResponseCode(204);

        $dbSpaceEn = $this->spaceKit->find($space->getId(), $this->account, "en");
        $this->assertFalse($dbSpaceEn);
        $dbSpaceRu = $this->spaceKit->find($space->getId(), $this->account, "ru");
        $this->assertInstanceOf("Space_Model_Domain",$dbSpaceRu);
    }

    public function testDeleteSuccessfull()
    {
        $this->setupAccount();

        $spaceKit = new ObjectCreator_Space_Kit();
        $this->space = $spaceKit->saveDomain($this->account, FALSE);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertEmpty($this->getResponse()->getBody());

        $this->assertResponseCode(204);
    }

    public function testDeleteFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $spaceKit = new ObjectCreator_Space_Kit();
        $this->space = $spaceKit->saveDomain($this->account, FALSE);

        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testDeleteFails_IfDefaultSpace()
    {
         $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertEmpty($this->getResponse()->getBody());

        $this->assertResponseCode(400);
    }

    public function testDeleteFails_IfSpaceNotExist()
    {
        $this->setupAccount();

        $spaceKit = new ObjectCreator_Space_Kit();
        $this->space = $spaceKit->saveDomain($this->account, FALSE);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId()+1,
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testDeleteFails_IfWrongAccount()
    {
        $this->setupAccount();

        $spaceKit = new ObjectCreator_Space_Kit();
        $this->space = $spaceKit->saveDomain($this->account, FALSE);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName() . "kkk",
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testDeleteFails_IfNotAuth()
    {
        $this->setupAccount();

        $spaceKit = new ObjectCreator_Space_Kit();
        $this->space = $spaceKit->saveDomain($this->account, FALSE);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }
}
