<?php

class V1_Space_GetActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setMethod("GET");
        $this->request->setHeader('accept-language', 'en');
    }

    public function testGetTranslations()
    {
        $this->setupAccount();

        $space = $this->_createSpace();
        $this->space = $this->spaceKit->saveDomain($space);


        $spaceRuDomain = $this->spaceKit->createDomain(
             new ObjectCreator_Space_Decorated_Id(
                 new ObjectCreator_Space_Decorated_Language(
                     $this->spaceDefaultDecorators(), 'ru'
                 ), $this->space->getId()
             )
         );

        $this->spaceKit->saveDomain($spaceRuDomain);

        $domain = $this->spaceKit->createDomain(
                  $this->spaceDefaultDecorators()
                );
        $this->spaceEn = $this->spaceKit->saveDomain($domain);

       $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->spaceEn->getId(),
            'language'    => 'en',
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();
        $space = json_decode($result, true);

        $this->assertArrayHasKey('en', $space['translations']);
        $this->assertTrue($space['translations']['en']);
        $this->assertArrayHasKey('ru', $space['translations']);
        $this->assertFalse($space['translations']['ru']);

    }

    public function testGetSuccessfull()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = $this->getResponse()->getBody();
        $space = json_decode($result);

        $headers = $this->getResponse()->getHeaders();
        $lastModifiedHeader = $headers[3];

        $this->assertContains(date_format(date_create($this->space->getEdited()), 'D, d M Y H:i:s O'), $lastModifiedHeader['value']);
        $this->assertEquals($this->space->getName(), $space->name);
        $this->assertEquals($this->space->getAccountId(), $space->accountId);
        $this->assertEquals($this->space->getDescription(), $space->description);
        $this->assertEquals($this->space->getPosition(), $space->position);
        $this->assertEquals($this->space->getCreated(), $space->created);
        $this->assertEquals($this->space->getEdited(), $space->edited);
    }

    public function testGetFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testGetFails_IfNotExist()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId()+1,
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testCategoriesSuccessfull()
    {
        $this->setupAccount();

        $numberOfCategories = 2;
        $this->categoryKit->add($numberOfCategories, $this->categoryDefaultDecorators());
        $categoryCollection = $this->categoryKit->getCollection($this->account->getName(), $this->space);
        $categories = array('count' => $categoryCollection->count(), 'categories' => $categoryCollection->toArray());

        $expectedResults = json_encode($categories);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'categories'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();

        $this->assertEquals($expectedResults, $result);

        $this->assertResponseCode(200);
    }

    public function testCategoiesFails_IfincorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'categories'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testCategoiesFails_IfSpaceNotExist()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId()+1,
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'categories'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testCategoiesFails_IfWrongAccount()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName() . 'WrongName',
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'categories'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testItemsSuccessfull()
    {
        $this->setupAccount();

        $numberOfItems = 2;
        $decorator = new ObjectCreator_Item_Decorated_Publishing(
                     new ObjectCreator_Item_Decorated_Status(
                         $this->defaultDecorators(), Item_Model_Domain::STATUS_ANSWERED
                     ), Item_Model_Domain::PUBLISHED);
        $itemCollection = $this->itemKit->add($numberOfItems, $decorator);
        $items = array('count' => $itemCollection->count(), 'items' => $itemCollection->toArray());

        $expectedResult = json_encode($items);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();

        $this->assertEquals($expectedResult, $result);

        $this->assertResponseCode(200);
    }

    public function testItemsFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testItemsFails_IfSpaceNotExist()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId()+1,
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testItemsFails_IfWrongAccount()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName() . 'WrongName',
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }
}
