<?php

class V1_Space_PutActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setMethod("PUT");
    }

    public function testPutSuccessfull()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'name'        => 'SpaceName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = $this->getResponse()->getBody();
        $space = json_decode($result);

        $this->assertEquals($postData['name'], $space->name);
        $this->assertEquals($postData['description'], $space->description);
        $this->assertEquals($postData['position'], $space->position);
        $this->assertEquals($this->space->getId(), $space->id);
        $this->assertEquals($this->space->getAccountId(), $space->accountId);
    }

     public function testPutFail_IfIncorrectHeader()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'name'        => 'spaceName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testPutFail_IfEmptyName()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'name'        => '',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"name":{"isEmpty":"Value is required and can\'t be empty"}}');
    }

    public function testPutFail_IfEmptyNameAndAnotherLanguage()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('accept-language', 'ru');

        $postData = array(
            'name'        => '',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $body = json_decode($this->getResponse()->getBody(), TRUE);
        $name = $body['name'];
        $isEmpty = $name['isEmpty'];
        $this->assertEquals($isEmpty, "Значение обязательно для заполнения и не может быть пустым");
    }

    public function testPutFail_IfAccountIsWrong()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->setupAccount();

        $postData = array(
            'name'        => 'SpaceName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testPutFail_IfNotAuth()
    {
        $this->setupAccount();

        $postData = array(
            'name'        => 'SpaceName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testChangeTranslationLanguage(){
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $space = $this->_createSpace();

        $postData = array(
            'name'           => $space->getName(),
            'description'    => $space->getDescription(),
            'position'       => $space->getPosition(),
            'language'       => $space->getLanguage(),
            'newTranslation' => 'ru',
        );
        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space',
            'id'          => $space->getId(),
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = json_decode($this->getResponse()->getBody(), true);

        $this->assertEquals($postData['newTranslation'], $result['language']);
    }

    public function testTryChangeLanguageOnExistent(){
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $space = $this->_createSpace();

        $this->spaceKit->saveDomain(
            $this->spaceKit->createDomain(
                new ObjectCreator_Space_Decorated_Id(
                    new ObjectCreator_Space_Decorated_Language(
                        $this->spaceDefaultDecorators(), 'ru'
                    ),$space->getId()
                )
            )
        );

        $postData = array(
            'name'           => $space->getName(),
            'description'    => $space->getDescription(),
            'position'       => $space->getPosition(),
            'language'       => $space->getLanguage(),
            'newTranslation' => 'ru',
        );
        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'space',
            'id'          => $space->getId(),
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
    }
}
