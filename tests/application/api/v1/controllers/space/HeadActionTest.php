<?php

class V1_Space_HeadActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
    }

    public function testHeadSuccessfull()
    {
        $this->setupAccount();

        $this->request->setMethod('Head');

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->space->getId(),
            'module'      => 'v1',
            'controller'  => 'space'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);
    }
}
