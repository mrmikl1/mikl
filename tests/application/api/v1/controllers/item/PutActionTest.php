<?php

class V1_Item_PutActionTest extends TestGenericController
{
    public function setUp() {
        parent::setUp();
        $this->request->setMethod('PUT');
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
    }

    public function testPutSuccessfull()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'title',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = $this->getResponse()->getBody();
        $items = json_decode($result);

        $this->assertEquals($postData['title'], $items->title);
        $this->assertEquals($postData['content'], $items->content);
        $this->assertEquals($postData['position'], $items->position);
        $this->assertEquals($postData['categoryId'], $items->categoryId);
        $this->assertEquals($postData['spaceId'], $items->spaceId);
        $this->assertEquals($item->getAccountId(), $items->accountId);
        $this->assertEquals($item->getId(), $items->id);
    }

    public function testPutFail_IfIncorrectHeader()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'itemName',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setRawBody(json_encode($postData));
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testPutFail_IfEmptyTitle()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"title":{"isEmpty":"Value is required and can\'t be empty"}}');
    }

    public function testPutFail_IfInvalidEmail()
    {
        $this->setupAccount();
        $domain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Status(
                        $this->defaultDecorators(),
                        Item_Model_Domain::STATUS_ANSWERED
                    )
                );
        $this->item = $this->itemKit->saveDomain($domain);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'aaa',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"email":{"emailAddressInvalidFormat":"'."'{$postData['email']}'".' is not a valid email address in the basic format local-part@hostname"}}');
    }

    public function testPutFail_IfEmptyTitleAndAnotherLanguage()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('accept-language', 'ru');

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $body = json_decode($this->getResponse()->getBody(), TRUE);
        $name = $body['title'];
        $isEmpty = $name['isEmpty'];
        $this->assertEquals($isEmpty, "Значение обязательно для заполнения и не может быть пустым");
    }

    public function testPutFail_IfSpaceNotExists()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId()+1,
             'categoryId'       => $this->category->getId(),
             'title'            => 'title',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertContains('"spaceId":{"notInArray":"\'' . $postData['spaceId'] . '\' was not found in the haystack"}', $this->getResponse()->getBody());
    }

    public function testPutFail_IfCategoryNotExist()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId()+1,
             'title'            => 'title',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"categoryId":{"notInArray":"\'' . $postData['categoryId'] . '\' was not found in the haystack"}}');
    }

    public function testChangeTranslationLanguage(){
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $item = $this->_createItem();

        $postData = array(
            'spaceId'        => $item->getSpaceId(),
            'categoryId'     => $item->getCategoryId(),
            'title'          => $item->getTitle(),
            'content'        => $item->getContent(),
            'position'       => $item->getPosition(),
            'language'       => $item->getLanguage(),
            'newTranslation' => 'ru',
        );
        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item',
            'id'          => $item->getId(),
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = json_decode($this->getResponse()->getBody(), true);

        $this->assertEquals($postData['newTranslation'], $result['language']);
    }

    public function testTryChangeLanguageOnExistent(){
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $item = $this->_createItem();

        $this->itemKit->saveDomain(
            $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Id(
                    new ObjectCreator_Item_Decorated_Language(
                        $this->defaultDecorators(), 'ru'
                    ),$item->getId()
                )
            )
        );

        $postData = array(
            'spaceId'        => $item->getSpaceId(),
            'categoryId'     => $item->getCategoryId(),
            'title'          => $item->getTitle(),
            'content'        => $item->getContent(),
            'position'       => $item->getPosition(),
            'language'       => $item->getLanguage(),
            'newTranslation' => 'ru',
        );
        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item',
            'id'          => $item->getId(),
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
    }
}