<?php

class V1_Item_IndexActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
    }

    public function testIndexSuccessfull()
    {
        $this->setupAccount();
        $this->_createItem();

        $numberOfItems = 2;
        $this->itemKit->add($numberOfItems, $this->defaultDecorators());
        $itemCollection = $this->itemKit->getCollection($this->account->getName(), $this->category->getId());
        $items = $itemCollection->toArray();
        $body = array(
            'count'  => $itemCollection->count(),
            'items' => $items
        );

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();
        $this->assertEquals(json_encode($body),$result);

        $this->assertResponseCode(200);
    }

    public function testIndexCorrectLanguageSuccessfull()
    {
        $this->setupAccount();

        $numberOfItems = 2;

        $decorator = new ObjectCreator_Item_Decorated_Publishing(
                     new ObjectCreator_Item_Decorated_Status(
                         $this->defaultDecorators(), Item_Model_Domain::STATUS_ANSWERED
                     ), Item_Model_Domain::PUBLISHED
                );
        $this->itemKit->add($numberOfItems, $decorator);
        $itemEn1 = $this->itemKit->getCollection($this->account->getName(), $this->category->getId());

        $itemRuDomain1 = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Id(
                    new ObjectCreator_Item_Decorated_Language(
                            new ObjectCreator_Item_Decorated_Publishing(
                                new ObjectCreator_Item_Decorated_Status(
                                    $this->defaultDecorators(), Item_Model_Domain::STATUS_ANSWERED
                            ), Item_Model_Domain::PUBLISHED
                        ), 'ru'
                    ), $itemEn1[0]->getId()
                )
            );
        $itemRu1 = $this->itemKit->saveDomain($itemRuDomain1);

        $categoryRuDomain2 = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Id(
                    new ObjectCreator_Item_Decorated_Language(
                            new ObjectCreator_Item_Decorated_Publishing(
                                new ObjectCreator_Item_Decorated_Status(
                                    $this->defaultDecorators(), Item_Model_Domain::STATUS_ANSWERED
                            ), Item_Model_Domain::PUBLISHED
                        ), 'ru'
                    ), $itemEn1[1]->getId()
                )
            );
        $itemRu2 = $this->itemKit->saveDomain($categoryRuDomain2);

        $itemEnDomain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Publishing(
                    new ObjectCreator_Item_Decorated_Status(
                        $this->defaultDecorators(), Item_Model_Domain::STATUS_ANSWERED
                    ), Item_Model_Domain::PUBLISHED
                )
            );
        $itemEn2 = $this->itemKit->saveDomain($itemEnDomain);

        $this->request->setHeader('Accept-Language', 'ru');
        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = json_decode($this->getResponse()->getBody(), true);

        $this->assertCount($numberOfItems+1, $result['items']);

        $first  = $result['items'][0];
        $second = $result['items'][1];
        $third  = $result['items'][2];

        $this->assertResponseCode(200);

        $this->assertEquals($first['id'], $itemEn1[0]->getId());
        $this->assertEquals($first['title'], $itemRu1->getTitle());
        $this->assertEquals($first['content'], $itemRu1->getContent());
        $this->assertEquals($first['language'], $itemRu1->getLanguage());
        $this->assertTrue($first['translations']['en']);
        $this->assertTrue($first['translations']['ru']);

        $this->assertEquals($second['id'], $itemEn1[1]->getId());
        $this->assertEquals($second['title'], $itemRu2->getTitle());
        $this->assertEquals($second['content'], $itemRu2->getContent());
        $this->assertEquals($second['language'], $itemRu2->getLanguage());
        $this->assertTrue($second['translations']['en']);
        $this->assertTrue($second['translations']['ru']);

        $this->assertEquals($third['id'], $itemEn2->getId());
        $this->assertEquals($third['title'], $itemEn2->getTitle());
        $this->assertEquals($third['content'], $itemEn2->getContent());
        $this->assertEquals($third['language'], $itemEn2->getLanguage());
        $this->assertTrue($third['translations']['en']);
        $this->assertFalse($third['translations']['ru']);
    }

    public function testIndexFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testView_TryViewUnanswered()
    {
        $this->setupAccount();
        $this->_createItem();

        $numberOfItems = 2;
        $this->itemKit->add($numberOfItems, $this->defaultDecorators());
        $itemCollection = $this->itemKit->getCollection($this->account->getName(), $this->category->getId());
        $items = $itemCollection->toArray();
        $body = array(
            'count'  => $itemCollection->count(),
            'items' => $items
        );

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();
        $result = json_decode($result);
        $this->assertEquals($result->count,0);

        $this->assertResponseCode(200);
    }
}
