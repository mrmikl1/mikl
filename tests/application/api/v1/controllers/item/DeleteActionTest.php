<?php

class V1_Item_DeleteActionTest extends TestGenericController
{
    public function setUp() {
        parent::setUp();
        $this->request->setMethod("DELETE");
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('accept-language', 'en');
    }


    public function testDeleteFails_IfLastTranslation()
    {
        $this->setupAccount();
        $item = $this->_createItem();
        $secondItem = $this->_createItem();

        $secondItemRuDomain = $this->itemKit->createDomain(
             new ObjectCreator_Item_Decorated_Id(
                 new ObjectCreator_Item_Decorated_Language(
                     $this->defaultDecorators(), 'ru'
                 ), $secondItem->getId()
             )
         );

        $this->itemKit->saveDomain($secondItemRuDomain);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'language'    => 'en',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restDeleteTranslation');
        $this->dispatch($url);
        $body = $this->getResponse()->getBody();
        $this->assertEmpty($body);

        $this->assertResponseCode(400);

        $dbItemRu = $this->itemKit->find($item->getId(), $this->account, "en");
        $this->assertInstanceOf("Item_Model_Domain",$dbItemRu);
    }

    public function testDeleteTranslation()
    {
        $this->setupAccount();
        $item = $this->_createItem();
        $itemRuDomain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Id(
                    new ObjectCreator_Item_Decorated_Language(
                        $this->defaultDecorators(), 'ru'
                    ), $item->getId()
                )
            );

        $this->itemKit->saveDomain($itemRuDomain);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'language'    => 'en',
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restDeleteTranslation');
        $this->dispatch($url);
        $body = $this->getResponse()->getBody();
        $this->assertEmpty($body);

        $this->assertResponseCode(204);

        $dbItemEn = $this->itemKit->find($item->getId(), $this->account, "en");
        $this->assertFalse($dbItemEn);
        $dbItemRu = $this->itemKit->find($item->getId(), $this->account, "ru");
        $this->assertInstanceOf("Item_Model_Domain",$dbItemRu);
    }

    public function testDeleteSuccessfull()
    {
        $this->setupAccount();
        $item = $this->_createItem();
        $itemRuDomain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Id(
                    new ObjectCreator_Item_Decorated_Language(
                        $this->defaultDecorators(), 'ru'
                    ), $item->getId()
                )
            );

        $this->itemKit->saveDomain($itemRuDomain);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertEmpty($this->getResponse()->getBody());

        $this->assertResponseCode(204);
    }

    public function testDeleteFails_IfIncorrectHeader()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testDeleteFails_IfWrongAccount()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->setupAccount();
        $item = $this->_createItem();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testDeleteFails_IfNotAuth()
    {
        $this->setupAccount();
        $domain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Status(
                        $this->defaultDecorators(),
                        Item_Model_Domain::STATUS_ANSWERED
                    )
                );
        $this->item = $this->itemKit->saveDomain($domain);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testDeleteFails_IfItemNotExist()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId()+1,
            'module'      => 'v1',
            'controller'  => 'item'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }
}
