<?php

class V1_Item_GetActionTest extends TestGenericController
{
    public function setUp() {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('accept-language', 'en');
        $this->request->setMethod("GET");
    }

    public function testGetTranslations()
    {
        $this->setupAccount();

        $domain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Status(
                        $this->defaultDecorators(),
                        Item_Model_Domain::STATUS_ANSWERED
                    )
                );
        $this->item = $this->itemKit->saveDomain($domain);

        $itemRuDomain = $this->itemKit->createDomain(
             new ObjectCreator_Item_Decorated_Id(
                 new ObjectCreator_Item_Decorated_Language(
                     $this->defaultDecorators(), 'ru'
                 ), $this->item->getId()
             )
         );

        $this->itemKit->saveDomain($itemRuDomain);

        $domain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Status(
                        $this->defaultDecorators(),
                        Item_Model_Domain::STATUS_ANSWERED
                    )
                );
        $this->itemEn = $this->itemKit->saveDomain($domain);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->itemEn->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();
        $item = json_decode($result, true);

        $this->assertArrayHasKey('en', $item['translations']);
        $this->assertTrue($item['translations']['en']);
        $this->assertArrayHasKey('ru', $item['translations']);
        $this->assertFalse($item['translations']['ru']);

    }

    public function testGetSuccessfull()
    {
        $this->setupAccount();
        $domain = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Status(
                        $this->defaultDecorators(),
                        Item_Model_Domain::STATUS_ANSWERED
                    )
                );
        $this->item = $this->itemKit->saveDomain($domain);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = $this->getResponse()->getBody();
        $item = json_decode($result);

        $headers = $this->getResponse()->getHeaders();
        $lastModifiedHeader = $headers[3];

        $this->assertEquals(date('D, d M Y H:i:s O', strtotime($this->item->getEdited())), $lastModifiedHeader['value']);
        $this->assertEquals($this->item->getTitle(), $item->title);
        $this->assertEquals($this->item->getContent(), $item->content);
        $this->assertEquals($this->item->getPosition(), $item->position);
        $this->assertEquals($this->item->getAccountId(), $item->accountId);
        $this->assertEquals($this->item->getSpaceId(), $item->spaceId);
        $this->assertEquals($this->item->getCategoryId(), $item->categoryId);
        $this->assertEquals($this->item->getCreated(), $item->created);
        $this->assertEquals($this->item->getEdited(), $item->edited);
    }

    public function testGetRuTranslationSuccessfull()
    {
        $this->request->setHeader('accept-language', 'ru');

        $this->setupAccount();
        $itemDomainEn = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Status(
                        $this->defaultDecorators(),
                        Item_Model_Domain::STATUS_ANSWERED
                    )
                );
        $itemEn = $this->itemKit->saveDomain($itemDomainEn);

        $itemDomainRu = $this->itemKit->createDomain(
                new ObjectCreator_Item_Decorated_Language(
                    new ObjectCreator_Item_Decorated_Id(
                        new ObjectCreator_Item_Decorated_Status(
                                $this->defaultDecorators(),
                                Item_Model_Domain::STATUS_ANSWERED
                        ), $itemEn->getId()
                    ), 'ru')
                );
        $itemRu = $this->itemKit->saveDomain($itemDomainRu);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $itemEn->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = $this->getResponse()->getBody();
        $item = json_decode($result);

        $headers = $this->getResponse()->getHeaders();
        $lastModifiedHeader = $headers[3];

        $this->assertEquals(date('D, d M Y H:i:s O',strtotime($itemRu->getEdited())), $lastModifiedHeader['value']);
        $this->assertEquals($itemRu->getTitle(), $item->title);
        $this->assertEquals($itemRu->getContent(), $item->content);
        $this->assertEquals($itemRu->getPosition(), $item->position);
        $this->assertEquals($itemRu->getAccountId(), $item->accountId);
        $this->assertEquals($itemRu->getSpaceId(), $item->spaceId);
        $this->assertEquals($itemRu->getCategoryId(), $item->categoryId);
        $this->assertEquals($itemRu->getEdited(), $item->edited);
        $this->assertNotNull($item->created);
    }

    public function testGetFails_IfNotExist()
    {
        $this->setupAccount();
        $item = $this->_createItem();

       $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId()+1,
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testGetFails_IfIncorrectHeader()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $item->getId(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }
}