<?php

class V1_Item_PostActionTest extends TestGenericController
{
    public function setUp() {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('accept-language', 'en');
        $this->request->setMethod("POST");
    }

    public function testPostFail_IfInvalidTranslations()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'id'               => $item->getId(),
             'title'            => 'aaaa',
             'content'          => 'content',
             'position'         => 2,
             'language'         => 'inlan', //incorect language code
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();
        $items = json_decode($result);

        $dbItem = $this->itemKit->find($item->getId(), $this->account, 'inlan');

        $this->assertFalse($dbItem);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"language":{"noRecordFound":"No record matching \'inlan\' was found"}}');
    }


    public function testPostSuccessfull()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '<script>alert("itemTitle")</script>',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(201);
        $this->assertHeader('location');

        $result = $this->getResponse()->getBody();
        $item = json_decode($result);

        $dbItem = $this->itemKit->find($item->id, $this->account);


        $this->assertEquals(htmlentities($postData['title']), $dbItem->getTitle());
        $this->assertEquals($postData['content'], $dbItem->getContent());
        $this->assertEquals($postData['position'], $dbItem->getPosition());
        $this->assertEquals($postData['categoryId'], $dbItem->getCategoryId());
        $this->assertEquals($postData['spaceId'], $dbItem->getSpaceId());
        $this->assertEquals($this->account->getId(), $dbItem->getAccountId());
        $this->assertNotNull($item->id);
        $this->assertEquals($postData['language'], $dbItem->getLanguage());
    }

    public function testPostNewLanguage()
    {
        $this->setupAccount();
        $item = $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'id'               => $item->getId(),
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '<script>alert("itemTitle")</script>',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'ru',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(201);
        $this->assertHeader('location');

        $result = $this->getResponse()->getBody();
        $dbItem = $this->itemKit->find($item->getId(), $this->account, 'ru');

        $this->assertInstanceOf("Item_Model_Domain", $dbItem);
    }

    public function testPostFail_IfIncorrectHeader()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'itemName',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testPostFail_IfEmptyTitle()
    {
        $this->setupAccount();
        $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"title":{"isEmpty":"Value is required and can\'t be empty"}}');
    }

    public function testPostFail_IfInvalidEmail()
    {
        $this->setupAccount();
        $this->_createItem();

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'aaaa',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"email":{"emailAddressInvalidFormat":"'."'{$postData['email']}'".' is not a valid email address in the basic format local-part@hostname"}}');
    }

    public function testPostFail_IfEmptyTitleAndAnotherLanguage()
    {
        $this->setupAccount();
        $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('accept-language', 'ru');

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $body = json_decode($this->getResponse()->getBody(), TRUE);
        $name = $body['title'];
        $isEmpty = $name['isEmpty'];
        $this->assertEquals($isEmpty, "Значение обязательно для заполнения и не может быть пустым");
    }

    public function testPostFail_IfSpaceNotExist()
    {
        $this->setupAccount();
        $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId()+1,
             'categoryId'       => $this->category->getId(),
             'title'            => 'title',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals('{"spaceId":{"notInArray":"\'' . $postData['spaceId'] . '\' was not found in the haystack"},"categoryId":{"notInArray":"\'' . $postData['categoryId'] . '\' was not found in the haystack"}}', $this->getResponse()->getBody());

    }

    public function testPostFail_IfCategoryNotExist()
    {
        $this->setupAccount();
        $this->_createItem();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId()+1,
             'title'            => 'title',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"categoryId":{"notInArray":"\'' . $postData['categoryId'] . '\' was not found in the haystack"}}');
    }


    public function testPostUnanswered_IfNoAuth()
    {
        $this->setupAccount();
        $this->_createItem();

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'title',
             'content'          => 'content',
             'position'         => 2,
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
             'language'         => 'en',
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'item'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(201);
    }
}