<?php

class V1_Category_IndexActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
    }

    public function testIndexSuccessfull()
    {
        $this->setupAccount();

        $numberOfCategories = 2;
        $this->categoryKit->add($numberOfCategories, $this->categoryDefaultDecorators());
        $categoryCollection = $this->categoryKit->getCollection($this->account->getName(), $this->space);
        $categories = $categoryCollection->toArray();
        $body = array(
            'count'  => $categoryCollection->count(),
            'categories' => $categories
        );

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);
        $result = $this->getResponse()->getBody();

        $this->assertEquals(json_encode($body),$result);

        $this->assertResponseCode(200);
    }

    public function testIndexCorrectLanguageSuccessfull()
    {
        $this->setupAccount();

        $numberOfCategories = 2;

        $this->categoryKit->add($numberOfCategories, $this->categoryDefaultDecorators());
        $categoryCollection = $this->categoryKit->getCollection($this->account->getName(), $this->space);
        $categoryDefaultEn = $categoryCollection->offsetGet(0);
        $categoryFirstEn   = $categoryCollection->offsetGet(1);
        $categorySecondEn  = $categoryCollection->offsetGet(2);

        $defaultCategoryRuDomain = $this->categoryKit->createDomain(
                new ObjectCreator_Category_Decorated_Id(
                    new ObjectCreator_Category_Decorated_Language(
                        $this->categoryDefaultDecorators(), 'ru'
                    ), $categoryDefaultEn->getId()
                )
            );
        $defaultCategoryRu = $this->categoryKit->saveDomain($defaultCategoryRuDomain);

        $categoryFirstRuDomain = $this->categoryKit->createDomain(
                new ObjectCreator_Category_Decorated_Id(
                    new ObjectCreator_Category_Decorated_Language(
                        $this->categoryDefaultDecorators(), 'ru'
                    ), $categoryFirstEn->getId()
                )
            );
        $categoryFirstRu = $this->categoryKit->saveDomain($categoryFirstRuDomain);

        $this->request->setHeader('Accept-Language', 'ru');
        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = json_decode($this->getResponse()->getBody(), true);

        $this->assertCount($categoryCollection->count(), $result['categories']);

        $first  = $result['categories'][0];
        $second = $result['categories'][1];
        $third  = $result['categories'][2];

        $this->assertResponseCode(200);

        $this->assertEquals($first['name'], $defaultCategoryRu->getName());
        $this->assertEquals($first['language'], 'ru');
        $this->assertTrue($first['translations']['en']);
        $this->assertTrue($first['translations']['ru']);

        $this->assertEquals($second['name'], $categoryFirstRu->getName());
        $this->assertEquals($second['language'], 'ru');
        $this->assertTrue($second['translations']['en']);
        $this->assertTrue($second['translations']['ru']);

        $this->assertEquals($third['name'], $categorySecondEn->getName());
        $this->assertEquals($third['language'], 'en');
        $this->assertTrue($third['translations']['en']);
        $this->assertFalse($third['translations']['ru']);
    }

    public function testIndexFails_ifIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }
}
