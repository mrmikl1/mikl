<?php

class V1_Category_PutActionTest extends TestGenericController
{
    public function setUp() {
        parent::setUp();
        $this->request->setMethod("PUT");
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
    }

    public function testPutSuccessfull()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => 'categoryName',
            'description' => 'description',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = $this->getResponse()->getBody();
        $category = json_decode($result);
        $this->assertEquals($postData['name'], $category->name);
        $this->assertEquals($postData['description'], $category->description);
        $this->assertEquals($postData['position'], $category->position);
        $this->assertEquals($postData['spaceId'], $category->spaceId);
        $this->assertEquals($this->category->getAccountId(), $category->accountId);
        $this->assertEquals($this->category->getId(), $category->id);
        $this->assertHeader('location');
    }

    public function testPutFail_IfIncorrectHeader()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => 'catName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testPutFail_IfEmptyName()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => '',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"name":{"isEmpty":"Value is required and can\'t be empty"}}');
    }

    public function testPutFail_IfEmptyNameAndAnotherLanguage()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('accept-language', 'ru');

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => '',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $body = json_decode($this->getResponse()->getBody(), TRUE);
        $name = $body['name'];
        $isEmpty = $name['isEmpty'];
        $this->assertEquals($isEmpty, "Значение обязательно для заполнения и не может быть пустым");
    }

    public function testPutFail_IfSpaceNotExist()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId()+1,
            'name'        => 'catName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"spaceId":{"notInArray":"\'' . $postData['spaceId'] . '\' was not found in the haystack"}}');
    }

    public function testPutFail_IfNotAuth()
    {
        $this->setupAccount();

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => 'categoryName',
            'description' => 'description',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testPutFail_IfWrongAccount()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->setupAccount();

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => 'categoryName',
            'description' => 'description',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testChangeTranslationLanguage(){
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $category = $this->_createCategory();

        $postData = array(
            'spaceId'        => $category->getSpaceId(),
            'name'           => $category->getName(),
            'description'    => $category->getDescription(),
            'position'       => $category->getPosition(),
            'language'       => $category->getLanguage(),
            'newTranslation' => 'ru',
        );
        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category',
            'id'          => $category->getId(),
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = json_decode($this->getResponse()->getBody(), true);

        $this->assertEquals($postData['newTranslation'], $result['language']);
    }

    public function testTryChangeLanguageOnExistent(){
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);
        $category = $this->_createCategory();

        $this->categoryKit->saveDomain(
            $this->categoryKit->createDomain(
                new ObjectCreator_Category_Decorated_Id(
                    new ObjectCreator_Category_Decorated_Language(
                        $this->categoryDefaultDecorators(), 'ru'
                    ),$category->getId()
                )
            )
        );

        $postData = array(
            'spaceId'        => $category->getSpaceId(),
            'name'           => $category->getName(),
            'description'    => $category->getDescription(),
            'position'       => $category->getPosition(),
            'language'       => $category->getLanguage(),
            'newTranslation' => 'ru',
        );
        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category',
            'id'          => $category->getId(),
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
    }
}
