<?php

class V1_Category_PostActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('accept-language', 'en');
        $this->request->setMethod("POST");
    }

    public function testPostNewLanguage()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'id'          => $this->category->getId(),
            'name'        => '<script>alert("spaceName")</script>',
            'description' => 'descr',
            'position'    => 2,
            'language'    => 'ru'
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(201);

        $result = $this->getResponse()->getBody();
        $category = json_decode($result);
        $dbCategory = $this->categoryKit->find($category->id, $this->account, 'ru');

        $this->assertInstanceOf("Category_Model_Domain", $dbCategory);
        $this->assertHeader('location');
    }

    public function testPostSuccessfull()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => '<script>alert("categoryName")</script>',
            'description' => 'description',
            'position'    => 2,
            'language'    => 'en'
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(201);

        $result = $this->getResponse()->getBody();
        $category = json_decode($result);
        $this->assertEquals(htmlentities($postData['name']), $category->name);
        $this->assertEquals($postData['description'], $category->description);
        $this->assertEquals($postData['position'], $category->position);
        $this->assertEquals($postData['spaceId'], $category->spaceId);
        $this->assertEquals($this->category->getAccountId(), $category->accountId);
        $this->assertEquals($this->category->getId()+1, $category->id);
        $this->assertHeader('location');
    }

    public function testPostFails_ifIncorrectHeader()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => 'categoryName',
            'description' => 'description',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testPostFail_IfEmptyName()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => '',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));


        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"name":{"isEmpty":"Value is required and can\'t be empty"}}');
    }

    public function testPostFail_IfEmptyNameAndAnotherLanguage()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('accept-language', 'ru');

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => '',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));


        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $body = json_decode($this->getResponse()->getBody(), TRUE);
        $name = $body['name'];
        $isEmpty = $name['isEmpty'];
        $this->assertEquals($isEmpty, "Значение обязательно для заполнения и не может быть пустым");
    }

    public function testPostFail_IfSpaceNotExist()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'spaceId'     => $this->space->getId()+1,
            'name'        => 'catName',
            'description' => 'descr',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"spaceId":{"notInArray":"\'' . $postData['spaceId'] . '\' was not found in the haystack"}}');
    }

    public function testPostFail_IfNotAuth()
    {
        $this->setupAccount();

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => 'categoryName',
            'description' => 'description',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }

    public function testPostFail_IfWrongAccount()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->setupAccount();

        $postData = array(
            'spaceId'     => $this->space->getId(),
            'name'        => 'categoryName',
            'description' => 'description',
            'position'    => 2
        );

        $this->request->setRawBody(json_encode($postData));

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }
}