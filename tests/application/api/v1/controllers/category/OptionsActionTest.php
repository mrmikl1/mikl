<?php

class V1_Category_OptionsActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
    }

    public function testOptionsSuccessfull()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setMethod('OPTIONS');

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $doc = json_decode($this->getResponse()->getBody(), true);

        $this->assertArrayHasKey('GET', $doc);
        $this->assertArrayHasKey('POST', $doc);
        $this->assertArrayHasKey('PUT', $doc);
        $this->assertArrayHasKey('DELETE', $doc);

        $this->assertArrayHasKey('description', $doc['PUT']);
        $this->assertArrayHasKey('description', $doc['GET']);
        $this->assertArrayHasKey('description', $doc['POST']);
        $this->assertArrayHasKey('description', $doc['DELETE']);

        $this->assertArrayHasKey('parametrs', $doc['PUT']);
        $this->assertArrayHasKey('parametrs', $doc['GET']);
        $this->assertArrayHasKey('parametrs', $doc['POST']);
        $this->assertArrayHasKey('parametrs', $doc['DELETE']);

        $this->assertArrayHasKey('example', $doc['PUT']);
        $this->assertArrayHasKey('example', $doc['GET']);
        $this->assertArrayHasKey('example', $doc['POST']);
        $this->assertArrayHasKey('example', $doc['DELETE']);

        $this->assertResponseCode(200);
    }
}
