<?php

class V1_Category_GetActionTest extends TestGenericController
{
    public function setUp() {
        parent::setUp();
        $this->request->setMethod('GET');
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setHeader('accept-language', 'en');
    }

    public function testGetSuccessfull()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(200);

        $result = $this->getResponse()->getBody();
        $category = json_decode($result);

        $headers = $this->getResponse()->getHeaders();
        $lastModifiedHeader = $headers[3];

        $this->assertContains(date_format(date_create($this->category->getEdited()), 'D, d M Y H:i:s O'), $lastModifiedHeader['value']);
        $this->assertEquals($this->category->getName(), $category->name);
        $this->assertEquals($this->category->getAccountId(), $category->accountId);
        $this->assertEquals($this->category->getSpaceId(), $category->spaceId);
        $this->assertEquals($this->category->getPosition(), $category->position);
        $this->assertEquals($this->category->getDescription(), $category->description);
        $this->assertEquals($this->category->getCreated(), $category->created);
        $this->assertEquals($this->category->getEdited(), $category->edited);
    }

    public function testGetTranslations()
    {
        $this->setupAccount();

        $this->category = $this->categoryKit->saveDomain(
                $this->categoryKit->createDomain(
                        $this->categoryDefaultDecorators()
                    )
                );

        $categoryRuDomain = $this->categoryKit->createDomain(
             new ObjectCreator_Category_Decorated_Id(
                 new ObjectCreator_Category_Decorated_Language(
                     $this->categoryDefaultDecorators(), 'ru'
                 ), $this->category->getId()
             )
         );

        $this->categoryKit->saveDomain($categoryRuDomain);

        $this->categoryEn = $this->categoryKit->saveDomain(
                $this->categoryKit->createDomain(
                        $this->categoryDefaultDecorators()
                    )
                );

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->categoryEn->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();
        $category = json_decode($result, true);

        $this->assertArrayHasKey('en', $category['translations']);
        $this->assertTrue($category['translations']['en']);
        $this->assertArrayHasKey('ru', $category['translations']);
        $this->assertFalse($category['translations']['ru']);

    }

    public function testGetFails_IfNotExist()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId()+1,
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testGetFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testItemsSuccessfull()
    {
        $this->setupAccount();

        $numberOfItems = 2;
        $decorator = new ObjectCreator_Item_Decorated_Publishing(
                     new ObjectCreator_Item_Decorated_Status(
                         $this->defaultDecorators(), Item_Model_Domain::STATUS_ANSWERED
                     ), Item_Model_Domain::PUBLISHED);
        $itemCollection = $this->itemKit->add($numberOfItems, $decorator);
        $items = array('count' => $itemCollection->count(), 'items' => $itemCollection->toArray());

        $expectedResult = json_encode($items);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();

        $this->assertEquals($expectedResult, $result);

        $this->assertResponseCode(200);
    }

    public function testItemsFails_IfCategoryNotExist()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId()+1,
            'module'      => 'v1',
            'controller'  => 'category',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testItemsFails_IfWrongAccount()
    {
        $this->setupAccount();

        $params = array(
            'accountName' => $this->account->getName() . 'WrongName',
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testItemsFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGet');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }
}