<?php

class V1_Category_DeleteActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
        $this->request->setMethod("DELETE");
        $this->request->setHeader('accept-language', 'en');
    }

    public function testDeleteFails_IfLastTranslation()
    {
        $this->setupAccount();
        $category = $this->_createCategory();
        $secondCategory = $this->_createCategory();

        $secondCategoryRuDomain = $this->categoryKit->createDomain(
             new ObjectCreator_Category_Decorated_Id(
                 new ObjectCreator_Category_Decorated_Language(
                     $this->categoryDefaultDecorators(), 'ru'
                 ), $secondCategory->getId()
             )
         );

        $this->categoryKit->saveDomain($secondCategoryRuDomain);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $category->getId(),
            'module'      => 'v1',
            'language'    => 'en',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restDeleteTranslation');
        $this->dispatch($url);
        $body = $this->getResponse()->getBody();
        $this->assertEmpty($body);

        $this->assertResponseCode(400);

        $dbCategoryRu = $this->categoryKit->find($category->getId(), $this->account, "en");
        $this->assertInstanceOf("Category_Model_Domain",$dbCategoryRu);
    }



     public function testDeleteTranslation()
    {
        $this->setupAccount();
        $category = $this->_createCategory();

        $categoryRuDomain = $this->categoryKit->createDomain(
             new ObjectCreator_Category_Decorated_Id(
                 new ObjectCreator_Category_Decorated_Language(
                     $this->categoryDefaultDecorators(), 'ru'
                 ), $category->getId()
             )
         );

        $this->categoryKit->saveDomain($categoryRuDomain);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'category',
            'id'          => $category->getId(),
            'language'    => 'en'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restDeleteTranslation');
        $this->dispatch($url);

        $body = $this->getResponse()->getBody();
        $this->assertEmpty($body);

        $this->assertResponseCode(204);

        $dbCategoryEn = $this->categoryKit->find($category->getId(), $this->account, "en");
        $this->assertFalse($dbCategoryEn);
        $dbCategoryRu = $this->categoryKit->find($category->getId(), $this->account, "ru");
        $this->assertInstanceOf("Category_Model_Domain", $dbCategoryRu);
    }

    public function testDeleteSuccessfull()
    {
        $this->setupAccount();
        $category = $this->_createCategory();

        $category = $this->categoryKit->createDomain(
             new ObjectCreator_Category_Decorated_Id(
                 new ObjectCreator_Category_Decorated_Language(
                         new ObjectCreator_Category_Decorated_Default(
                            $this->categoryDefaultDecorators(), false
                        ), 'ru'
                 ), $category->getId()
             )
         );

        $category = $this->categoryKit->saveDomain($category);

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);
        echo $this->getResponse()->getBody();
        $this->assertEmpty($this->getResponse()->getBody());

        $this->assertResponseCode(204);
    }

    public function testDeleteFails_IfDefaultCategory()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $category = $this->_createCategory();
        $categoryDefaultDomain = $this->categoryKit->createDomain(
             new ObjectCreator_Category_Decorated_Id(
                 new ObjectCreator_Category_Decorated_Default(
                     $this->categoryDefaultDecorators(), 'Y'
                 ), $category->getId()
             )
         );

        $category = $this->categoryKit->saveDomain($category);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertEmpty($this->getResponse()->getBody());

        $this->assertResponseCode(400);
    }

    public function testDeleteFails_IfNotExist()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $categoryKit = new ObjectCreator_Category_Kit();
        $this->category = $categoryKit->saveDomain($this->category);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId()+1,
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testDeleteFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $categoryKit = new ObjectCreator_Category_Kit();
        $this->category = $categoryKit->saveDomain($this->category);

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testDeleteFails_IfWrongAccount()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->setupAccount();

        $categoryKit = new ObjectCreator_Category_Kit();
        $this->category = $categoryKit->saveDomain($this->category);

        $params = array(
            'accountName' => $this->account->getName(),
            'id'          => $this->category->getId(),
            'module'      => 'v1',
            'controller'  => 'category'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'rest');
        $this->dispatch($url);

        $this->assertResponseCode(401);
    }
}