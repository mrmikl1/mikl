<?php
class V1_AccountControllerTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
        $this->request->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->request->setHeader('accept', 'application/json');
    }

    public function testPostSuccessfull()
    {
        $postData = array(
            'firstName'         => '<script>alert("Igor")</script>',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'accName'
        );

        $this->request->setMethod('POST')->setRawBody(json_encode($postData));

        $params = array(
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $result = json_decode($this->getResponse()->getBody(), TRUE);

        $this->assertResponseCode(201);
        $this->assertEquals($result['name'], $postData['accountName']);
        $this->assertNotNull($result['created']);
        $this->assertHeader("Location");
    }

    public function testPostFails_IfIncorrectHeader()
    {
        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'accName'
        );

        $this->request->setMethod('POST')->setRawBody(json_encode($postData));
        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testPostFails_IfEmailIsIncorrectAndAnotherLanguage()
    {
        $this->getRequest()->setHeader('accept-language', 'ru');

        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'accName'
        );

        $this->request->setMethod('POST')->setRawBody(json_encode($postData));

        $params = array(
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(400);

        $body = json_decode($this->getResponse()->getBody(), TRUE);
        $emailAddressInvalidFormat = $body['email']['emailAddressInvalidFormat'];
        $this->assertEquals($emailAddressInvalidFormat, "'userEmail' недопустимый адрес электронной почты. Введите его в формате имя@домен");
    }

    public function testPostFails_IfEmailIsIncorrect()
    {
        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'accName1'
        );

        $this->request->setMethod('POST')->setRawBody(json_encode($postData));

        $params = array(
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"email":{"emailAddressInvalidFormat":"\'userEmail\' is not a valid email address in the basic format local-part@hostname"}}');
    }

    public function testPostFails_IfEmptyFields()
    {
        $postData = array(
            'firstName'         => '',
            'lastName'          => '',
            'email'             => '',
            'password'          => '',
            'passwordConfirm'   => '',
            'accountName'       => ''
        );

        $this->request->setMethod('POST')->setRawBody(json_encode($postData));

        $params = array(
           'module'      => 'v1',
           'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(), '{"email":{"isEmpty":"Value is required and can\'t be empty"},"password":{"isEmpty":"Value is required and can\'t be empty"},"passwordConfirm":{"isEmpty":"Value is required and can\'t be empty"},"accountName":{"isEmpty":"Value is required and can\'t be empty"},"firstName":{"isEmpty":"Value is required and can\'t be empty"},"lastName":{"isEmpty":"Value is required and can\'t be empty"}}');
    }

    public function testPostFails_IfPasswordsAreNotIndentical()
    {
        $postData = array(
            'firstName'          => 'Igor',
            'lastName'           => 'D',
            'email'              => 'userEmail@ukr.net',
            'password'           => 'pass1',
            'passwordConfirm'    => 'pass2',
            'accountName'        => 'accName2'
        );

        $this->request->setMethod('POST')->setRawBody(json_encode($postData));

        $params = array(
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(),'{"passwordConfirm":{"notSame":"The two given tokens do not match"}}');
    }

    public function testPostFails_IfAccountIsNotUnique()
    {
        $this->setupAccount();

        $postData = array(
            'firstName'          => 'Igor',
            'lastName'           => 'D',
            'email'              => 'userEmail@ukr.net',
            'password'           => 'pass1',
            'passwordConfirm'    => 'pass1',
            'accountName'        => $this->account->getName()
        );

        $this->request->setMethod('POST')->setRawBody(json_encode($postData));

        $params = array(
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(400);
        $this->assertEquals($this->getResponse()->getBody(),'{"accountName":{"recordFound":"A record matching \'' . $postData['accountName'] . '\' was found"}}');
    }

    public function testGetSuccessfull()
    {
        $this->setupAccount();

        $params = array(
            'id'        => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $body = $this->getResponse()->getBody();
        $this->assertContains('name":"' . $this->account->getName() . '"', $body);
        $this->assertResponseCode(200);
    }

    public function testGetFails_IfAccountIsIncorrect()
    {
        $this->setupAccount();

        $params = array(
            'id'          => $this->account->getName() . 'k',
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

     public function testGetFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'id'        => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testSpacesSuccessfull()
    {
        $this->setupAccount();

        $numberOfSpaces = 2;
        $this->spaceKit->add($numberOfSpaces, $this->spaceDefaultDecorators());
        $spaceCollection = $this->spaceKit->getCollection($this->account->getName());
        $spaces = $spaceCollection->toArray();

        $body = json_encode($spaces);

        $this->request->setMethod('GET');

        $params = array(
            'id'          => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'account',
            'action'      => 'spaces'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();

        $this->assertEquals($body, $result);

        $this->assertResponseCode(200);
    }

    public function testSpacesFails_IfWrongAccount()
    {
        $this->setupAccount();

        $params = array(
            'id'          => $this->account->getName() . 'WrongName',
            'module'      => 'v1',
            'controller'  => 'account',
            'action'      => 'spaces'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

    public function testSpacesFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);
        $this->request->setHeader('accept', NULL);

        $params = array(
            'id'          => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'account',
            'action'      => 'spaces'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testCategoriesSuccessfull()
    {
        $this->setupAccount();

        $numberOfCategories = 2;
        $this->categoryKit->add($numberOfCategories, $this->categoryDefaultDecorators());
        $categoryCollection = $this->categoryKit->getCollection($this->account->getName(), $this->space);
        $categories = $categoryCollection->toArray();

        $body = json_encode($categories);

        $params = array(
            'id'          => $this->account->getName(),
            'module'      => 'v1',
            'action'      => 'categories'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();

        $this->assertEquals($body, $result);

        $this->assertResponseCode(200);
    }

    public function testCategoriesFails_IfWrongAccount()
    {
        $this->setupAccount();

        $params = array(
            'id'          => $this->account->getName() . 'WrongName',
            'module'      => 'v1',
            'controller'  => 'account',
            'action'      => 'categories'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

     public function testCategoriesFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);

        $params = array(
            'id'          => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'account',
            'action'      => 'categories'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testItemsSuccessfull()
    {
        $this->setupAccount();

        $numberOfItems = 2;
        $this->itemKit->add($numberOfItems, $this->defaultDecorators());
        $itemCollection = $this->itemKit->getCollection($this->account->getName(), $this->category->getId());
        $items = $itemCollection->toArray();

        $body = json_encode($items);

        $params = array(
            'id'          => $this->account->getName(),
            'module'      => 'v1',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $result = $this->getResponse()->getBody();

        $this->assertEquals($body, $result);

        $this->assertResponseCode(200);
    }

    public function testItemsFails_IfWrongAccount()
    {
        $this->setupAccount();

        $params = array(
            'id'          => $this->account->getName() . 'WrongName',
            'module'      => 'v1',
            'controller'  => 'account',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $this->assertResponseCode(404);
    }

     public function testItemsFails_IfIncorrectHeader()
    {
        $this->setupAccount();

        $this->request->setHeader('content-type', NULL);

        $params = array(
            'id'          => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'account',
            'action'      => 'items'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restGetAccount');
        $this->dispatch($url);

        $this->assertResponseCode(415);
    }

    public function testOptionsSuccessfull()
    {
        $this->setupAccount();
        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $this->request->setMethod('OPTIONS');

        $params = array(
            'accountName' => $this->account->getName(),
            'module'      => 'v1',
            'controller'  => 'account'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'restAccount');
        $this->dispatch($url);

        $doc = json_decode($this->getResponse()->getBody(), true);

        $this->assertArrayHasKey('GET', $doc);
        $this->assertArrayHasKey('POST', $doc);

        $this->assertArrayHasKey('description', $doc['GET']);
        $this->assertArrayHasKey('description', $doc['POST']);

        $this->assertArrayHasKey('parametrs', $doc['GET']);
        $this->assertArrayHasKey('parametrs', $doc['POST']);

        $this->assertArrayHasKey('example', $doc['GET']);
        $this->assertArrayHasKey('example', $doc['POST']);

        $this->assertResponseCode(200);
    }
}