<?php

class V1_SpaceControllerTest extends TestGenericSuite
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('PHPUnit Framework');

        $suite = self::_loadControllerSuites(
            $suite,
            'v1',
            'Space'
        );

        return $suite;
    }
}
