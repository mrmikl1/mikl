<?php
class Auth_IndexControllerTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testLoginSuccessfull()
    {
        $this->setupAccount();
        $postData = array(
             'email'             => $this->user->getEmail(),
             'password'          => 'password',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $params = array('accountName' => $this->account->getName());
        $params['locale'] = 'en';
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'login');
        $this->dispatch($url);

        $user = Zend_Auth::getInstance()->getStorage()->read();
        $this->assertInstanceOf('User_Model_Domain', $user);

        $this->assertRedirect();

    }


    public function testLoginFails_IfUserNotActive()
    {
        $this->setupAccount(FALSE);
        $postData = array(
             'email'             => $this->user->getEmail(),
             'password'          => 'password',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $params = array('accountName' => $this->account->getName());
        $params['locale'] = 'en';
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'login');
        $this->dispatch($url);

        $user = Zend_Auth::getInstance()->getStorage()->read();
        $this->assertNotInstanceOf('User_Model_Domain', $user);
        $this->assertNotRedirect();

    }

    public function testLoginFails_IfIncorrectEmailOrPassword()
    {
        $this->setupAccount();
        $postData = array(
             'email'             => 'email@ukr.net',
             'password'          => 'pass2',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $params = array('accountName' => $this->account->getName());
        $params['locale'] = 'en';
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'login');
        $this->dispatch($url);

        $user = Zend_Auth::getInstance()->getStorage()->read();
        $this->assertNotInstanceOf('User_Model_Domain', $user);
        $this->assertNotRedirect();

        $this->assertXpath('//div[@class="alert alert-danger"]');
    }

    public function testLoginFails_InAnotherAccount()
    {
        $this->setupAccount();

        $postData = array(
             'email'             => $this->user->getEmail(),
             'password'          => 'password',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $params = array('accountName' => 'invalidAccountName');
        $params['locale'] = 'en';
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'login');
        $this->dispatch($url);

        $user = Zend_Auth::getInstance()->getStorage()->read();
        $this->assertNotInstanceOf('User_Model_Domain', $user);
        $this->assertNotRedirect();

        $this->assertEquals($this->getResponse()->getHttpResponseCode(), 404);

    }

    public function testLogoutSuccess()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array('accountName' => $this->account->getName());
        $params['locale'] = 'en';
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'logout');
        $this->dispatch($url);

        $this->assertRedirect();
        $this->assertEmpty(Zend_Auth::getInstance()->getStorage()->read());

        $this->_redirectToResultURL();

        $this->assertXpath('//div[@class="alert alert-success"]');
    }

    public function testLoginSuccessfullAfterActivation()
    {
        $this->setupAccount(FALSE);

        $paramsActivate = array(
            'accountName' => $this->account->getName(),
            'hash'        => $this->user->getActivationHash());
        $urlParamsActivate = $this->urlizeOptions($paramsActivate);
        $urlActivate = $this->url($urlParamsActivate, 'activation');
        $this->dispatch($urlActivate);


        $postData = array(
             'email'             => $this->user->getEmail(),
             'password'          => 'password',
        );
        $this->request->setMethod('POST')->setPost($postData);
        $params = array('accountName' => $this->account->getName());
        $params['locale'] = 'en';
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'login');
        $this->dispatch($url);

        $user = Zend_Auth::getInstance()->getStorage()->read();

        $this->assertInstanceOf('User_Model_Domain', $user);

        $this->assertRedirect();

    }
    public function testForgotPasswordSuccessfull()
    {
        $this->setupAccount();

        $postData = array('email' => $this->user->getEmail());
        $this->request->setMethod('POST')->setPost($postData);

        $paramsFP = array('accountName' => $this->account->getName());
        $paramsFP['locale'] = 'en';
        $urlParamsFP = $this->urlizeOptions($paramsFP);
        $urlFP = $this->url($urlParamsFP, 'forgotPassword');
        $this->dispatch($urlFP);

        $this->assertRedirect();

        $this->_redirectToResultURL();;

        $this->assertXpath('//div[@class="alert alert-success"]');
    }

    public function testForgotPasswordFails_IfEmptyOrNotExistentOrInvalidEmail()
    {
        $this->setupAccount();

        $postData = array('email' => 'Invalidemail@ukr.net');
        $this->request->setMethod('POST')->setPost($postData);

        $paramsFP = array('accountName' => $this->account->getName());
        $paramsFP['locale'] = 'en';
        $urlParamsFP = $this->urlizeOptions($paramsFP);
        $urlFP = $this->url($urlParamsFP, 'forgotPassword');
        $this->dispatch($urlFP);

        $this->assertXpath('//div[@class="form-group has-error"]/label[@for="email"]');

    }


    public function testResetPasswordSuccessfull()
    {
        $this->setupAccount(true, '67890');

        $paramsRP = array(
            'accountName' => $this->account->getName(),
            'hash'        => $this->user->getforgotPasswordHash()
        );
        $paramsRP['locale'] = 'en';
        $urlParamsRP = $this->urlizeOptions($paramsRP);
        $urlRP = $this->url($urlParamsRP, 'resetPassword');
        $this->dispatch($urlRP);

        $this->_redirectToResultURL();

        $this->assertXpath('//div[@class="alert alert-success"]');
    }

    public function testResetPasswordFails_IfUrlHasIncorrectHash()
    {
        $this->setupAccount(true, '67890');

        $paramsRP = array(
            'accountName' => $this->account->getName(),
            'hash'        => $this->user->getforgotPasswordHash() . "k"
        );
        $paramsRP['locale'] = 'en';
        $urlParamsRP = $this->urlizeOptions($paramsRP);
        $urlRP = $this->url($urlParamsRP, 'resetPassword');
        $this->dispatch($urlRP);

        $this->assertNotRedirect();

        $this->assertXpath('//div[@class="alert alert-danger"]');
    }

}