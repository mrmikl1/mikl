<?php
class Account_IndexControllerTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testRegisterAction()
    {
        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'accName'
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertRedirect();

        $spacesCollection = $this->spaceKit->getCollection($postData['accountName']);
        $this->assertCount(1, $spacesCollection);
        $categoriesCollection = $this->categoryKit->getCollection($postData['accountName'], $spacesCollection[0]);
        $this->assertCount(1, $categoriesCollection);
    }

    public function testRegisterFails_IfAllFieldsEmpty()
    {
         $postData = array(
             'firstName'         => '',
             'lastName'          => '',
             'email'             => '',
             'password'          => '',
             'passwordConfirm'   => '',
             'accountName'       => ''
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertNotRedirect();

        foreach ($postData as $formElementName => $value)
        {
            $this->assertXpathContentContains('//div', 'Value is required and can\'t be empty');
        }
    }

    public function testRegisterFails_IfPasswordsAreNotIndentical()
    {
        $postData = array(
            'firstName'          => 'Igor',
            'lastName'           => 'D',
            'email'              => 'userEmail@ukr.net',
            'password'           => 'pass1',
            'passwordConfirm'    => 'pass2',
            'accountName'        => 'accName'
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);
        $this->assertNotRedirect();
        $this->assertXpathContentContains('//div', 'The two given tokens do not match');

        foreach($postData as $formElementName => $value) {
            if($formElementName != 'password' && $formElementName != 'passwordConfirm')
                $this->assertContains( $value, $this->getResponse()->getBody());
        }
    }

    public function testRegisterFails_IfEmailIsIncorrect()
    {
        $postData = array(
            'firstName'          => 'Igor',
            'lastName'           => 'D',
            'email'              => 'userEmail',
            'password'           => 'pass1',
            'passwordConfirm'    => 'pass1',
            'accountName'        => 'accName'
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertNotRedirect();

        $this->assertXpathContentContains('//div', '\''.$postData['email'].'\' is not a valid email address in the basic format local-part@hostname');
     }

    public function testRegisterFails_IfAccountIsNotUnique()
    {
        $this->setupAccount();

        $postData = array(
            'firstName'        => 'Igor',
            'lastName'         => 'D',
            'email'            => 'ukr@ukr.net',
            'password'         => 'pass1',
            'passwordConfirm'  => 'pass1',
            'accountName'      => $this->account->getName()
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertNotRedirect();

        $this->assertXpathContentContains('//div', 'A record matching \''.$postData['accountName'].'\' was found');
    }

    public function testActivationFails_IfRepeatedlyPushActivationLink()
    {
        $this->setupAccount();

        $params = array('action' => 'activate',
            'controller'  => 'account',
            'accountName' => $this->account->getName(),
            'hash'        => $this->user->getActivationHash()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, "activation");
        $this->dispatch($url);

        $this->assertXpath('//div[@class="alert alert-danger"]');
    }

    public function testActivationFails_IfIncorrectHash()
    {
        $this->setupAccount(FALSE);

        $params = array(
            'accountName' => $this->account->getName(),
            'hash'        => 'incorrectHash'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'activation');
        $this->dispatch($url);

        $this->assertXpath('//div[@class="alert alert-danger"]');
    }

    public function testActivationFails_IfIncorrectAccount()
    {
        $this->setupAccount(FALSE);

        $params = array(
            'accountName' => 'incorrectName',
            'hash'        => $this->user->getActivationHash()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams,'activation');
        $this->dispatch($url);

        $this->assertEquals($this->getResponse()->getHttpResponseCode(), 404);
    }

    public function testActivationSuccessfull()
    {
        $this->setupAccount(FALSE);

        $params = array(
            'accountName' => $this->account->getName(),
            'hash'        => $this->user->getActivationHash()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams,'activation');
        $this->dispatch($url);

        $this->assertXpath('//div[@class="alert alert-success"]');
    }
    public function testRegisterFails_IfAccountNameIsIncorrect()
    {
        //Array

        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'support'
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertNotRedirect();
        $this->assertXpathContentContains('//div/div/div', 'Already exists');

        //NS

        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'ns1'
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertNotRedirect();
        $this->assertXpathContentContains('//div/div/div', 'Account Name cannot contain group of characters like: ns1, ns2a and so one');

        //Less than 3 symbols

        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => 'mn'
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertNotRedirect();
        $this->assertXpathContentContains('//div/div/div', '\'' . $postData['accountName'] . '\' is less than 3 characters long');

        //Krakozyabra's

        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'password'          => 'pass',
            'passwordConfirm'   => 'pass',
            'accountName'       => '/}{*фвы+='
        );

        $this->request->setMethod('POST')->setPost($postData);
        $url = $this->url(array(), 'register');
        $this->dispatch($url);

        $this->assertNotRedirect();
        $this->assertXpathContentContains('//div/div/div', 'Account Name may contain only: a-z, 0-9');
    }
}

