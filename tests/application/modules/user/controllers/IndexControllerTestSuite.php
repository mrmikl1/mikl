<?php

class User_IndexControllerTest extends TestGenericSuite
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('PHPUnit Framework');

        $suite = self::_loadControllerSuites(
            $suite,
            'Space'
        );

        return $suite;
    }
}
