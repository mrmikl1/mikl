<?php
class User_Index_ChangePasswordActionTest extends TestGenericController
{
    public function setUp()
        {
            parent::setUp();
        }

    public function testChangePasswordSuccessfull()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'currentPassword' => 'password',
            'password'        => 'pass',
            'confirmPassword' => 'pass',
            'action'          => 'ChangePassword',
            'id'              => $this->user->getId()
        );

        $this->request->setMethod('POST')->setPost($postData);

        $params = array(
            'accountName' => $this->account->getName(),
            'locale'      => 'en',
            'id'          => $this->user->getId()
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'changePassword');
        $this->dispatch($url);

        $this->assertRedirect();

        $this->_redirectToResultURL();

        $this->assertXpath('//div[@class="alert alert-success"]');
    }
    public function testChangePasswordFails_IfOldPasswordIsWrong()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'currentPassword'    => 'WrongOldPassword',
            'password'           => 'pass',
            'confirmPassword'    => 'pass',
            'action'             => 'ChangePassword',
            'id'                 => $this->user->getId()
        );

        $this->request->setMethod('POST')->setPost($postData);

        $params = array(
            'accountName' => $this->account->getName(),
            'locale'      => 'en',
            'id'          => $this->user->getId()
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'changePassword');
        $this->dispatch($url);

        $this->assertXpath('//div[@class="alert alert-danger"]');
    }
}
