<?php
class User_Index_UserPreferencesActionTest extends TestGenericController
{
    public function setUp()
        {
            parent::setUp();
        }
    public function testUserPreferencesChangeSuccessfull()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $postData = array(
            'firstName'         => 'Igor',
            'lastName'          => 'D',
            'email'             => 'userEmail@ukr.net',
            'action'            => 'Save'
        );

        $this->request->setMethod('POST')->setPost($postData);

        $params = array(
            'accountName' => $this->account->getName(),
            'locale'      => 'en',
            'id'          => $this->user->getId()
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'userPreferences');
        $this->dispatch($url);

        $this->assertRedirect();

        $this->_redirectToResultURL();

        $this->assertXpath('//div[@class="alert alert-success"]');
    }

    public function testUserPreferencesError_IfUserNotExist()
    {
        $this->setupAccount();

        Zend_Auth::getInstance()->getStorage()->write($this->user);

        $params = array(
            'accountName' => $this->account->getName(),
            'locale'      => 'en',
            'id'          => $this->user->getId()+1
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'userPreferences');
        $this->dispatch($url);

        $this->assertEquals($this->getResponse()->getHttpResponseCode(), 404);
    }

}
