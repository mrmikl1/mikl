<?php

class Default_Item_AskQuestionActionTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testFormOpenSuccessfully()
    {
        $this->setupAccount();

        $params = array(
           'accountName' => $this->account->getName(),
           'language'    => 'en'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'askQuestion');
        $this->dispatch($url);

        $this->assertController('item');
        $this->assertModule('default');

        $this->assertAction('ask-question');
        $this->assertXpath('//select[@name="spaceId"]');
        $this->assertXpath('//input[@name="categoryId" and @type="hidden" and @value="' . $this->category->getId() . '"]');
        $this->assertXpath('//input[@name="title"]');
        $this->assertXpath('//textarea[@name="content"]');
    }

    public function testFormTitle_IsEmpty()
    {
        $this->setupAccount();

        $params = array(
           'accountName' => $this->account->getName(),
           'language'    => 'en'
        );


        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '',
             'content'          => 'content',
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'askQuestion');

        $this->dispatch($url);

        $this->assertController('item');
        $this->assertModule('default');

        $this->assertNotRedirect();
        $this->assertXpath('//div[@class="form-group has-error"]/label[@for="title"]');
        $this->assertXpathContentContains('//div/select/option[@selected="selected"]', $this->space->getName());
        $this->assertXpath('//input[@name="categoryId" and @type="hidden" and @value="' . $this->category->getId() . '"]');
        $this->assertXpathContentContains('//div/textarea[@name="content"]', $postData['content']);
    }

    public function testFormSaveData()
    {
        $this->setupAccount();

        $params = array(
           'accountName' => $this->account->getName(),
           'language'    => 'en'
        );


        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'askQuestion',
             'content'          => 'content',
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'askQuestion');

        $this->dispatch($url);

        $this->assertRedirect();

        $this->_redirectToResultURL();
        $this->assertXpath('//div[@class="alert alert-success"]');
    }

    public function testCategory_NotContentUnansweredQuestion()
    {
        $this->setupAccount();

        $params = array(
           'accountName' => $this->account->getName(),
           'language'    => 'en'
        );

        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => 'askQuestion',
             'content'          => 'content',
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'askQuestion');

        $this->dispatch($url);

        $this->assertRedirect();

        $this->_redirectToResultURL();
        $this->assertNotXpathContentContains("//div[@class='row']/ol/li/h4", $postData['title']);
    }

    public function testOpenFormFewCategories()
    {
        $this->setupAccount();
        $this->categoryKit->saveDomain($this->account, $this->space);

        $params = array(
           'accountName' => $this->account->getName(),
           'language'    => 'en'
        );

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'askQuestion');
        $this->dispatch($url);

        $this->assertController('item');
        $this->assertModule('default');
        $this->assertAction('ask-question');
        $this->assertXpath('//select[@name="spaceId"]');
        $this->assertXpath('//select[@name="categoryId"]');
        $this->assertXpath('//input[@name="title"]');
        $this->assertXpath('//textarea[@name="content"]');
    }

    public function testFormTitleFewCategories_IsEmpty()
    {
        $this->setupAccount();
        $this->categoryKit->saveDomain($this->account, $this->space);

        $params = array(
           'accountName' => $this->account->getName(),
           'language'    => 'en'
        );


        $postData = array(
             'spaceId'          => $this->space->getId(),
             'categoryId'       => $this->category->getId(),
             'title'            => '',
             'content'          => 'content',
             'fullName'         => 'ololo',
             'email'            => 'blah@blah.com',
        );

        $this->request->setMethod('POST')->setPost($postData);

        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'askQuestion');

        $this->dispatch($url);

        $this->assertController('item');
        $this->assertModule('default');

        $this->assertNotRedirect();
        $this->assertXpath('//div[@class="form-group has-error"]/label[@for="title"]');
        $this->assertXpathContentContains('//div/select/option[@selected="selected"]', $this->space->getName());
        $this->assertXpathContentContains('//div/select[@name="categoryId"]/option[@selected="selected"]', $this->category->getName());
        $this->assertXpathContentContains('//div/textarea[@name="content"]', $postData['content']);
    }
}
