<?php

class Default_IndexControllerTest extends TestGenericController
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexSuccessfull()
    {
        $this->setupAccount();
        $this->_createCategory();

        $numberOfSpaces = 3;
        $numberOfCategories = 2;
        $numberOfItems = 3;

        $this->spaceKit->add($numberOfSpaces, $this->spaceDefaultDecorators());
        $spaces = $this->spaceKit->getCollection($this->account->getName());

        foreach ($spaces as $space) {
            /* @var $space Space_Model_Domain
             * @var $category Category_Model_Domain*/
            $this->categoryKit->add($numberOfCategories, $this->categoryDefaultDecorators());
            $categories[$space->getId()] = $this->categoryKit->getCollection($this->account->getName(), $space);
            foreach ($categories[$space->getId()] as $category){
                $this->itemKit->add($numberOfItems, $this->defaultDecorators());

                $items[$category->getId()] = $this->itemKit->getCollection($this->account->getName(), $category->getId());
            }
        }

        $params = array(
            'locale'       => 'en',
            'accountName'  => $this->account->getName()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');
        $this->dispatch($url);

        $this->assertXpath('//div');
        $this->assertXpathCount('//div/div/div/ul/li', count($spaces));
        foreach ($spaces as $space) {
            $this->assertXpathContentContains('//div/div/div/ul/li/a', $space->getName());
        }
        $this->assertXpathContentContains('//div/div/div/div/div/h1', $spaces[0]->getName());

        foreach ($categories[$spaces[0]->getId()] as $category) {
            $this->assertXpathContentContains('//div/div/div/div/div/div/h2', $category->getName());
            foreach ($items[$category->getId()] as $item){
                $this->assertXpathContentContains('//div/div/div/div/div/div/ol/li/a', $item->getTitle());
            }
        }
    }

    public function testIndex_IfFewCategories()
    {
        $this->setupAccount();
        $this->categoryKit->saveDomain($this->category);

        $params = array(
            'locale'       => 'en',
            'accountName'  => $this->account->getName()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');
        $this->dispatch($url);
        $this->assertXpathContentContains(
                '//div[@class="row"]/div/h2/a[@href="' . $url . "/" . $this->space->getId() . "#c" . $this->category->getId() . '"]', $this->category->getName());
        $this->assertXpathContentContains('//h2[@id="c' . $this->category->getId() . '"]', $this->category->getName());
    }

    public function testIndexSuccessfull_IfOneSpace()
    {
        $this->setupAccount();
        $params = array(
            'locale'       => 'en',
            'accountName'  => $this->account->getName()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');
        $this->dispatch($url);

        $this->assertNotXpath('//div/p/span[@style="color: red; font-size: 2em"]');
    }

    public function testIndexSuccessfull_IfOneCategory()
    {
        $this->setupAccount();
        $params = array(
            'locale'       => 'en',
            'accountName'  => $this->account->getName()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');
        $this->dispatch($url);

        $this->assertNotXpath('//div/p/span');
    }


    public function testIndexFails_IfNoSpace()
    {
        $this->setupAccount();
        $params = array(
            'locale'       => 'en',
            'accountName'  => $this->account->getName(),
            'id'           => $this->space->getId() + 1
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');
        $this->dispatch($url);

        $this->assertEquals($this->getResponse()->getHttpResponseCode(), 404);
    }

    public function testIndexFails_IfNoAccount()
    {
        $this->setupAccount();
        $params = array(
            'locale'       => 'en',
            'accountName'  => 'wrongAccount'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');
        $this->dispatch($url);

        $this->assertEquals($this->getResponse()->getHttpResponseCode(), 404);
    }

    public function testCountAnsweredQuestions()
    {
        $this->setupAccount();

        $decorator = new ObjectCreator_Item_Decorated_Publishing(
                    new ObjectCreator_Item_Decorated_Status(
                            $this->defaultDecorators(),
                            Item_Model_Domain::STATUS_ANSWERED
                        ),Item_Model_Domain::PUBLISHED
                    );

        $collection = $this->itemKit->add(4, $decorator);
        $this->itemKit->add(5, $this->defaultDecorators());

        $params = array(
            'locale'       => 'en',
            'accountName'  => $this->account->getName()
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');

        $this->dispatch($url);

        $this->assertXpathCount("//li/h4", $collection->count());
    }

    public function testLanguageSelectorExists()
    {
        $this->setupAccount();

        $params = array(
            'accountName'  => $this->account->getName(),
            'locale'       => 'en'
        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');

        $this->dispatch($url);

        $this->assertXpath('//select[@name="language"]');
        $this->assertXpathContentContains('//select[@name="language"]/option[@selected = "selected"]', 'English');
    }
       public function testLanguageCookiesExists()
    {
        $this->setupAccount();

        $params = array(
            'accountName'  => $this->account->getName(),
            'locale'       => 'en'

        );
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams, 'viewSpaceFrontend');

        $this->dispatch($url);

        $cookies = $this->getRequest()->getCookie();

        $this->assertNotEmpty($cookies['locale']);
        $this->assertXpath('//select[@name="language"]');
        $this->assertXpathContentContains('//select[@name="language"]/option[@selected = "selected"]', $cookies['locale']);
    }
}
