<?php

class ItemControllerTest extends TestGenericSuite
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('PHPUnit Framework');

        $suite = self::_loadControllerSuites(
            $suite,
            'Default',
            'Item'
        );

        return $suite;
    }
}



