DROP TABLE IF EXISTS `items`;
DROP TABLE IF EXISTS `categories`;
DROP TABLE IF EXISTS `spaces`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `accounts`;
DROP TABLE IF EXISTS items_translations;
DROP TABLE IF EXISTS categories_translations;
DROP TABLE IF EXISTS spaces_translations;

CREATE TABLE `accounts` (
`account_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` TEXT NOT NULL,
`created` DATETIME NOT NULL,
`space_id` INT(10) UNSIGNED NOT NULL,
`category_id` INT(10) UNSIGNED NOT NULL,
`item_id` INT(10) UNSIGNED NOT NULL,
`language` CHAR(2) NOT NULL DEFAULT 'en',
`language_front` CHAR(2) NOT NULL DEFAULT 'en',
PRIMARY KEY (`account_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`account_id` int(10) unsigned NOT NULL,
`email` text NOT NULL,
`first_name` text NOT NULL,
`last_name` text NOT NULL,
`password` varchar(250) NOT NULL,
`activation_hash` varchar(250) NOT NULL,
`active` enum('Y','N') NOT NULL DEFAULT 'N',
`forgot_password_hash` varchar(250) DEFAULT NULL,
`created` DATETIME NOT NULL,
`edited` DATETIME NOT NULL,
PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `spaces` (
`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`space_id` INT(10) UNSIGNED NOT NULL,
`account_id` INT(10) UNSIGNED NOT NULL,
`name` TEXT NOT NULL,
`description` TEXT NULL,
`default` ENUM('Y','N') NOT NULL DEFAULT 'N',
`position` INT(10) UNSIGNED NULL,
`created` DATETIME NOT NULL,
`edited` DATETIME NOT NULL,
PRIMARY KEY (`space_id`, `account_id`),
INDEX `id` (`id`),
INDEX `FK_spaces_accounts` (`account_id`),
CONSTRAINT `FK_spaces_accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TRIGGER `space_increment`
BEFORE INSERT ON `spaces` FOR EACH ROW
BEGIN DECLARE newId INT; SELECT `space_id`+1 INTO newId FROM `accounts` WHERE `account_id` = NEW.account_id; UPDATE `accounts` SET `space_id` = newId WHERE `account_id` = NEW.account_id; SET NEW.space_id = newId; END;

CREATE TABLE `categories` (
`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`category_id` INT(10) UNSIGNED NOT NULL,
`account_id` INT(10) UNSIGNED NOT NULL,
`space_id` INT(10) UNSIGNED NOT NULL,
`name` TEXT NOT NULL,
`description` TEXT NULL,
`default` ENUM('Y','N') NOT NULL DEFAULT 'N',
`position` INT(10) UNSIGNED NULL,
`created` DATETIME NOT NULL,
`edited` DATETIME NOT NULL,
PRIMARY KEY (`category_id`, `account_id`),
INDEX `id` (`id`),
INDEX `FK_categories_spaces` (`space_id`),
INDEX `FK_categories_accounts` (`account_id`),
CONSTRAINT `FK_categories_accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`) ON DELETE CASCADE,
CONSTRAINT `FK_categories_spaces` FOREIGN KEY (`space_id`) REFERENCES `spaces` (`space_id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TRIGGER `category_increment`
BEFORE INSERT ON `categories` FOR EACH ROW
BEGIN DECLARE newId INT; SELECT `category_id`+1 INTO newId FROM `accounts` WHERE `account_id` = NEW.account_id; UPDATE `accounts` SET `category_id` = newId WHERE `account_id` = NEW.account_id; SET NEW.category_id = newId; END;

CREATE TABLE `items` (
`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`item_id` INT(10) UNSIGNED NOT NULL,
`account_id` INT(10) UNSIGNED NOT NULL,
`space_id` INT(10) UNSIGNED NOT NULL,
`category_id` INT(10) UNSIGNED NOT NULL,
`title` TEXT NOT NULL,
`content` TEXT NOT NULL,
`position` INT(10) UNSIGNED NULL DEFAULT NULL,
`created` DATETIME NOT NULL,
`edited` DATETIME NOT NULL,
`status` ENUM('answered','unanswered') NOT NULL DEFAULT 'unanswered',
`full_name` VARCHAR(255) NULL,
`email` VARCHAR(255) NULL,
`published` ENUM('Y','N') NOT NULL DEFAULT 'N',
PRIMARY KEY (`item_id`, `account_id`),
INDEX `FK_items_accounts` (`account_id`),
INDEX `FK_items_categories` (`category_id`),
INDEX `FK_items_spaces` (`space_id`),
INDEX `id` (`id`),
CONSTRAINT `FK_items_accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`) ON DELETE CASCADE,
CONSTRAINT `FK_items_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE,
CONSTRAINT `FK_items_spaces` FOREIGN KEY (`space_id`) REFERENCES `spaces` (`space_id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TRIGGER `item_increment`
BEFORE INSERT ON `items` FOR EACH ROW
BEGIN DECLARE newId INT; SELECT `item_id`+1 INTO newId FROM `accounts`  WHERE `account_id` = NEW.account_id; UPDATE `accounts` SET `item_id` = newId WHERE `account_id` = NEW.account_id; SET NEW.item_id = newId; END;

CREATE TABLE `spaces_translations` (
`space_id` INT(10) NOT NULL,
`account_id` INT(10) NOT NULL,
`language_code` CHAR(5) NOT NULL,
`name` VARCHAR(255) NOT NULL,
`description` VARCHAR(255) NULL DEFAULT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `spaces_translations`
ADD PRIMARY KEY (`space_id`, `account_id`,`language_code`);

CREATE TABLE `categories_translations` (
`category_id` INT(10) NOT NULL,
`account_id` INT(10) NOT NULL,
`language_code` CHAR(5) NOT NULL,
`name` VARCHAR(255) NOT NULL,
`description` VARCHAR(255) NULL DEFAULT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `categories_translations`
ADD PRIMARY KEY (`category_id`, `account_id`,`language_code`);

CREATE TABLE `items_translations` (
	`item_id` INT(10) UNSIGNED NOT NULL,
	`account_id` INT(10) UNSIGNED NOT NULL,
	`language_code` CHAR(5) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`content` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`item_id`, `account_id`, `language_code`),
	CONSTRAINT `FK_items_translations_items` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON UPDATE NO ACTION ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE `languages` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL DEFAULT '0',
	`code` VARCHAR(10) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO languages (name,code)
    VALUES ('English','en'),
           ('Русский','ru'),
           ('Українська','uk');

ALTER TABLE `spaces_translations`
    ALTER `space_id` DROP DEFAULT;
    ALTER TABLE `spaces_translations`
CHANGE COLUMN `space_id` `space_id` INT(10) UNSIGNED NOT NULL FIRST,
    ADD CONSTRAINT `FK_spaces_translations_spaces` FOREIGN KEY (`space_id`) REFERENCES `spaces` (`space_id`) ON UPDATE NO ACTION ON DELETE CASCADE;