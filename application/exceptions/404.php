<?php
class Application_Exception_404 extends Exception
{
    public function __construct()
    {
        $translate  = Zend_Registry::get('Zend_Translate');
        $moduleName = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
        $textError  = $translate->_('Page not found');
        if($moduleName === "v1") {
            $message = json_encode(array(
                'error' => $textError
            ));
        } else {
            $message = $textError;
        }
        $code = 404;
        parent:: __construct($message, $code, null);
    }
}
