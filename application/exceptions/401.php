<?php
class Application_Exception_401 extends Exception
{
    public function __construct()
    {
        $translate  = Zend_Registry::get('Zend_Translate');
        $moduleName = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
        $textError  = $translate->_('Unauthorized Request. Please log in.!');
        if($moduleName === "v1") {
            $message = json_encode(array(
                'error' => $textError
            ));
        } else {
            $message = $textError;
        }
        $code = 401;
        parent:: __construct($message, $code, null);
    }
}
