<?php

class Language_Model_Mapper
{
    public function createArrayFromRow($row)
    {
        $data = array(
            'id'           => $row->id,
            'name'         => $row->name,
            'code'         => $row->code,
        );
        return $data;
    }

    public function getAllLanguages()
    {
        $dbTable = new Language_Model_DbTable();

        $rowSet = $dbTable->fetchAll($dbTable->select());
        $languages = array();
        foreach ($rowSet as $row) {
            $languages[$row['code']] = $this->createArrayFromRow($row);
        }

       return new Application_Collection_Generic($languages);
    }

    public function getCollection(Account_Model_Domain $account)
    {
        $dbTable = new Language_Model_DbTable();
        $dbItemsTranslations = new Item_Model_TranslationsDbTable();

        $query = $dbTable
            ->select()
            ->from($dbTable->info(Zend_Db_Table_Abstract::NAME))
            ->setIntegrityCheck(false)
            ->joinLeft(
                array('it' => $dbItemsTranslations
                    ->select()
                    ->from($dbItemsTranslations->info(Zend_Db_Table_Abstract::NAME), array('count' => 'count(*)', 'language_code'))
                    ->where($dbItemsTranslations->getAdapter()->quoteInto('account_id = ?', $account->getId()))
                    ->group('language_code')),
                'languages.code = it.language_code')
            ->order('count desc');
        $rowSet = $dbTable->fetchAll($query);

        $languages = array();
        foreach ($rowSet as $row) {
            $languages[$row['code']] = $this->createArrayFromRow($row);
        }

       return new Application_Collection_Generic($languages);
    }
}