<?php

class Error_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout->setLayout("admin-one-column");
        $this->_translate = Zend_Registry::get('Zend_Translate');
    }

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = $this->_translate->_('You have reached the error page');
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = $this->_translate->_('Page not found');
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = $this->_translate->_('Application error');
                break;
        }

        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->log($this->view->message, $priority, $errors->exception);
            $log->log('Request Parameters', $priority, $errors->request->getParams());
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request   = $errors->request;
    }

    public function error404Action()
    {
        if ($this->getRequest()->getParam('currentModule') == 'default') {
            $this->_helper->layout->setLayout("layout");
        }
        $this->view->type = '404';
        $this->view->message = $this->_translate->_('Page not found');
        $this->view->accName = $this->getParam('accountName');
        $this->view->module = $this->getRequest()->getParam('module');
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $this->_getParam('error_handler');
        }
        $this->getRequest()->setParam('error_handler', NULL);
        $this->view->request = $this->getRequest();
        $this->_helper->viewRenderer->render('error');
    }

    public function error403Action()
    {
        $errors = $this->_getParam('error_handler');
        $this->view->type = '403';
        $this->view->message = $this->_translate->_('You haven\'t permission');
        if ($this->getParam('accountName')) {
            $this->view->accName = $this->getParam('accountName');
    	}
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        $this->view->request = $errors->request;
        $this->_helper->viewRenderer->render('error');
    }

    public function error415Action()
    {
        $this->view->type = '415';
        $this->view->message = $this->_translate->_('Incorrect header');
        if ($this->getParam('accountName')){
            $this->view->accName = $this->getParam('accountName');
        }
        $this->view->request = $errors->request;
        $this->_helper->viewRenderer->render('error');
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

