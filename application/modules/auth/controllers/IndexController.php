<?php

class Auth_IndexController extends Zend_Controller_Action
{
    /**
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;

    public function init()
    {
        $this->_flashMessenger =
            $this->_helper->getHelper('FlashMessenger');

        $this->_helper->layout->setLayout("admin-one-column");

        $this->view->flashMessenger = $this->_flashMessenger;
        $this->_translate = Zend_Registry::get('Zend_Translate');
    }


    public function loginAction()
    {
        $urlRedirect = $this->getRequest()->getCookie('redirect');

        $accountName = $this->getParam('accountName');
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $authService = new Application_Service_Auth();
            $result = $authService->login($data, $accountName);
            if ($result instanceof Auth_Form_Login) {
                $loginForm = $result;
                $this->_flashMessenger->addMessage($this->_translate->_("Invald email or password"), "error");
            } elseif ($result instanceof User_Model_Domain) {
                if ($urlRedirect) {
                    setcookie('redirect', $urlRedirect, time()-3600, '/');
                    return $this->_redirect($urlRedirect);
                } else {
                    $url = $this->_helper->url->url(array('accountName' => $accountName), 'admin');
                    return $this->_redirect($url);
                }
            }
        } else {
            $loginForm = new Auth_Form_Login();
        }

        $this->view->form = $loginForm;
    }
    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $accountName = $this->getParam('accountName');
        $url = $this->_helper->url->url(array('accountName' => $accountName), 'login');
        $this->_flashMessenger->addMessage($this->_translate->_("You've logged out successfully."), "success");
        return $this->_redirect($url);
    }

    public function forgotPasswordAction()
    {
        $accountName = $this->getParam('accountName');

        $resetPasswordForm = new Auth_Form_ResetPassword($this->getAllParams());

        if($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            if($resetPasswordForm->isValid($data)) {

                $accountMapper = new Account_Model_Mapper();
                $account       = $accountMapper->findByName($accountName);

                $userMapper = new User_Model_Mapper();
                $user = $userMapper->findByEmail($account, $data['email']);
                $user->setForgotPasswordHash(md5(time()));
                $userMapper->save($user);
                if (APPLICATION_ENV != 'testing') {
                    $config = Zend_Registry::get('config');
                    $configEmail = array(
                        'auth'      => $config->get('email')->auth,
                        'username'  => $config->get('email')->username,
                        'password'  => $config->get('email')->password,
                        'ssl'       => $config->get('email')->ssl,
                        'port'      => $config->get('email')->port
                    );

                    $transport = new Zend_Mail_Transport_Smtp($config->get('email')->hostname, $configEmail);

                    $forgotPasswordUrl = $this->_helper->url->url(array(
                        'hash'         => $user->getForgotPasswordHash()),
                        'resetPassword'
                    );
                    $mail = new Zend_Mail('UTF-8');
                    $mail->setBodyText(
                        "If you want to change your password, go to URL: " . $forgotPasswordUrl);
                    $mail->setFrom($config->get('email')->fromEmail, $config->get('email')->fromFullName);
                    $mail->addTo($data['email'], $user->getFirstName() . " " . $user->getLastName());
                    $mail->setSubject('Change password.');
                    $mail->send($transport);
                }

                $this->_flashMessenger->addMessage($this->_translate->_("Please check your email inbox with further password recovery instructions"), "success");

                $url = $this->_helper->url->url(array('accountName' => $accountName), 'login');
                return $this->_redirect($url);
            }
        }

        $this->view->form = $resetPasswordForm;
    }

    public function resetPasswordAction()
    {
        $accountName = $this->getParam('accountName');
        $forgotPasswordHash = $this->getParam('hash');
        $accountMapper = new Account_Model_Mapper();

        $account = $accountMapper->findByName($accountName);
        $userMapper = new User_Model_Mapper();
        $user = $userMapper->findByAccountAndPasswordHash($account, $forgotPasswordHash);
        if($user){
            $user->setForgotPasswordHash(NULL);

            $newPassword = substr(md5(time()), 0, 7);
            $user->setPassword(md5($newPassword));
            $userMapper->save($user);

            if (APPLICATION_ENV != 'testing') {
                $config = Zend_Registry::get('config');
                $configEmail = array(
                    'auth'      => $config->get('email')->auth,
                    'username'  => $config->get('email')->username,
                    'password'  => $config->get('email')->password,
                    'ssl'       => $config->get('email')->ssl,
                    'port'      => $config->get('email')->port
                );

                $transport = new Zend_Mail_Transport_Smtp($config->get('email')->hostname, $configEmail);
                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyText(
                    "Your new password: " . $newPassword);
                $mail->setFrom($config->get('email')->fromEmail, $config->get('email')->fromFullName);
                $mail->addTo($user->getEmail(), $user->getFirstName() . " " . $user->getLastName());
                $mail->setSubject('New password');
                $mail->send($transport);
            }
            $this->_flashMessenger->addMessage($this->_translate->_("New password has been sent to your email"), "success");

            $url = $this->_helper->url->url(array('accountName' => $accountName), 'login');

            return $this->_redirect($url);
        } else {
            $this->_flashMessenger->addMessage($this->_translate->_("Incorrect reset password url"), "error");
        }

    }
}
