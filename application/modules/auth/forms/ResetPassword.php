<?php

class Auth_Form_ResetPassword extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->addElementPrefixPath('Application_Form_Validate', APPLICATION_PATH . '/forms/Validate/', 'validate');

        $this->setMethod('Post');

        $email = new Zend_Form_Element_Text('email', $this->_getElementDefaultOptions());
        $email->setLabel($this->_translate->_('Email'))
              ->setRequired()
              ->setValidators(
                    array(
                        array('EmailAddress'),
                        array(
                            'validator' => 'EmailExists',
                            'options'   => $this->_getParams())
                    )
                );

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Post'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit');

        $this->addElements(array($email, $submit));
    }
}
