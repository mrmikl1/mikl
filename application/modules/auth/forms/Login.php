<?php

class Auth_Form_Login extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->setMethod('Post');

        $email = new Zend_Form_Element_Text('email', $this->_getElementDefaultOptions());
        $email->setLabel($this->_translate->_('Email'))
              ->setRequired()
              ->setValidators(array(array('EmailAddress')));

        $password = new Zend_Form_Element_Password('password', $this->_getElementDefaultOptions());
        $password->setLabel($this->_translate->_('Password'))
                 ->setRequired();

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Login'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit');

        $this->addElements(array($email, $password, $submit));
    }
}
