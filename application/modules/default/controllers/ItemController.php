<?php

class ItemController extends Zend_Controller_Action
{
    protected $_flashMessenger = null;

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flashMessenger = $this->_flashMessenger;
        $this->_translate = Zend_Registry::get('Zend_Translate');
    }

    public function askQuestionAction()
    {
        $askQuestionForm = new Default_Form_AskQuestion($this->getAllParams());
        if ($this->getRequest()->isPost()) {
            $accountName = $this->getParam('accountName');
            $data = $this->getRequest()->getPost();
            $data['status']    = Item_Model_Domain::STATUS_UNANSWERED;
            $data['published'] = Item_Model_Domain::UNPUBLISHED;

            $itemService = new Application_Service_Items();
            $itemService = new Application_Service_Decorated_Email_AskQuestion($itemService);
            $itemService = new Application_Service_Decorated_Validation_AskQuestion($itemService);
            $result = $itemService->save($accountName, $data);

            if ($result instanceof Item_Model_Domain) {
                $url = $this->_helper->url->url(array(
                    'accountName' => $accountName,
                    'id'     => $result->getSpaceId()
                ), 'viewSpaceFrontend');
                $this->_flashMessenger->addMessage($this->_translate->_("Item saved"), "success");
                return $this->_redirect($url);
            } else {
                $askQuestionForm = $result;
            }
        }
        $this->view->askQuestionForm = $askQuestionForm;
    }
}
