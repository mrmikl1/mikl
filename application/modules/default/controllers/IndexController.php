<?php

class IndexController extends Zend_Controller_Action
{
     /**
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;

    public function init()
    {
        $this->_flashMessenger =
        $this->_helper->getHelper('FlashMessenger');

        $this->_helper->layout->setLayout("layout");

        $this->view->flashMessenger = $this->_flashMessenger;
        $this->_translate = Zend_Registry::get('Zend_Translate');
        $this->_locale = Zend_Registry::get('locale');
    }

    public function indexAction()
    {
        if ($this->getRequest()->getParam('action') == 'integrate') {
            $this->_helper->layout->setLayout("integrate");
        }
        $accountName   = $this->getParam('accountName');
        $spaceId       = $this->getParam('id');

        $spaceService     = new Application_Service_Spaces();
        $spacesCollection = $spaceService->getCollection($accountName, $this->_locale);
        $spacesCollection->setSelected($spaceId);

        $categoryService      = new Application_Service_Categories();
        $categoriesCollection = $categoryService->getCollection($accountName, $spacesCollection->getSelected()->getId(), $this->_locale);

        $itemsCollection = array();
        $itemService     = new Application_Service_Items();
        foreach ($categoriesCollection as $category) {
            $itemsCollection[$category->getId()] = $itemService->getCollection($accountName, $spaceId, $category->getId(), $this->_locale);
        }

        $this->view->spaces          = $spacesCollection;
        $this->view->categories      = $categoriesCollection;
        $this->view->items           = $itemsCollection;
        $this->view->askQuestionForm = new Default_Form_AskQuestion(array('accountName' => $accountName, 'spaceId' => $spaceId));
    }
    public function integrateAction()
    {
        $this->forward('index');
    }
}

