<?php

class Default_Form_AskQuestion extends Application_Abstract_Form
{
    public function initForm()
    {
        $urlHelper = new Zend_View_Helper_Url();
        $this->setMethod('Post');
        $this->setAction($urlHelper->url(array(), 'askQuestion'));

        $space        = new Application_Form_Element_SpaceSelect('spaceId', $this->_getElementDefaultOptions());
        $spaceOptions = $space->getOptions($this->_getParam('accountName'));

        $spaceId = !$this->_getParam('spaceId') ? $spaceOptions[0]['key'] : $this->_getParam('spaceId');
        
        $space->setLabel($this->_translate->_('Space*'))
              ->setMultiOptions(
                    $spaceOptions
              )
              ->setValue($spaceId)
              ->setRequired();

        $category = new Application_Form_Element_CategorySelect('categoryId', $this->_getElementDefaultOptions());

        $categories = $category->getOptions(
                        $this->_getParam('accountName'),
                        $spaceId
                    );

        if(count($categories) < 2) {
            $category = new Zend_Form_Element_Hidden('categoryId');
            $category->setDecorators( // clears default decorators
                array(
                    array('ViewHelper') // display only element without decoration
                )
            );
        } else {
            $category->setLabel($this->_translate->_('Category*'))
                     ->setMultiOptions($categories);
        }

        $category->setValue(!$this->_getParam('categoryId') ? $categories[0]['key'] : $this->_getParam('categoryId'))
                 ->setRequired();

        $fullName = new Zend_Form_Element_Text('fullName', $this->_getElementDefaultOptions());
        $fullName->setLabel($this->_translate->_('Name'));

        $email = new Zend_Form_Element_Text('email', $this->_getElementDefaultOptions());
        $email->setLabel($this->_translate->_('Email'))
              ->addValidator(new Zend_Validate_EmailAddress());

        $title = new Zend_Form_Element_Text('title', $this->_getElementDefaultOptions());
        $title->setLabel($this->_translate->_('Question*'))
              ->setRequired()
              ->setAttrib('autofocus', 'true');

        $content = new Zend_Form_Element_Textarea('content', $this->_getElementDefaultOptions());
        $content->setLabel($this->_translate->_('Details'))
                ->setAttribs(array(
                        'rows' => '10',
                        'cols' => '40'
                     ));

        $this->addElements(array($space, $category, $fullName, $email, $title, $content));

        if (APPLICATION_ENV == 'production' || APPLICATION_ENV == 'staging') {
            $recaptcha  = new Zend_Service_ReCaptcha(
                '6LfZxskSAAAAABH11vDiY1UdmiBGWoJRWmfATQUv',
                '6LfZxskSAAAAAMG7aReUmDAFC_OVXcdq7wyUOQq2',
                null,
                array('theme' => 'clean')
                );
            $captcha = new Zend_Form_Element_Captcha('recaptcha_response_field',
                    array('captcha'        => 'ReCaptcha',
                          'captchaOptions' => array('captcha' => 'ReCaptcha', 'service' => $recaptcha),
                          'ignore'         => true));
            $captcha->addDecorators(
                            array(
                                array('HtmlTag',
                                    array(
                                        'tag'   => 'div',
                                        'class' => 'col-lg-10 col-lg-offset-2',
                                        'style' => 'margin-bottom:15px')
                                )
                            )
                        );
            $this->addElement($captcha);
        }

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Ask'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit');

        $this->addElement($submit);
    }
}

