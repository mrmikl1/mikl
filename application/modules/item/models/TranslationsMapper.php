<?php

class Item_Model_TranslationsMapper
{
    public function find($id, $accountId, $spaceId)
    {
        $dbTable = new Item_Model_TranslationsDbTable();
        $dbTableItems = new Item_Model_DbTable();

        $select = $dbTable->select()
                        ->setIntegrityCheck(false)
                        ->from(array('it' => $dbTable->info(Zend_Db_Table_Abstract::NAME)), "language_code")
                        ->joinInner(array("i" => $dbTableItems->info(Zend_Db_Table_Abstract::NAME)), "`i`.item_id = `it`.item_id")
                        ->joinLeft(array('itc' => $dbTable->select()->where('item_id = ?', $id)), 'it.language_code = itc.language_code', "item_id")
                        ->where('it.account_id = ?', $accountId)
                        ->group('it.language_code');
        if($spaceId) {
            $select->where('i.space_id = ?', $spaceId);
        }
        $rowSet = $dbTable->fetchAll($select);
        $translations = array();
        foreach ($rowSet as $row) {
            $translations[$row->language_code] = $row->item_id ? true : false;
        }

      return new Application_Collection_Generic($translations);
    }


}
