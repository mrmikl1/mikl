<?php

class Item_Model_DbTable extends ZendX_Db_Table_Abstract
{
    protected $_name = 'items';
    protected $_autoincrementColName = 'id';
    protected $_primary = array('item_id', 'account_id');
}
