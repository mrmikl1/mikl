<?php

class Item_Model_TranslationsDbTable extends ZendX_Db_Table_Abstract
{
    protected $_name = 'items_translations';
    protected $_primary= array('item_id', 'account_id', 'language_code');
}