<?php
class Item_Model_Domain extends Application_Abstract_Models
{
    const PUBLISHED = 'Y';
    const UNPUBLISHED = 'N';

    const STATUS_UNANSWERED = 'unanswered';
    const STATUS_ANSWERED = 'answered';

    protected $_id;
    protected $_accountId;
    protected $_categoryId;
    protected $_spaceId;
    protected $_title;
    protected $_content;
    protected $_position;
    protected $_created;
    protected $_edited;
    protected $_status;
    protected $_fullName;
    protected $_email;
    protected $_published;
    protected $_language;
    protected $_translations;


    public function getId()
    {
        return $this->_id;
    }

    public function getAccountId()
    {
        return $this->_accountId;
    }

    public function getCategoryId()
    {
        return $this->_categoryId;
    }

    public function getSpaceId()
    {
        return $this->_spaceId;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function getPosition()
    {
        return $this->_position;
    }

    public function getContent()
    {
        return $this->_content;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getEdited()
    {
        return $this->_edited;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getFullName()
    {
        return $this->_fullName;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function getPublished()
    {
        return $this->_published == Item_Model_Domain::PUBLISHED ? true : false;
    }

    public function getLanguage()
    {
        return $this->_language;
    }
    public function getTranslations()
    {
        $translationsMapper = new Item_Model_TranslationsMapper();
        $this->_translations = $translationsMapper->find($this->_id, $this->_accountId, $this->_spaceId);
        return $this->_translations;
    }

    public function setId($id)
    {
        $this->_id = $id;
        return $this->_id;
    }

    public function setAccountId($accountId)
    {
        $this->_accountId = $accountId;
        return $this->_accountId;
    }

        public function setCategoryId($categoryId)
    {
        $this->_categoryId = $categoryId;
        return $this->_categoryId;
    }

    public function setSpaceId($spaceId)
    {
        $this->_spaceId = $spaceId;
        return $this->_spaceId;
    }

    public function setTitle($title)
    {
        $this->_title = $title;
        return $this->_title;
    }

    public function setContent($content)
    {
        $this->_content = $content;
        return $this->_content;
    }

    public function setPosition($position)
    {
        $this->_position = $position;
        return $this->_position;
    }

    public function setCreated($created)
    {
        $this->_created = $created;
        return $this->_created;
    }

    public function setEdited($edited)
    {
        $this->_edited = $edited;
        return $this->_edited;
    }

    public function setStatus($status)
    {
        $this->_status = $status;
        return $this->_status;
    }

    public function setFullName($fullName)
    {
        $this->_fullName = $fullName;
        return $this->_fullName;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
        return $this->_email;
    }

    public function setPublished($published)
    {
        $this->_published = $published;
        return $this->_published;
    }

    public function setLanguage($language)
    {
        $this->_language = $language;
        return $this->_language;
    }
}