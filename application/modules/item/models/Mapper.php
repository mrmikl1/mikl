<?php

class Item_Model_Mapper
{
    public function createNewDomainFromRow($row)
    {
        $data = array(
            'accountId'     => $row->account_id,
            'spaceId'       => $row->space_id,
            'title'         => $row->title,
            'categoryId'    => $row->category_id,
            'content'       => $row->content,
            'id'            => $row->item_id,
            'position'      => $row->position,
            'created'       => $row->created,
            'edited'        => $row->edited,
            'status'        => $row->status,
            'fullName'      => $row->full_name,
            'email'         => $row->email,
            'published'     => $row->published,
            'language'      => $row->language_code
        );
        return new Item_Model_Domain($data);
    }

    public function save(Item_Model_Domain &$item)
    {
        $data = array(
            'account_id'    => $item->getAccountId(),
            'space_id'      => $item->getSpaceId(),
            'category_id'   => $item->getCategoryId(),
            'position'      => $item->getPosition(),
            'status'        => $item->getStatus() ? $item->getStatus() : Item_Model_Domain::STATUS_UNANSWERED,
            'full_name'     => $item->getFullName(),
            'email'         => $item->getEmail(),
            'published'     => $item->getPublished() ? Item_Model_Domain::PUBLISHED : Item_Model_Domain::UNPUBLISHED,

        );
        $translationsData = array(
            'account_id'    => $item->getAccountId(),
            'language_code' => $item->getLanguage(),
            'content'       => $item->getContent(),
            'title'         => $item->getTitle(),
        );
        $dbTableTranslations = new Item_Model_TranslationsDbTable();
        $dbTable = new Item_Model_DbTable();
        if (NULL === $item->getId()) {
            $data['created'] = gmdate('Y-m-d H:i:s');
            $item->setId($dbTable->insert($data));
            $item->setCreated($data['created']);

            $translationsData['item_id'] = $item->getId();
            $dbTableTranslations->insert($translationsData);
        } else {
            $where = array(
                $dbTable->getAdapter()->quoteInto('item_id = ?', $item->getId()),
                $dbTable->getAdapter()->quoteInto('account_id = ?', $item->getAccountId()),
            );
            $data['edited'] = gmdate('Y-m-d H:i:s');
            $dbTable->update($data, $where);
            $item->setEdited($data['edited']);

            $dbTableTranslations = new Item_Model_TranslationsDbTable();
            $row = $dbTableTranslations->find($item->getId(), $item->getAccountId(), $item->getLanguage());
            if($row->current()) {
                $translationsWhere = array(
                    $dbTableTranslations->getAdapter()->quoteInto('item_id = ?', $item->getId()),
                    $dbTableTranslations->getAdapter()->quoteInto('account_id = ?', $item->getAccountId()),
                    $dbTableTranslations->getAdapter()->quoteInto('language_code = ?', $item->getLanguage()),
                );
                $dbTableTranslations->update($translationsData, $translationsWhere);
            } else {
                $translationsData['item_id'] = $item->getId();
                $dbTableTranslations->insert($translationsData);
            }
        }
        return $item;
    }

    public function find($id, Account_Model_Domain $account, $language = 'en')
    {
        $dbTable = new Item_Model_DbTable();
        $dbTableTranslations = new Item_Model_TranslationsDbTable();
        $select = $dbTable->select()
                          ->setIntegrityCheck(false)
                          ->from(array('i' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                          ->joinLeft(array('it' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)), 'it.item_id = i.item_id AND it.account_id = i.account_id')
                          ->where('i.account_id = ?', $account->getId())
                          ->where('i.item_id = ?', $id);
        if($language) {
            $select->where('it.language_code = ?', $language);
        }
        $row = $dbTable->fetchRow($select);
        if (!$row) {
            $row['item_id'] = $id;
            $row['language_code'] = NULL;
            $row = $this->_getLanguageIfNotExistPrimary($row, $account->getId());
            if(!$row){
                return false;
            }
        }
        return $this->createNewDomainFromRow($row);
    }

    public function delete(Item_Model_Domain $item, Account_Model_Domain $account, $language = NULL)
    {
        $dbTable = new Item_Model_DbTable();
        $translationsDbTable = new Item_Model_TranslationsDbTable();
        if($language === NULL){
            $where = array(
                $dbTable->getAdapter()->quoteInto('item_id = ?', $item->getId()),
                $dbTable->getAdapter()->quoteInto('account_id = ?', $account->getId()),
            );
            $result = $dbTable->delete($where);
            return $result;
        } else {
            $transltions = $item->getTranslations();
            $count = 0;
            foreach($transltions as $transltion) {
                if($transltion === true) {
                    $count++;
                }
            }
            if($count > 1) {
                $where = array(
                    $translationsDbTable->getAdapter()->quoteInto('item_id = ?', $item->getId()),
                    $translationsDbTable->getAdapter()->quoteInto('account_id = ?', $account->getId()),
                    $translationsDbTable->getAdapter()->quoteInto('language_code = ?', $language),
                );
                $result = $translationsDbTable->delete($where);
                return $result;
            }else {
                return false;
            }
        }
    }

    public function changePublishingStatus($id, Account_Model_Domain $account, $published)
    {
        $dbTable = new Item_Model_DbTable();
        $where[] = $dbTable->getAdapter()->quoteInto("account_id = ?", $account->getId());
        $where[] = $dbTable->getAdapter()->quoteInto("item_id = ?", $id);
        $data = array("published" => $published);
        return $dbTable->update($data, $where);
    }

    public function changeTranslationLanguage(Item_Model_Domain $item, $language){
        $dbTableTranslations = new Item_Model_TranslationsDbTable();

        $data = array("language_code" => $language);
        $where = array(
            $dbTableTranslations->getAdapter()->quoteInto("account_id = ?", $item->getAccountId()),
            $dbTableTranslations->getAdapter()->quoteInto("item_id = ?", $item->getId()),
            $dbTableTranslations->getAdapter()->quoteInto("language_code = ?", $item->getLanguage()));
        $result = $dbTableTranslations->update($data, $where);
        return $result;
    }

    public function getCollection($accountId, $spaceId = NULL, $categoryId = NULL, $language = 'en', $status = Item_Model_Domain::STATUS_ANSWERED, $published = Item_Model_Domain::PUBLISHED)
    {
        $dbTable = new Item_Model_DbTable();
        $dbTableTranslations = new Item_Model_TranslationsDbTable();
        $items = array();
        $languageCode = $dbTableTranslations->getAdapter()->quoteInto('it.language_code =  ?', $language);

        $select = $dbTable->select()
                          ->setIntegrityCheck(false)
                          ->from(array('i' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                          ->joinLeft(
                                  array('it' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)),
                                  'it.item_id = i.item_id AND it.account_id = i.account_id and '. $languageCode,
                                  array('language_code', 'title', 'content')
                            )
                          ->where('i.`account_id` = ?', $accountId)
                          ->order('position');
        if($status !== NULL) {
            $select->where('status = "' . $status . '"');
        }
        if($published !== NULL) {
            $select->where('published = "' . $published . '"');
        }

        if ($spaceId) {
            $select->where('`space_id` = ?', $spaceId);
        }
        if ($categoryId) {
            $select->where('`category_id` = ?', $categoryId);
        }

        $rowSet  = $dbTable->fetchAll($select);

        foreach ($rowSet as $row){
            $row = $this->_getLanguageIfNotExistPrimary($row, $accountId);
            $item = $this->createNewDomainFromRow($row);
            $item->getTranslations();
            array_push($items, $item);
        }
        return new Application_Collection_Generic($items);
    }

    private function _getLanguageIfNotExistPrimary($row, $accountId){
        $dbTable = new Item_Model_DbTable();
        $dbTableTranslations = new Item_Model_TranslationsDbTable();
        if($row['language_code'] === NULL){
            $select = $dbTable->select()
                    ->setIntegrityCheck(false)
                    ->from(array('i' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                    ->joinLeft(
                        array('it' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)),
                        'it.item_id = i.item_id AND it.account_id = i.account_id'
                    )
                    ->where('i.account_id = ?',$accountId)
                    ->where('i.item_id = ?', $row['item_id'])
                    ->limit(1);
            $row = $dbTable->fetchRow($select);
        }
        return $row;
    }

}
