<?php

class Item_Form_Edit extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->setMethod('Post');

        if ($this->_getParam('id')) {
            $id = new Zend_Form_Element_Hidden('id');
            $id->setDecorators( // clears default decorators
                array(
                    array('ViewHelper') // display only element without decoration
                )
            );

            $this->addElement($id);

            $trans = new Zend_Form_Element_Hidden('newTranslation');
            $trans->setDecorators(array(array('ViewHelper')));

            $this->addElement($trans);
        }

        $language = new Zend_Form_Element_Text('language', $this->_getElementDefaultOptions());
        $language->setLabel($this->_translate->_('Language'))
                 ->setRequired()
                 ->setValidators(array(array('Db_RecordExists', true, array('table' => 'languages', 'field' => 'code'))))
                 ->setDecorators( // clears default decorators
                      array(
                        array('ViewScript',
                            array('viewScript' => 'forms/element-position.phtml')
                        )
                    )
                );

        $space        = new Application_Form_Element_SpaceSelect('spaceId', $this->_getElementDefaultOptions());
        $spaceOptions = $space->getOptions($this->_getParam('accountName'));

        $space->setLabel($this->_translate->_('Space*'))
              ->setMultiOptions(
                    $spaceOptions
              )
              ->setValue($this->_getParam('spaceId'))
              ->setRequired()
              ->setAttribs(
                      array(
                          "data-bootstro-title"     => $this->_translate->_('Space select'),
                          "data-bootstro-content"   => $this->_translate->_('Here you can select space, where you want to create question.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "1",
                          "class"                   => "bootstro-item"
                          )
                      );

        $category = new Application_Form_Element_CategorySelect('categoryId', $this->_getElementDefaultOptions());

        $category->setLabel($this->_translate->_('Category*'))
                 ->setMultiOptions(
                    $category->getOptions(
                        $this->_getParam('accountName'),
                        !$this->_getParam('spaceId') ?
                            $spaceOptions[0]['key'] : $this->_getParam('spaceId')
                    )
                 )
                 ->setValue($this->_getParam('categoryId'))
                 ->setRequired()
                 ->setAttribs(
                      array(
                          "data-bootstro-title"     => $this->_translate->_('Category select'),
                          "data-bootstro-content"   => $this->_translate->_('Here you can select category, where you want to create question.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "2",
                          "class"                   => "bootstro-item"
                          )
                      )
                ->addDecorator(
                   'HtmlTag', array(
                       'tag'       => 'hr',
                       'placement' => 'append'
                   )
               );

        $title = new Zend_Form_Element_Text('title', $this->_getElementDefaultOptions());
        $title->setLabel($this->_translate->_('Question*'))
              ->setRequired()
              ->setAttrib('autofocus', 'true')
              ->setAttribs(
                      array(
                          "data-bootstro-title"     => $this->_translate->_('Question'),
                          "data-bootstro-content"   => $this->_translate->_('Here you can enter your question.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "3",
                          "class"                   => "bootstro-item"
                          )
                      );

        $position = new Zend_Form_Element_Text('position', $this->_getElementDefaultOptions());
        $position->setLabel($this->_translate->_('Position'))
                 ->setDecorators( // clears default decorators
                      array(
                        array('ViewScript',
                            array('viewScript' => 'forms/element-position.phtml')
                        )
                    )
                )
                ->setAttribs(
                        array(
                          "data-bootstro-title"     => $this->_translate->_('Position'),
                          "data-bootstro-content"   => $this->_translate->_('Here you can specify question position.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "5",
                          "class"                   => "bootstro-item"
                            )
                        );

        $publishing = new Zend_Form_Element_Checkbox('published', $this->_getElementDefaultOptions());
        $publishing->setLabel($this->_translate->_('Publishing status'));

        $content = new Zend_Form_Element_Textarea('content', $this->_getElementDefaultOptions());
        $content->setLabel($this->_translate->_('Answer'))
             ->setAttribs(array(
                'rows' => '10',
                'cols' => '40'
             ));

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Save'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit')
                ->setAttribs(
                        array(
                          "data-bootstro-title"     => $this->_translate->_('Submit'),
                          "data-bootstro-content"   => $this->_translate->_('Save.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "6",
                          "class"                   => "bootstro-item"
                            )
                        );

        $this->addElements(array($language, $space, $category, $title, $content, $position, $submit));
    }
}

