<?php
class Item_Form_AnswerQuestion extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->setMethod('Post');

        if ($this->_getParam('id')) {
            $id = new Zend_Form_Element_Hidden('id');
            $id->setDecorators( // clears default decorators
                array(
                    array('ViewHelper') // display only element without decoration
                )
            );
            $this->addElement($id);
        }

        $space        = new Application_Form_Element_SpaceSelect('spaceId', $this->_getElementDefaultOptions());
        $spaceOptions = $space->getOptions($this->_getParam('accountName'));

        $space->setLabel($this->_translate->_('Space*'))
              ->setMultiOptions(
                    $spaceOptions
              )
              ->setValue($this->_getParam('spaceId'))
              ->setRequired()
              ->setAttribs(
                      array(
                          "data-bootstro-title"     => $this->_translate->_('Space select'),
                          "data-bootstro-content"   => $this->_translate->_('There you can choose space, where you want to replace item.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "1",
                          "class"                   => "bootstro-item"
                          )
                      );

        $category = new Application_Form_Element_CategorySelect('categoryId', $this->_getElementDefaultOptions());
        $category->setLabel($this->_translate->_('Category*'))
                 ->setMultiOptions(
                    $category->getOptions(
                        $this->_getParam('accountName'),
                        !$this->_getParam('spaceId') ?
                            $spaceOptions[0]['key'] : $this->_getParam('spaceId')
                    )
                 )
                 ->setValue($this->_getParam('categoryId'))
                 ->setRequired()
                 ->setAttribs(
                      array(
                          "data-bootstro-title"     => $this->_translate->_('Category select'),
                          "data-bootstro-content"   => $this->_translate->_('There you can choose category, where you want to replace item.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "2",
                          "class"                   => "bootstro-item"
                          )
                      );

        $fullName = new Application_Form_Element_TextNote('fullName', $this->_getElementDefaultOptions());
        $fullName->setLabel($this->_translate->_('User\'s name'));

        $email = new Application_Form_Element_TextNote('email', $this->_getElementDefaultOptions());
        $email->setLabel($this->_translate->_('User\'s E-Mail'));

        $title = new Zend_Form_Element_Text('title', $this->_getElementDefaultOptions());
        $title->setLabel($this->_translate->_('Question*'))
              ->setRequired()
              ->setAttrib('autofocus', 'true')
              ->setAttribs(
                      array(
                          "data-bootstro-title"     => $this->_translate->_('Question'),
                          "data-bootstro-content"   => $this->_translate->_('There you can entered a question.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "3",
                          "class"                   => "bootstro-item"
                          )
                      );

        $position = new Zend_Form_Element_Text('position', $this->_getElementDefaultOptions());
        $position->setLabel('Position')
                 ->setDecorators( // clears default decorators
                      array(
                        array('ViewScript',
                            array('viewScript' => 'forms/element-position.phtml')
                        )
                    )
                 )
                 ->setAttribs(
                        array(
                            "data-bootstro-title"     => $this->_translate->_('Position'),
                            "data-bootstro-content"   => $this->_translate->_('There you can specify position of your question.'),
                            "data-bootstro-placement" => "bottom",
                            "data-bootstro-step"      => "5",
                            "class"                   => "bootstro-item"
                            )
                        );

        $content = new Zend_Form_Element_Textarea('content', $this->_getElementDefaultOptions());
        $content->setLabel($this->_translate->_('Answer'))
                ->setAttribs(array(
                    'rows' => '10',
                    'cols' => '40'
                 ));

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Answer'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit')
                ->setAttribs(
                        array(
                            "data-bootstro-title"     => $this->_translate->_('Submit'),
                            "data-bootstro-content"   => $this->_translate->_('Save.'),
                            "data-bootstro-placement" => "bottom",
                            "data-bootstro-step"      => "6",
                            "class"                   => "bootstro-item"
                            )
                        );

        $this->addElements(array($space, $category, $fullName, $email, $title, $content, $position, $submit));
    }
}

