<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected $_config;

    protected function _initConfig()
    {
        $this->_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        Zend_Registry::set('config', $this->_config);
    }

    protected function _initRouter()
    {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        $router->removeDefaultRoutes();

        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', @$_SERVER["HTTP_HOST"], $regs)) {
            $hostname = $regs['domain'];
        } else {
            $hostname = 'localhost'; // for tests
        }

        $hostnameRoute = new Zend_Controller_Router_Route_Hostname(
            ':accountName.' . $hostname
        );

        // REST Routes

	$api = new Zend_Controller_Router_Route('api');

        $restAccount = new Zend_Rest_Route($front, array(), array('v1' => array('account')));
        $chainedRouteAccount = new Zend_Controller_Router_Route_Chain();
        $chainedRouteAccount->chain($api)->chain($restAccount);
        $router->addRoute('restAccount', $chainedRouteAccount);

        $restRoute = new Zend_Rest_Route($front, array(), array(
            'v1' => array('space', 'category', 'item')
        ));

        $chainedRoute = new Zend_Controller_Router_Route_Chain();
        $chainedRoute->chain($hostnameRoute)->chain($api)->chain($restRoute);
        $router->addRoute('rest', $chainedRoute);

        $getRestRoute = new Zend_Controller_Router_Route(
            ':module/:controller/:id/:action',
            array(
                'module'      => 'v1',
                'controller'  => NULL,
                'id'          => NULL
            )
        );

        $chainedGetRoute = new Zend_Controller_Router_Route_Chain();
        $chainedGetRoute->chain($hostnameRoute)->chain($api)->chain($getRestRoute);
        $router->addRoute('restGet', $chainedGetRoute);

        $deleteTranslationLanguage = new Zend_Controller_Router_Route(
            ':module/:controller/:id/translation/:language',
            array(
                'module'      => 'v1',
                'controller'  => NULL,
                'id'          => NULL,
                'language'    => 'en',
                'action'      => 'delete-translation'
            )
        );

        $chainedLanguageRoute = new Zend_Controller_Router_Route_Chain();
        $chainedLanguageRoute->chain($hostnameRoute)->chain($api)->chain($deleteTranslationLanguage);
        $router->addRoute('restDeleteTranslation', $chainedLanguageRoute);

        $getRestRouteAccount = new Zend_Controller_Router_Route(
            ':module/account/:id/:action',
            array(
                'module'      => 'v1',
                'controller'  => 'account',
                'id'          => NULL
            )
        );

        $chainedRouteGetAccount = new Zend_Controller_Router_Route_Chain();
        $chainedRouteGetAccount->chain($api)->chain($getRestRouteAccount);
        $router->addRoute('restGetAccount', $chainedRouteGetAccount);

        //REGULAR Routes

        //Register

        $route['name'] = 'register';
        $route['route'] = 'account/register';
        $route['defaults'] = array(
                                'module'     => 'account',
                                'controller' => 'index',
                                'action'     => 'register'
                            );
        $route['regs'] = null;
        $route['chain'] = false;
        $routes[] = $route;

        //Registered

        $route['name'] = 'registered';
        $route['route'] = 'account/registered';
        $route['defaults'] = array(
                                'module'     => 'account',
                                'controller' => 'index',
                                'action'     => 'registered'
                            );
        $route['regs'] = null;
        $route['chain'] = false;
        $routes[] = $route;

        //Activation

        $route['name'] = 'activation';
        $route['route'] = 'activate/:hash';
        $route['defaults'] = array(
                                'module'      => 'account',
                                'controller'  => 'index',
                                'action'      => 'activate'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //AccountPreferences

        $route['name'] = 'accountPreferences';
        $route['route'] = 'account-preferences/';
        $route['defaults'] = array(
                                'module'     => 'account',
                                'controller' => 'index',
                                'action'     => 'account-preferences'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Admin

        $route['name'] = 'admin';
        $route['route'] = 'admin/';
        $route['defaults'] = array(
                                'module'      => 'admin',
                                'controller'  => 'index',
                                'action'      => 'index'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Login

        $route['name'] = 'login';
        $route['route'] = 'auth/login/';
        $route['defaults'] = array(
                                'module'      => 'auth',
                                'controller'  => 'index',
                                'action'      => 'login'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Logout

        $route['name'] = 'logout';
        $route['route'] = 'auth/logout/';
        $route['defaults'] = array(
                                'module'      => 'auth',
                                'controller'  => 'index',
                                'action'      => 'logout'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //ForgotPassword

        $route['name'] = 'forgotPassword';
        $route['route'] = 'auth/forgot-password/';
        $route['defaults'] = array(
                                'module'      => 'auth',
                                'controller'  => 'index',
                                'action'      => 'forgot-password'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //ResetPassword

        $route['name'] = 'resetPassword';
        $route['route'] = 'auth/reset-password/:hash';
        $route['defaults'] = array(
                                'module'      => 'auth',
                                'controller'  => 'index',
                                'action'      => 'reset-password'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Create Category

        $route['name'] = 'createCategory';
        $route['route'] = 'admin/category/create/:spaceId';
        $route['defaults'] = array(
                                'module'      => 'category',
                                'controller'  => 'index',
                                'action'      => 'create',
                                'spaceId'     => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //EditCategory

        $route['name'] = 'editCategory';
        $route['route'] = 'admin/category/edit/:id/:spaceId/';
        $route['defaults'] = array(
                                'module'      => 'category',
                                'controller'  => 'index',
                                'action'      => 'edit'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //DeleteCategory

        $route['name'] = 'deleteCategory';
        $route['route'] = 'admin/category/delete/:id/:spaceId/';
        $route['defaults'] = array(
                                'module'      => 'category',
                                'controller'  => 'index',
                                'action'      => 'delete',
                                'spaceId'     => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //View Space Frontend

        $route['name'] = 'viewSpaceFrontend';
        $route['route'] = ':id';
        $route['defaults'] = array(
                                'module'      => 'default',
                                'controller'  => 'index',
                                'action'      => 'index',
                                'id'          => NULL
                            );
        $route['regs'] = array('id' => '\d+');
        $route['chain'] = true;
        $routes[] = $route;

        //Create Item

        $route['name'] = 'createItem';
        $route['route'] = 'admin/item/create/:spaceId/:categoryId';
        $route['defaults'] = array(
                                'module'      => 'item',
                                'controller'  => 'index',
                                'action'      => 'create',
                                'spaceId'     => NULL,
                                'categoryId'  => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Edit Item

        $route['name'] = 'editItem';
        $route['route'] = 'admin/item/edit/:id/:spaceId/:categoryId/';
        $route['defaults'] = array(
                                'module'      => 'item',
                                'controller'  => 'index',
                                'action'      => 'edit',
                                'spaceId'     => NULL,
                                'categoryId'  => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Delete Item

        $route['name'] = 'deleteItem';
        $route['route'] = 'admin/item/delete/:id/:spaceId/:categoryId';
        $route['defaults'] = array(
                                'module'      => 'item',
                                'controller'  => 'index',
                                'action'      => 'delete'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Create Space

        $route['name'] = 'createSpace';
        $route['route'] = 'admin/space/create/';
        $route['defaults'] = array(
                                'module'      => 'space',
                                'controller'  => 'index',
                                'action'      => 'create'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;


        //Edit Space

        $route['name'] = 'editSpace';
        $route['route'] = 'admin/space/edit/:id/';
        $route['defaults'] = array(
                                'module'      => 'space',
                                'controller'  => 'index',
                                'action'      => 'edit'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //DeleteSpace

        $route['name'] = 'deleteSpace';
        $route['route'] = 'admin/space/delete/:id/';
        $route['defaults'] = array(
                                'module'      => 'space',
                                'controller'  => 'index',
                                'action'      => 'delete'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //ViewSpace

        $route['name'] = 'viewSpace';
        $route['route'] = 'admin/space/view/:id/';
        $route['defaults'] = array(
                                'module'      => 'space',
                                'controller'  => 'index',
                                'action'      => 'view',
                                'id'          => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //User Preferences

        $route['name'] = 'userPreferences';
        $route['route'] = 'profile/:id';
        $route['defaults'] = array(
                                'module'      => 'user',
                                'controller'  => 'index',
                                'action'      => 'user-preferences'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //ChangePassword

        $route['name'] = 'changePassword';
        $route['route'] = 'user/change-password';
        $route['defaults'] = array(
                                'module'      => 'user',
                                'controller'  => 'index',
                                'action'      => 'change-password'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //Integrate

        $route['name'] = 'integrate';
        $route['route'] = 'integrate/:id';
        $route['defaults'] = array(
                                'module'      => 'default',
                                'controller'  => 'index',
                                'action'      => 'integrate',
                                'id'          => '1'
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

	// AskQuestion

        $route['name'] = 'askQuestion';
        $route['route'] = 'ask/:spaceId';
        $route['defaults'] = array(
                                'module'      => 'default',
                                'controller'  => 'item',
                                'action'      => 'ask-question',
                                'spaceId'     => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //AnswerQuestion

        $route['name'] = 'answerQuestion';
        $route['route'] = 'admin/item/answer/:id/:spaceId/:categoryId/';
        $route['defaults'] = array(
                                'module'      => 'item',
                                'controller'  => 'index',
                                'action'      => 'answer-question',
                                'spaceId'     => NULL,
                                'categoryId'  => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        //ChangePublishingStatus

        $route['name'] = 'ChangePublishingStatus';
        $route['route'] = 'admin/item/change-publishing-status/:id/:statusToChange/';
        $route['defaults'] = array(
                                'module'         => 'item',
                                'controller'     => 'index',
                                'action'         => 'change-publishing-status',
                                'id'             => NULL,
                                'statusToChange' => NULL
                            );
        $route['regs'] = null;
        $route['chain'] = true;
        $routes[] = $route;

        foreach ($routes as $routeArray) {
            $routeNoLocale = $routeArray;

            $routeArray['route'] = ':locale/' . $routeArray['route'];
            $routeArray['defaults']['locale'] = NULL;
            $routeArray['regs']['locale'] = "^[a-z]{2}$";
            $addRoute = new Zend_Controller_Router_Route(
                $routeArray['route'],
                $routeArray['defaults'],
                $routeArray['regs']
            );

            $addRouteNoLocale = new Zend_Controller_Router_Route(
                $routeNoLocale['route'],
                $routeNoLocale['defaults'],
                $routeNoLocale['regs']
            );

            if ($routeArray['chain']) {
                $router->addRoute($routeArray['name'], $hostnameRoute->chain($addRoute));
                $router->addRoute($routeArray['name'] . "NoLocale", $hostnameRoute->chain($addRouteNoLocale));
            } else {
                $router->addRoute($routeArray['name'], $addRoute);
                $router->addRoute($routeArray['name'] . "NoLocale", $addRouteNoLocale);
            }
        }
    }

    protected function _initDb()
    {
        $currentDbAdpter = Zend_Db_Table::getDefaultAdapter();
        if (empty($currentDbAdpter)) {
            $adapter = $this->getPluginResource('db')->getDbAdapter();

            Zend_Db_Table::setDefaultAdapter(
                $adapter
            );
        } else {
            // for tests there is no need to bootstrap db connection,
            // otherwise it will fail tests due to establishing too many connections
        }
    }
    protected function _initResources()
    {
        $autoloader = new Zend_Loader_Autoloader_Resource(array(
            'namespace' => 'Application',
            'basePath'  => APPLICATION_PATH,
            'resourceTypes' => array(
                'Services' => array(
                    'path' => 'services/',
                    'namespace' => 'Service'
                ),
                'Forms' => array(
                    'path' => 'forms/',
                    'namespace' => 'Form'
                ),
                'Collections' => array(
                    'path' => 'collections/',
                    'namespace' => 'Collection'
                ),
                'Exceptions' => array(
                    'path' => 'exceptions/',
                    'namespace' => 'Exception'
                ),
                'Abstract' => array(
                    'path' => 'abstract/',
                    'namespace' => 'Abstract'
                ),
                'Controllers' => array(
                    'path' => 'controllers/',
                    'namespace' => 'Controller'
                )
            )
        ));
    }
    protected function _initFrontController ( )
    {
        $front = Zend_Controller_Front::getInstance();

        $front->addModuleDirectory(APPLICATION_PATH . '/api/');
        $front->addModuleDirectory(APPLICATION_PATH . '/modules/');

        $front->setDefaultModule('default');

        $front->setParam('displayExceptions', $this->_config->get('resources')->frontController->params->displayExceptions);

        $front->registerPlugin(new ZendX_Controller_Plugin_Cache);
        $front->registerPlugin(new ZendX_Controller_Plugin_i18n);
        $front->registerPlugin(new ZendX_Controller_Plugin_415);
        $front->registerPlugin(new ZendX_Controller_Plugin_HttpAuth);
        $front->registerPlugin(new ZendX_Controller_Plugin_404);
        $front->registerPlugin(new ZendX_Controller_Plugin_401);
        $front->registerPlugin(new ZendX_Controller_Plugin_Layout);
        $front->registerPlugin(new ZendX_Controller_Plugin_Request);

        return $front;
    }
}

