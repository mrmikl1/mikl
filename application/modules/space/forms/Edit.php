<?php

class Space_Form_Edit extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->setMethod('Post');

        if ($this->_getParam('id')) {
            $id = new Zend_Form_Element_Hidden('id');
            $id->setDecorators( // clears default decorators
                array(
                    array('ViewHelper') // display only element without decoration
                )
            );

            $this->addElement($id);

            $trans = new Zend_Form_Element_Hidden('newTranslation');
            $trans->setDecorators(array(array('ViewHelper')));

            $this->addElement($trans);
        }

        $language = new Zend_Form_Element_Text('language', $this->_getElementDefaultOptions());
        $language->setLabel($this->_translate->_('Language'))
                 ->setRequired()
                 ->setValidators(array(array('Db_RecordExists', true, array('table' => 'languages', 'field' => 'code'))))
                 ->setDecorators( // clears default decorators
                      array(
                        array('ViewScript',
                            array('viewScript' => 'forms/element-position.phtml')
                        )
                    )
                );

        $name = new Zend_Form_Element_Text('name', $this->_getElementDefaultOptions());
        $name->setLabel($this->_translate->_('Name*'))
             ->setRequired()
             ->setAttrib('autofocus', 'true')
             ->setAttribs(
                      array(
                          "data-bootstro-title"     => $this->_translate->_('Space name'),
                          "data-bootstro-content"   => $this->_translate->_('There you can enter space\'s name'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "1",
                          "class"                   => "bootstro-space"
                          )
                     );

        $position = new Zend_Form_Element_Text('position', $this->_getElementDefaultOptions());
        $position->setLabel($this->_translate->_('Position'))
                 ->setDecorators( // clears default decorators
                    array(
                        array('ViewScript',
                            array(
                                'viewScript' => 'forms/element-position.phtml')
                        )
                    )
                )
                 ->setAttribs(
                        array(
                          "data-bootstro-title"     => $this->_translate->_('Position'),
                          "data-bootstro-content"   => $this->_translate->_('There you can specify position of your space.'),
                          "data-bootstro-placement" => "bottom",
                          "data-bootstro-step"      => "3",
                          "class"                   => "bootstro-space"
                            )
                         );

        $description = new Zend_Form_Element_Textarea('description', $this->_getElementDefaultOptions());
        $description->setLabel($this->_translate->_('Description'))
                    ->setAttribs(array(
                        'rows' => '10',
                        'cols' => '40'
                    ));

        $default = new Zend_Form_Element_Hidden('default');
        $default->setDecorators( // clears default decorators
                array(
                    array('ViewHelper') // display only element without decoration
                )
            );

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Save'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit')
               ->setAttribs(
                       array(
                         "data-bootstro-title"      => $this->_translate->_('Submit'),
                         "data-bootstro-content"    => $this->_translate->_('Save.'),
                         "data-bootstro-placement"  => "bottom",
                         "data-bootstro-step"       => "4",
                         "class"                    => "bootstro-space"
                           )
                       );

        $this->addElements(array($language, $name, $description, $position, $submit, $default));
    }
}
