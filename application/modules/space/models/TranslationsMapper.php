<?php

class Space_Model_TranslationsMapper
{
    public function find($id, $accountId)
    {
        $dbTable = new Space_Model_TranslationsDbTable();
        $select = $dbTable->select()
                        ->setIntegrityCheck(false)
                        ->from(array('st' => $dbTable->info(Zend_Db_Table_Abstract::NAME)), array('language_code'))
                        ->joinLeft(array('stc' => $dbTable->select()->where('space_id = ?', $id)), 'st.language_code = stc.language_code', array('space_id'))
                        ->where('st.account_id = ?', $accountId)
                        ->group('st.language_code');
        $s = $select->assemble();
        $rowSet = $dbTable->fetchAll($select);
        $translations = array();
        foreach ($rowSet as $row) {
            $translations[$row->language_code] = $row->space_id ? true : false;
        }
        
        return new Application_Collection_Generic($translations);
    }


}
