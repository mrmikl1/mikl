<?php

class Space_Model_DbTable extends ZendX_Db_Table_Abstract
{
    protected $_name = 'spaces';
    protected $_autoincrementColName = 'id';
    protected $_primary= array('space_id', 'account_id');
}
