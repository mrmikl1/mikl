<?php

class Space_Model_Mapper
{
    public function createNewDomainFromRow($row)
    {
        $data = array(
            'accountId'    => $row->account_id,
            'name'         => $row->name,
            'id'           => $row->space_id,
            'description'  => $row->description,
            'position'     => $row->position,
            'created'      => $row->created,
            'edited'       => $row->edited,
            'language'     => $row->language_code
        );
        $data['default'] = ($row->default == 'Y') ? true : false;

        return new Space_Model_Domain($data);
    }

    public function save(Space_Model_Domain &$space)
    {
        $data = array(
            'account_id'   => $space->getAccountId(),
            'position'     => $space->getPosition(),
        );

        $translationsData = array(
            'name'         => $space->getName(),
            'description'  => $space->getDescription(),
            'language_code'=> $space->getLanguage(),
            'account_id'   => $space->getAccountId());
        $data['default'] = ($space->getDefault() === TRUE) ? 'Y' : 'N';

        $dbTable = new Space_Model_DbTable();
        $dbTableTranslations = new Space_Model_TranslationsDbTable();

        if ($space->getId() === NULL) {
            $data['created'] = gmdate('Y-m-d H:i:s');
            $space->setId($dbTable->insert($data));
            $space->setCreated($data['created']);

            $translationsData['space_id'] = $space->getId();
            $dbTableTranslations->insert($translationsData);
        } else {
            $where = array(
                $dbTable->getAdapter()->quoteInto('space_id = ?', $space->getId()),
                $dbTable->getAdapter()->quoteInto('account_id = ?', $space->getAccountId()),
            );
            $data['edited'] = gmdate('Y-m-d H:i:s');
            $dbTable->update($data, $where);
            $space->setEdited($data['edited']);

            $dbTableTranslations = new Space_Model_TranslationsDbTable();
            $row = $dbTableTranslations->find($space->getId(), $space->getAccountId(), $space->getLanguage());
            if($row->current()) {
                $translationsWhere = array(
                    $dbTableTranslations->getAdapter()->quoteInto('space_id = ?', $space->getId()),
                    $dbTableTranslations->getAdapter()->quoteInto('account_id = ?', $space->getAccountId()),
                    $dbTableTranslations->getAdapter()->quoteInto('language_code = ?', $space->getLanguage()),
                );
                $dbTableTranslations->update($translationsData, $translationsWhere);
            } else {
                $translationsData['space_id'] = $space->getId();
                $dbTableTranslations->insert($translationsData);
            }
        }
        return $space;
    }

    public function find($id, Account_Model_Domain $account, $language = 'en')
    {
        $dbTableTranslations = new Space_Model_TranslationsDbTable();
        $dbTable = new Space_Model_DbTable();

        $select = $dbTable->select()
                        ->setIntegrityCheck(false)
                        ->from(array('s' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                        ->joinLeft(
                            array('st' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)),
                            'st.space_id = s.space_id AND st.account_id = s.account_id and ' . $dbTableTranslations->getAdapter()->quoteInto('st.language_code =  ?', $language),
                            array('language_code', 'name', 'description')
                        )
                        ->where('s.account_id = ?', $account->getId())
                        ->where('s.space_id = ?', $id);
        if($language) {
            $select->where('st.language_code = ?', $language);
        }
        $row = $dbTable->fetchRow($select);
        if (!$row) {
            $row['space_id'] = $id;
            $row['language_code'] = NULL;
            $row = $this->_getLanguageIfNotExistPrimary($row, $account->getId());
            if(!$row) {
                return false;
            }
        }
        $space = $this->createNewDomainFromRow($row);
        return $space;
    }


    /**
     *
     * @param int $accountId
     * @param string $language
     * @return boolean|\Application_Collection_Generic
     */
    public function getCollection($accountId, $language = 'en')
    {
        $dbTable = new Space_Model_DbTable();
        $dbTableTranslations = new Space_Model_TranslationsDbTable();

        $languageCode = $dbTableTranslations->getAdapter()->quoteInto('st.language_code =  ?', $language);

        $select = $dbTable->select()
                ->setIntegrityCheck(false)
                ->from(array('s' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                ->joinLeft(
                    array('st' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)),
                    'st.space_id = s.space_id AND st.account_id = s.account_id and ' . $languageCode,
                    array('language_code', 'name', 'description')
                )
                ->where('s.account_id = ?', $accountId)
                ->order('position');
        $rowSet = $dbTable->fetchAll($select);

        $spaces = array();
        foreach ($rowSet as $row){
            $row = $this->_getLanguageIfNotExistPrimary($row, $accountId);
            $space = $this->createNewDomainFromRow($row);
            $space->getTranslations();
            array_push($spaces, $space);
        }
        return new Application_Collection_Generic($spaces);
    }

    public function delete(Space_Model_Domain $space, Account_Model_Domain $account, $language = NULL)
    {
        $dbTable = new Space_Model_DbTable();
        $dbTableTranslations = new Space_Model_TranslationsDbTable();
         if($language === NULL){
            $where = array(
                $dbTable->getAdapter()->quoteInto('space_id = ?', $space->getId()),
                $dbTable->getAdapter()->quoteInto('account_id = ?', $account->getId()),
                $dbTable->getAdapter()->quoteInto($dbTable->getAdapter()->quoteIdentifier('default') . ' = ?', 'N')
            );
            $result = $dbTable->delete($where);
            return $result;
        } else {
            $transltions = $space->getTranslations();
            $count = 0;
            foreach($transltions as $transltion) {
                if($transltion === true) {
                    $count++;
                }
            }
            if($count > 1) {
                $where = array(
                    $dbTableTranslations->getAdapter()->quoteInto('space_id = ?', $space->getId()),
                    $dbTableTranslations->getAdapter()->quoteInto('account_id = ?', $account->getId()),
                    $dbTableTranslations->getAdapter()->quoteInto('language_code = ?', $language),
                );
                $result = $dbTableTranslations->delete($where);
                return $result;
            } else {
                return false;
            }
        }
    }

    public function changeTranslationLanguage(Space_Model_Domain &$space, $language){
        $dbTableTranslations = new Space_Model_TranslationsDbTable();

        $data = array("language_code" => $language);
        $where = array(
            $dbTableTranslations->getAdapter()->quoteInto("account_id = ?", $space->getAccountId()),
            $dbTableTranslations->getAdapter()->quoteInto("space_id = ?", $space->getId()),
            $dbTableTranslations->getAdapter()->quoteInto("language_code = ?", $space->getLanguage()));
        $result = $dbTableTranslations->update($data, $where);
        if($result){
            $space->getTranslations();
        }
        return $result;
    }

    private function _getLanguageIfNotExistPrimary($row, $accountId){
        if($row['language_code'] === NULL){
            $dbTable = new Space_Model_DbTable();
            $dbTableTranslations = new Space_Model_TranslationsDbTable();
            $select = $dbTable->select()
                    ->setIntegrityCheck(false)
                    ->from(array('s' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                    ->joinLeft(
                        array('st' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)),
                        'st.space_id = s.space_id AND st.account_id = s.account_id'
                    )
                    ->where('s.account_id = ?', $accountId)
                    ->where('s.space_id = ?', $row['space_id'])
                    ->limit(1);
            $row = $dbTable->fetchRow($select);
        }
        return $row;
    }
}


