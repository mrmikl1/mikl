<?php

class Space_Model_TranslationsDbTable extends ZendX_Db_Table_Abstract
{
    protected $_name = 'spaces_translations';
    protected $_primary= array('space_id', 'account_id','language_code');
}