<?php

class Category_Form_Edit extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->setMethod('Post');

        if ($this->_getParam('id')) {
            $id = new Zend_Form_Element_Hidden('id');
            $id->setDecorators( // clears default decorators
                array(
                    array('ViewHelper') // display only element without decoration
                )
            );

            $this->addElement($id);

            $trans = new Zend_Form_Element_Hidden('newTranslation');
            $trans->setDecorators(array(array('ViewHelper')));

            $this->addElement($trans);
        }

        $space = new Application_Form_Element_SpaceSelect('spaceId', $this->_getElementDefaultOptions());
        $space->setLabel($this->_translate->_('Space*'))
              ->setMultiOptions(
                    $space->getOptions($this->_getParam('accountName'))
                )
              ->setValue($this->_getParam('spaceId'))
              ->setRequired()
              ->addDecorator(
                 'HtmlTag', array(
                     'tag'       => 'hr',
                     'placement' => 'append'
                 )
             )
              ->setAttribs(
                      array(
                          "data-bootstro-title"      => $this->_translate->_('Space select'),
                          "data-bootstro-content"    => $this->_translate->_('There you can choose space, where you want to create category.'),
                          "data-bootstro-placement"  => "bottom",
                          "data-bootstro-step"       => "1",
                          "class"                    => "bootstro-category"
                          )
                      );

        $name = new Zend_Form_Element_Text('name', $this->_getElementDefaultOptions());
        $name->setLabel($this->_translate->_('Name*'))
             ->setRequired()
             ->setAttrib('autofocus', 'true')
             ->setAttribs(
                      array(
                          "data-bootstro-title"      => $this->_translate->_('Category name'),
                          "data-bootstro-content"    => $this->_translate->_('There you can choose your category\'s name.'),
                          "data-bootstro-placement"  => "bottom",
                          "data-bootstro-step"       => "2",
                          "class"                    => "bootstro-category"
                          )
                     );

        $position = new Zend_Form_Element_Text('position', $this->_getElementDefaultOptions());
        $position->setLabel($this->_translate->_('Position'))
                 ->setDecorators( // clears default decorators
                    array(
                        array('ViewScript',
                            array(
                                'viewScript' => 'forms/element-position.phtml')
                        )
                    )
                )
             ->setAttribs(
                      array(
                          "data-bootstro-title"      => $this->_translate->_('Position'),
                          "data-bootstro-content"    => $this->_translate->_('There you can choose your category\'s position.'),
                          "data-bootstro-placement"  => "bottom",
                          "data-bootstro-step"       => "4",
                          "class"                    => "bootstro-category"
                          )
                     );

        $description = new Zend_Form_Element_Textarea('description', $this->_getElementDefaultOptions());
        $description->setLabel($this->_translate->_('Description:'))
                    ->setAttribs(array(
                        'rows' => '10',
                        'cols' => '40'
                    ));

        $default = new Zend_Form_Element_Hidden('default');
        $default->setDecorators( // clears default decorators
                array(
                    array('ViewHelper') // display only element without decoration
                )
            );

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Save'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit')
               ->setAttribs(
                        array(
                            "data-bootstro-title"     => $this->_translate->_('Submit'),
                            "data-bootstro-content"   => $this->_translate->_('Save.'),
                            "data-bootstro-placement" => "bottom",
                            "data-bootstro-step"      => "5",
                            "class"                   => "bootstro-category"
                            )
                       );

        $this->addElements(array($space, $name, $description, $position, $submit, $default));
    }
}
