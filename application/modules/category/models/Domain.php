<?php
class Category_Model_Domain extends Application_Abstract_Models
{
    protected $_id;
    protected $_accountId;
    protected $_spaceId;
    protected $_name;
    protected $_description;
    protected $_selected;
    protected $_default;
    protected $_position;
    protected $_created;
    protected $_edited;
    protected $_language;

    public function getId()
    {
        return $this->_id;
    }

    public function getAccountId()
    {
        return $this->_accountId;
    }

    public function getSpaceId()
    {
        return $this->_spaceId;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function getDefault()
    {
        return $this->_default;
    }

    public function getPosition()
    {
        return $this->_position;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getEdited()
    {
        return $this->_edited;
    }
    
    public function getLanguage()
    {
        return $this->_language;
    }

    public function getTranslations()
    {
        $translationsMapper = new Category_Model_TranslationsMapper();
        $this->_translations = $translationsMapper->find($this->_id, $this->_accountId);
        return $this->_translations;
    }

    public function isSelected()
    {
        return $this->_selected;
    }

    public function setId($id)
    {
        $this->_id = $id;
        return $this->_id;
    }

    public function setAccountId($accountId)
    {
        $this->_accountId = $accountId;
        return $this->_accountId;
    }

    public function setSpaceId($spaceId)
    {
        $this->_spaceId = $spaceId;
        return $this->_spaceId;
    }

    public function setName($name)
    {
        $this->_name = $name;
        return $this->_name;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
        return $this->_description;
    }

    public function setDefault($default)
    {
        $this->_default = ($default === true || $default === "Y") ? true : false;
        return $this->_default;
    }

    public function setPosition($position)
    {
        $this->_position = $position;
        return $this->_position;
    }

    public function setSelected($selected)
    {
        $this->_selected = (bool) $selected;
        return $this->_selected;
    }

    public function setCreated($created)
    {
        $this->_created = $created;
        return $this->_created;
    }

    public function setEdited($edited)
    {
        $this->_edited = $edited;
        return $this->_edited;
    }
    public function setLanguage($language)
    {
        $this->_language = $language;
        return $this->_language;
    }
}


