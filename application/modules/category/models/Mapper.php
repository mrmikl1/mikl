<?php

class Category_Model_Mapper
{
    public function createNewDomainFromRow($row)
    {
        $data = array(
            'accountId'    => $row->account_id,
            'spaceId'      => $row->space_id,
            'name'         => $row->name,
            'id'           => $row->category_id,
            'description'  => $row->description,
            'position'     => $row->position,
            'created'      => $row->created,
            'edited'       => $row->edited,
            'language'     => $row->language_code
        );
        $data['default'] = ($row->default == 'Y') ? true : false;

        return new Category_Model_Domain($data);
    }

    public function save(Category_Model_Domain &$category)
    {
        $data = array(
            'account_id'  => $category->getAccountId(),
            'space_id'    => $category->getSpaceId(),
            'position'    => $category->getPosition(),
        );
        $translationsData = array(
            'name'        => $category->getName(),
            'language_code'=> $category->getLanguage(),
            'account_id'  => $category->getAccountId(),
            'description' => $category->getDescription(),
        );
        $data['default'] = ($category->getDefault() === TRUE) ? 'Y' : 'N';
        $dbTable = new Category_Model_DbTable();
        $dbTableTranslations = new Category_Model_TranslationsDbTable();
        if (NULL === $category->getId()) {
            $data['created'] = gmdate('Y-m-d H:i:s');
            $category->setId($dbTable->insert($data));
            $category->setCreated($data['created']);

            $translationsData['category_id'] = $category->getId();
            $dbTableTranslations->insert($translationsData);
        } else {
            $where = array(
                $dbTable->getAdapter()->quoteInto('category_id = ?', $category->getId()),
                $dbTable->getAdapter()->quoteInto('account_id = ?', $category->getAccountId()),
            );
            $data['edited'] = gmdate('Y-m-d H:i:s');
            $category->setEdited($data['edited']);
            $dbTable->update($data, $where);

            $dbTableTranslations = new Category_Model_TranslationsDbTable();
            $row = $dbTableTranslations->find($category->getId(), $category->getAccountId(), $category->getLanguage());
            if($row->current()) {
                $translationsWhere = array(
                    $dbTableTranslations->getAdapter()->quoteInto('category_id = ?', $category->getId()),
                    $dbTableTranslations->getAdapter()->quoteInto('account_id = ?', $category->getAccountId()),
                    $dbTableTranslations->getAdapter()->quoteInto('language_code = ?', $category->getLanguage()),
                );
                $dbTableTranslations->update($translationsData, $translationsWhere);
            } else {
                $translationsData['category_id'] = $category->getId();
                $dbTableTranslations->insert($translationsData);
            }
        }
        return $category;
    }

    public function find($id, Account_Model_Domain $account, $language='en')
    {
        $dbTable = new Category_Model_DbTable();
        $dbTableTranslations = new Category_Model_TranslationsDbTable();

        $select = $dbTable->select()
                          ->setIntegrityCheck(false)
                          ->from(array('c' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                          ->joinLeft(array('ct' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)), 'ct.category_id = c.category_id AND ct.account_id = c.account_id')
                          ->where('c.account_id = ?', $account->getId())
                          ->where('c.category_id = ?', $id);
        if($language) {
            $select->where('ct.language_code = ?', $language);
        }

        $row = $dbTable->fetchRow($select);

        if (!$row) {
            $row['space_id'] = $id;
            $row['language_code'] = NULL;
            $row = $this->_getLanguageIfNotExistPrimary($row, $account->getId());
            if(!$row) {
                return false;
            }
        }

        return $this->createNewDomainFromRow($row);
    }

    public function getCollection(Account_Model_Domain $account, Space_Model_Domain $space = NULL, $language = NULL)
    {
        $dbTable = new Category_Model_DbTable();
        $dbTableTranslations = new Category_Model_TranslationsDbTable();
        $languageCode = $dbTableTranslations->getAdapter()->quoteInto('ct.language_code =  ?', $language);
        $categories = array();
        $select = $dbTable->select()
                          ->setIntegrityCheck(false)
                          ->from(array('c' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                          ->joinLeft(
                                  array('ct' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)),
                                  'ct.category_id = c.category_id AND ct.account_id = c.account_id and ' . $languageCode,
                                  array('language_code', 'name', 'description')
                            )
                          ->where('c.`account_id` = ?', $account->getId())
                          ->order('position');

        if ($space) {
            $select->where('c.`space_id` = ?', $space->getId());
        }

        $rowSet = $dbTable->fetchAll($select);

        foreach ($rowSet as $row){
            $row = $this->_getLanguageIfNotExistPrimary($row, $account);
            $category = $this->createNewDomainFromRow($row);
            $category->getTranslations();
            array_push($categories, $category);
        }

        return new Application_Collection_Generic($categories);
    }

    public function delete(Category_Model_Domain $category, Account_Model_Domain $account, $language = NULL)
    {
        $dbTable = new Category_Model_DbTable();
        $dbTableTranslations = new Category_Model_TranslationsDbTable();
         if($language === NULL){
            $where = array(
                $dbTable->getAdapter()->quoteInto('category_id = ?', $category->getId()),
                $dbTable->getAdapter()->quoteInto('account_id = ?', $account->getId()),
		$dbTable->getAdapter()->quoteInto($dbTable->getAdapter()->quoteIdentifier('default') . ' = ?', 'N')
            );
            $result = $dbTable->delete($where);
            return $result;
        } else {
            $transltions = $category->getTranslations();
            $count = 0;
            foreach($transltions as $transltion) {
                if($transltion === true) {
                    $count++;
                }
            }
            if($count > 1) {
                $where = array(
                    $dbTableTranslations->getAdapter()->quoteInto('category_id = ?', $category->getId()),
                    $dbTableTranslations->getAdapter()->quoteInto('account_id = ?', $account->getId()),
                    $dbTableTranslations->getAdapter()->quoteInto('language_code = ?', $language),
                );
                $result = $dbTableTranslations->delete($where);
                return $result;
            } else {
                return false;
            }
        }
    }

    public function changeTranslationLanguage(Category_Model_Domain &$category, $language){
        $dbTableTranslations = new Category_Model_TranslationsDbTable();

        $data = array("language_code" => $language);
        $where = array(
            $dbTableTranslations->getAdapter()->quoteInto("account_id = ?", $category->getAccountId()),
            $dbTableTranslations->getAdapter()->quoteInto("category_id = ?", $category->getId()),
            $dbTableTranslations->getAdapter()->quoteInto("language_code = ?", $category->getLanguage()));
        $result = $dbTableTranslations->update($data, $where);
        if($result){
            $category->getTranslations();
        }
        return $result;
    }

    private function _getLanguageIfNotExistPrimary($row, $account){
        if($row['language_code'] === NULL){
            $dbTable = new Category_Model_DbTable();
            $dbTableTranslations = new Category_Model_TranslationsDbTable();
            $select = $dbTable->select()
                    ->setIntegrityCheck(false)
                    ->from(array('c' => $dbTable->info(Zend_Db_Table_Abstract::NAME)))
                    ->joinLeft(
                        array('ct' => $dbTableTranslations->info(Zend_Db_Table_Abstract::NAME)),
                        'ct.category_id = c.category_id AND ct.account_id = c.account_id'
                    )
                    ->where('c.account_id = ?', $account->getId())
                    ->where('c.category_id = ?', $row['category_id'])
                    ->limit(1);
            $row = $dbTable->fetchRow($select);
        }
        return $row;
    }
}
