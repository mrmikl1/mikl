<?php

class Category_Model_TranslationsDbTable extends ZendX_Db_Table_Abstract
{
    protected $_name = 'categories_translations';
    protected $_primary= array('category_id', 'account_id','language_code');
}