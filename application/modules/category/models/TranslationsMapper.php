<?php

class Category_Model_TranslationsMapper
{
    public function find($id, $accountId)
    {
        $dbTable = new Category_Model_TranslationsDbTable();
        $select = $dbTable->select()
                        ->setIntegrityCheck(false)
                        ->from(array('cat' => $dbTable->info(Zend_Db_Table_Abstract::NAME)), array('language_code'))
                        ->joinLeft(array('catc' => $dbTable->select()->where('category_id = ?', $id)), 'cat.language_code = catc.language_code', array('category_id'))
                        ->where('cat.account_id = ?', $accountId)
                        ->group('cat.language_code');
        $s = $select->assemble();
        $rowSet = $dbTable->fetchAll($select);
        $translations = array();
        foreach ($rowSet as $row) {
            $translations[$row->language_code] = $row->category_id ? true : false;
        }

        return new Application_Collection_Generic($translations);
    }


}
