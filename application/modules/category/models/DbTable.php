<?php

class Category_Model_DbTable extends ZendX_Db_Table_Abstract
{
    protected $_name = 'categories';
    protected $_autoincrementColName = 'id';
    protected $_primary = array('category_id', 'account_id');
}
