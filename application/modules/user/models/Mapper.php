<?php

class User_Model_Mapper
{
    public function createNewDomainFromRow($resultRow)
    {
        $row = array(
                'accountId'          => $resultRow->account_id,
                'id'                 => $resultRow->user_id,
                'email'              => $resultRow->email,
                'firstName'          => $resultRow->first_name,
                'lastName'           => $resultRow->last_name,
                'activationHash'     => $resultRow->activation_hash,
                'forgotPasswordHash' => $resultRow->forgot_password_hash,
                'password'           => $resultRow->password,
                'created'            => $resultRow->created,
                'edited'             => $resultRow->edited
        );
        $row['active'] = ($resultRow->active == 'Y') ? true : false;
        return new User_Model_Domain($row);
    }


    public function save(User_Model_Domain $user)
    {
        $data = array(
            'email'                => $user->getEmail(),
            'first_name'           => $user->getFirstName(),
            'last_name'            => $user->getLastName(),
            'account_id'           => $user->getAccountId(),
            'activation_hash'      => $user->getActivationHash(),
            'forgot_password_hash' => $user->getforgotPasswordHash(),
            'password'             => $user->getPassword(),
        );
        $data['active'] = ($user->getActive() === true) ? 'Y' : 'N';
        $dbTable = new User_Model_DbTable();
        if (NULL === $user->getId()) {
            $data['created'] = gmdate('Y-m-d H:i:s');
            $user->setId($dbTable->insert($data));
            $user->setCreated($data['created']);
        }
        else {
            $data['edited'] = gmdate('Y-m-d H:i:s');
            $dbTable->update($data, 'user_id = ' . $user->getId());
            $user->setEdited($data['edited']);
        }
        return $user;
    }

    public function findByAccountIdAndHash(Account_Model_Domain $account, $hash)
    {
        $dbTable = new User_Model_DbTable();
        $select = $dbTable->select()->where('account_id = ?', $account->getId())->where('activation_hash = ?', $hash);
        $row = $dbTable->fetchRow($select);
        if ($row) {
            return $this->createNewDomainFromRow($row);
        } else {
            return false;
        }
    }

    public function findByEmail(Account_Model_Domain $account, $email)
    {
        $dbTable = new User_Model_DbTable();
        $select = $dbTable->select()
                          ->where('email = ?', $email)
                          ->where('account_id = ?', $account->getId());

        $resultRow = $dbTable->fetchRow($select);
        if ($resultRow) {
            return $this->createNewDomainFromRow($resultRow);
        }
        else {
            return FALSE;
        }
    }

    public function findByAccountAndPasswordHash(Account_Model_Domain $account, $hash)
    {
        $dbTable = new User_Model_DbTable();
        $select = $dbTable->select()->where('account_id = ?', $account->getId())->where('forgot_password_hash = ?', $hash);
        $row = $dbTable->fetchRow($select);
        if (!$row) {
            return FALSE;
        }
        $user = $this->createNewDomainFromRow($row);
        return $user;
    }

    public function findByAccountIdAndUserId(Account_Model_Domain $account, $id)
    {
        $dbTable = new User_Model_DbTable();
        $select = $dbTable->select()->where('account_id = ?', $account->getId())->where('user_id = ?', $id);
        $row = $dbTable->fetchRow($select);
        if (!$row) {
            return false;
        }
        $user = $this->createNewDomainFromRow($row);
        return $user;
    }

    public function findByAccountId($accountId){
        $dbTable = new User_Model_DbTable();
        $select = $dbTable->select()->where('account_id = ?', $accountId);
        $row = $dbTable->fetchRow($select);
        if (!$row) {
            return false;
        }
        $user = $this->createNewDomainFromRow($row);
        return $user;
    }
}