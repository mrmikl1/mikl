<?php
class User_Model_Domain extends Application_Abstract_Models
{
    protected $_email;
    protected $_id;
    protected $_accountId;
    protected $_firstName;
    protected $_lastName;
    protected $_password;
    protected $_activationHash;
    protected $_active;
    protected $_forgotPasswordHash;
    protected $_created;
    protected $_edited;

    public function getEmail()
    {
        return $this->_email;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getAccountId()
    {
        return $this->_accountId;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function getLastName()
    {
        return $this->_lastName;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function getActivationHash()
    {
        return $this->_activationHash;
    }

    public function getActive()
    {
        return $this->_active;
    }

    public function getforgotPasswordHash()
    {
        return $this->_forgotPasswordHash;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getEdited()
    {
        return $this->_edited;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
        return $this->_email;
    }

    public function setId($id)
    {
        $this->_id = $id;
        return $this->_id;
    }

    public function setAccountId($accId)
    {
        $this->_accountId = $accId;
        return $this->_accountId;
    }

    public function setFirstName($fName)
    {
        $this->_firstName = $fName;
        return $this->_firstName;
    }

    public function setLastName($lName)
    {
        $this->_lastName = $lName;
        return $this->_lastName;
    }

    public function setPassword($pass)
    {
        $this->_password = $pass;
        return $this->_password;
    }

    public function setActivationHash($activationHash)
    {
        $this->_activationHash = $activationHash;
        return $this->_activationHash;
    }

    public function setActive($active)
    {
        $this->_active = $active;
        return $this->_active;
    }

    public function setForgotPasswordHash($forgotPasswordHash)
    {
        $this->_forgotPasswordHash = $forgotPasswordHash;
        return $this->_forgotPasswordHash;
    }

    public function setCreated($created)
    {
        $this->_created = $created;
        return $this->_created;
    }

    public function setEdited($edited)
    {
        $this->_edited = $edited;
        return $this->_edited;
    }

    /**
     * @todo move to generic model
     * @return \User_Model_Domain
     */
    public function toArray()
    {
        foreach ($this as $key => $value) {
            $toArray[trim($key, '_')] = $value;
        }

        return $toArray;
    }
}


