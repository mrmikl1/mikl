<?php

class User_IndexController extends Zend_Controller_Action
{
     /**
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;

    public function init()
    {
        $this->_flashMessenger =
        $this->_helper->getHelper('FlashMessenger');

        $this->_helper->layout->setLayout("admin-one-column");

        $this->view->flashMessenger = $this->_flashMessenger;
        $this->_translate = Zend_Registry::get('Zend_Translate');
    }

    public function changePasswordAction()
    {
        $this->_helper->viewRenderer('user-preferences');
        $userId = $this->getParam('id');

        $accountName = $this->getParam('accountName');
        $data = $this->getRequest()->getPost();

        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);
        $userMapper = new User_Model_Mapper();
        $user = $userMapper->findByAccountIdAndUserId($account, $userId);
        $changePasswordForm = new User_Form_ChangePassword($this->getRequest()->getParams());

        if ($this->getRequest()->isPost()) {
            if (!$changePasswordForm->isValid($data)) {
                $this->view->user = $user;
                $this->_flashMessenger->addMessage($this->_translate->_("Password is wrong"), "error");
            } else {
                $user->setPassword(md5($data['password']));
                $userMapper->save($user);
                $url = $this->_helper->url->url(array('id' => $user->getId()), 'userPreferences');
                $this->_flashMessenger->addMessage($this->_translate->_("Password Changed"), "success");
                return $this->_redirect($url);
            }
        }
        $userPreferencesForm = new User_Form_UserPreferences();
        $userPreferencesForm->populate($user->toArray());
        $this->view->userPreferencesForm = $userPreferencesForm;
        $this->view->changePasswordForm = $changePasswordForm;
    }

    public function userPreferencesAction()
    {
        $accountName = $this->getParam('accountName');
        $userId = $this->getParam('id');

        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);

        $userMapper = new User_Model_Mapper();
        $user = $userMapper->findByAccountIdAndUserId($account, $userId);

        $userPreferencesForm = new User_Form_UserPreferences();

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            if ($userPreferencesForm->isValid($data)) {
                $user->setEmail($data['email']);
                $user->setFirstName($data['firstName']);
                $user->setLastName($data['lastName']);
                $userMapper->save($user);

                Zend_Auth::getInstance()->getStorage()->write($user);
                $this->_flashMessenger->addMessage($this->_translate->_("Account data changed"), "success");
                $url = $this->_helper->url->url(array('id' => $user->getId()), 'userPreferences');
                return $this->_redirect($url);
            } else {
                $user = $data;
            }
        }

        /* clean up this logic when 404 and 403 erros ready */

        if (!is_array($user)) {
            $user = $user->toArray();
        }

        $userPreferencesForm->populate($user);
        $this->view->userPreferencesForm = $userPreferencesForm;

        $changePasswordForm = new User_Form_ChangePassword($this->getAllParams());
        $changePasswordForm->populate($user);
        $this->view->changePasswordForm = $changePasswordForm;
    }
}
