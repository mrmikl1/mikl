<?php

class User_Form_ChangePassword extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->addElementPrefixPath('Application_Form_Validate', APPLICATION_PATH . '/forms/Validate/', 'validate');

        $this->setMethod('Post');
        $this->setAction($this->getView()->url(array('id' => $this->_getParam('id')), 'changePassword'));

        $id = new Zend_Form_Element_Hidden('id');
        $id->setDecorators( // clears default decorators
            array(
                array('ViewHelper') // display only element without decoration
            )
        );

        $currentPassword = new Zend_Form_Element_Password('currentPassword', $this->_getElementDefaultOptions());
        $currentPassword->setLabel($this->_translate->_('Current password*'))
                        ->setRequired()
                        ->setValidators(array(array(
                            'validator' => 'currentPassword',
                            'options'   => $this->_getParams()
                        )));

        $password = new Zend_Form_Element_Password('password', $this->_getElementDefaultOptions());
        $password->setLabel($this->_translate->_('New password*'))
                 ->setRequired();

        $confirmPassword = new Zend_Form_Element_Password('confirmPassword', $this->_getElementDefaultOptions());
        $confirmPassword->setLabel($this->_translate->_('New password confirm*'))
                        ->setRequired()
                        ->setValidators(array(
                               array(
                                   'identical',
                                   false,
                                   array('token' => 'password')
                               )
                        ));

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Change password'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit');

        $this->addElements(array($id, $currentPassword, $password, $confirmPassword, $submit));
    }

}