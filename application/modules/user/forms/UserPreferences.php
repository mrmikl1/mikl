<?php

class User_Form_UserPreferences extends Application_Abstract_Form
{
    public function initForm()
    {
        $this->setMethod('Post');

        $id = new Zend_Form_Element_Hidden('id');
        $id->setDecorators( // clears default decorators
            array(
                array('ViewHelper') // display only element without decoration
            )
        );

        $email = new Zend_Form_Element_Text('email', $this->_getElementDefaultOptions());
        $email->setLabel($this->_translate->_('Email*'))
              ->setRequired()
              ->setValidators(array(array('EmailAddress')));

        $firstName = new Zend_Form_Element_Text('firstName', $this->_getElementDefaultOptions());
        $firstName->setLabel($this->_translate->_('First name*'))
                  ->setRequired();

        $lastName = new Zend_Form_Element_Text('lastName', $this->_getElementDefaultOptions());
        $lastName->setLabel($this->_translate->_('Last name*'))
                 ->setRequired();

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Save'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit');

        $this->addElements(array($id, $email, $firstName, $lastName, $submit));
    }
}