<?php
class Account_IndexController extends Zend_Controller_Action
{
     /**
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;

    public function init()
    {
        $this->_flashMessenger =
            $this->_helper->getHelper('FlashMessenger');

        $this->view->flashMessenger = $this->_flashMessenger;
        $this->_translate = Zend_Registry::get('Zend_Translate');
    }

    public function registerAction()
    {
        if (APPLICATION_ENV == 'production') {
            throw new Application_Exception_404();
        }
        $options = array('theme' => 'clean');

        $recaptcha  = new Zend_Service_ReCaptcha(
            '6LfZxskSAAAAABH11vDiY1UdmiBGWoJRWmfATQUv',
            '6LfZxskSAAAAAMG7aReUmDAFC_OVXcdq7wyUOQq2',
            null,
            $options
        );

        if (APPLICATION_ENV == 'production' || APPLICATION_ENV == 'staging') {
            Zend_Registry::set('recaptcha', $recaptcha);
        }

        $form = new Account_Form_Register($this->getAllParams());
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $accountService = new Application_Service_Accounts();
            $account = $accountService->create($data);
            if($account instanceof Account_Model_Domain){
                setcookie('showHelp',true);
                $url = $this->_helper->url->url(array(
                    'module'      => 'account',
                    'controller'  => 'index',
                    'action'      => 'registered')
                );
                return $this->_redirect($url);
            } else {
                $formErrors = $account->getMessages();
                $this->view->formErrors = $formErrors;
                $this->view->postData = $data;
            }
        }
        $this->view->form = $form;
    }

    public function registeredAction()
    {
        $this->_helper->layout->setLayout("admin-one-column");
    }

    public function activateAction()
    {
        $this->_helper->layout->setLayout("admin-one-column");

        $accountName = $this->getParam('accountName');
        $hash = $this->getParam('hash');
        $accountService = new Application_Service_Accounts();
        $account = $accountService->get($accountName);
        if ($account) {
            $userMapper = new User_Model_Mapper();
            $user = $userMapper->findByAccountIdAndHash($account, $hash);
        }
        if (!empty($user)) {
            if(!$user->getActive()) {
                $user->setActive(true);
                $userMapper->save($user);
                $this->_flashMessenger->addMessage($this->_translate->_("You have successfully activated your account!"), "success");
            } else {
                $this->_flashMessenger->addMessage($this->_translate->_("Your account has been already activated!"), "error");
            }
        } else {
            $this->_flashMessenger->addMessage($this->_translate->_("Incorrect activation URL!"), "error");
        }
    }
}
