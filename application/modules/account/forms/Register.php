<?php

class Account_Form_Register extends Application_Abstract_Form
{
    public function initForm()
    {
        $translate = Zend_Registry::get('Zend_Translate');
        $this->addElementPrefixPath('Application_Form_Validate', APPLICATION_PATH . '/forms/Validate/', 'validate');
        $this->setMethod('Post');
        $email = new Zend_Form_Element_Text('email', $this->_getElementDefaultOptions());
        $email->setLabel($this->_translate->_('Email'))
              ->setRequired()
              ->setValidators(array(array('EmailAddress')));

        $password = new Zend_Form_Element_Password('password', $this->_getElementDefaultOptions());
        $password->setLabel($this->_translate->_('Password'))
                 ->setRequired();
        $confirmPassword = new Zend_Form_Element_Password('passwordConfirm', $this->_getElementDefaultOptions());
        $confirmPassword->setLabel($this->_translate->_('Confirm password'))
                 ->setRequired()
                 ->setValidators(array(
                     array('identical', false, array('token' => 'password'))
                 ));
        $accountName = new Zend_Form_Element_Text('accountName', $this->_getElementDefaultOptions());
        $accountName->setLabel($this->_translate->_('Account name'))
                    ->setDescription($this->_translate->_('Will be used as your own subdomain for <a href="http://' . $_SERVER['SERVER_NAME'] . '">grofaq.com</a>'))
                    ->setRequired()
                    ->setFilters(array('StringTrim', 'StripTags'))
                    ->setValidators(array(
                        array('Db_NoRecordExists', true, array('table' => 'accounts', 'field' => 'name')),
                        array('MultiMatch', true, array(
                                array(
                                    '/^((?!ns[0-9]).+)*$/' => $translate->_('Account Name cannot contain group of characters like: ns1, ns2a and so one'),
                                    '/^[a-zA-Z0-9]+$/'  => $translate->_('Account Name may contain only: a-z, 0-9')
                                )
                            )
                        ),
                        array('NotInArray', true, array('haystack' => array('billing','customer', 'staging', 'customers', 'client', 'clients', 'forum', 'community', 'support','www','admin','ftp','imap','pop','mail','smtp','email','webmail','sales','ssh'))),
                        array('StringLength', true, array('min' => 3))

                ));
        $firstName = new Zend_Form_Element_Text('firstName', $this->_getElementDefaultOptions());
        $firstName->setLabel($this->_translate->_('First name'))
                  ->setRequired();
        $lastName = new Zend_Form_Element_Text('lastName', $this->_getElementDefaultOptions());
        $lastName->setLabel($this->_translate->_('Last name'))
                  ->setRequired();
        $submit = new Zend_Form_Element_Button('submit');
        $submit->setLabel($this->_translate->_('Register'))
               ->setDecorators( // clears default decorators
                   array(
                       array('ViewScript',
                           array(
                               'viewScript' => 'forms/element-button.phtml')
                       )
                   )
               );
        $submit->setAttrib('type', 'submit');
        $this->addElements(array($accountName,  $firstName, $lastName, $email, $password, $confirmPassword, $submit));
    }

}