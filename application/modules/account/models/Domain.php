<?php
class Account_Model_Domain extends Application_Abstract_Models
{
    protected $_name;
    protected $_id;
    protected $_created;

    public function __construct(array $options = NULL) {
        if(is_array($options))
        {foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            $this->$method($value);
        }}
        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function setName($name)
    {
        $this->_name = $name;
        return $this->_name;
    }

    public function setId($id)
    {
        $this->_id = $id;
        return $this->_id;
    }

    public function setCreated($created)
    {
        $this->_created = $created;
        return $this->_created;
    }
}