<?php

class Account_Model_Mapper
{
    public function createNewDomainFromRow($row)
    {
        $data = array(
                'id'                => $row->account_id,
                'name'              => $row->name,
                'created'           => $row->created,
        );

        return new Account_Model_Domain($data);
    }

    /**
     *
     * @param string $name
     * @return Account_Model_Domain
     */
    public function findByName($name)
    {
        $dbTable = new Account_Model_DbTable();
        $select = $dbTable->select()
                          ->where(
                                $dbTable->getAdapter()->quoteInto('name = ?', $name)
                            );

        $row = $dbTable->fetchRow($select);
        if(!$row) {
            return false;
        }
        return $this->createNewDomainFromRow($row);

    }
    public function save(Account_Model_Domain &$account)
    {
        $data = array(
            'name'         => $account->getName()
        );

        $dbTable = new Account_Model_DbTable();
        if (NULL === $account->getId()) {
            $data['created'] = gmdate('Y-m-d H:i:s');
            $account->setId($dbTable->insert($data));
            $account->setCreated($data['created']);
        } else {
            $where = array(
                $dbTable->getAdapter()->quoteInto('account_id = ?', $account->getId()),
            );
            $dbTable->update($data, $where);
        }

        return $account;
    }
}
