<?php

class V1_ItemController extends Application_Abstract_RestController
{
    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->getResponse()->setHeader('accept', 'application/json');
    }
    public function getAction()
    {
        $itemId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $language = $this->getRequest()->getHeader('accept-language');

        $itemService = new Application_Service_Items();
        $item = $itemService->get($accountName, $itemId, $language);
        $date = date_format(date_create($item->getEdited()), 'D, d M Y H:i:s O');

        $this->getResponse()->setHeader('last-modified', $date);
        $this->getResponse()->setBody($item->toJson());
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function headAction()
    {
        $itemId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $itemService = new Application_Service_Items();
        $item = $itemService->get($accountName, $itemId);
        $contentLength = strlen($item->toJson());
        $date = date_format(date_create($item->getEdited()), 'D, d M Y H:i:s O');

        $this->getResponse()->setHeader('last-modified', $date);
        $this->getResponse()->setHeader('content-length', $contentLength);
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function indexAction()
    {
        $accountName = $this->getParam('accountName');
        $spaceId = $this->getParam('spaceId');
        $categoryId = $this->getParam('categoryId');

        $language = $this->getRequest()->getHeader('accept-language');
        $itemService = new Application_Service_Items();
        if($auth = Zend_Auth::getInstance()->hasIdentity()) {
            $itemCollection = $itemService->getCollection($accountName, $spaceId, $categoryId, $language, NULL, NULL);
        } else {
            $itemCollection = $itemService->getCollection($accountName, $spaceId, $categoryId, $language);
        }
        $items = $itemCollection->toArray();

        $body = array(
            'count' => $itemCollection->count(),
            'items' => $items
        );

        $this->getResponse()->setBody(json_encode($body));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function postAction()
    {
        $accountName = $this->getParam('accountName');
        $data = json_decode($this->getRequest()->getRawBody(), true);

        if ($this->getParam('id'))
            $data['id'] = $this->getParam('id');

        $auth = Zend_Auth::getInstance()->hasIdentity();
        $itemService = new Application_Service_Items();
        if ($auth) {
            $itemService = new Application_Service_Decorated_Validation_Edit($itemService);
        } else {
            $itemService = new Application_Service_Decorated_Validation_AskQuestion($itemService);
        }
        if(!$this->getParam('id')) {
            if($auth) {
                $data['status'] = Item_Model_Domain::STATUS_ANSWERED;
            } else {
                $data['status'] = Item_Model_Domain::STATUS_UNANSWERED;
            }
        }
        $escapedData = $this->escapeData($data, 'content');

        $item = $itemService->save($accountName, $escapedData);
        if ($item instanceof Item_Model_Domain) {
            $url = $this->_helper->url->url(array(
                'accountName' => $accountName,
                'id'          => $item->getId()
            ), 'rest');
            if ($this->getParam('id')) {
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->setHttpResponseCode(201);
            }
//            $this->getResponse()->setHeader('Location', $url);
            $response = $item->toJson();
            $this->getResponse()->setBody($response);

        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->setBody(json_encode($item->getMessages()));
        }
    }

    public function putAction()
    {
        $this->forward('post');
    }

    public function deleteAction()
    {
        $itemId = $this->getParam('id');
        $accountName = $this->getParam('accountName');

        $itemService = new Application_Service_Items();
        $itemService->delete($accountName, $itemId);

        $this->getResponse()->setHttpResponseCode(204);
    }

    public function deleteTranslationAction()
    {
        $itemId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $language = $this->getParam('language');
        $itemService = new Application_Service_Items();
        $result = $itemService->delete($accountName, $itemId, $language);

        if ($result === false) {
            $this->getResponse()->setHttpResponseCode(400);
        } else {
            $this->getResponse()->setHttpResponseCode(204);
        }
    }

    public function optionsAction()
    {
        $this->getResponse()->setBody($this->_doc());
        $this->getResponse()->setHeader('Allow', 'OPTIONS, HEAD, INDEX, GET, POST, PUT, DELETE');
        $this->getResponse()->setHttpResponseCode(200);
    }
}