<?php

class V1_AccountController extends Application_Abstract_RestController
{
    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->getResponse()->setHeader('accept', 'application/json');
    }

    public function postAction()
    {
        $data = json_decode($this->getRequest()->getRawBody(), true);

        $escapedData = $this->escapeData($data);

        $accountsService = new Application_Service_Accounts();
        $account = $accountsService->create($escapedData);
        if($account instanceof Account_Model_Domain) {
            $this->getResponse()->setBody($account->toJson());
            $this->getResponse()->setHttpResponseCode(201);
            $url = $this->_helper->url->url(array(
                        'accountName' => $account->getName())
                    );
            $this->getResponse()->setHeader('Location', $url);
        } else {
            $formErrors = $account->getMessages();

            $this->getResponse()->setBody(json_encode($formErrors));
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function deleteAction() {

    }

    public function getAction()
    {
        $accountName = $this->getParam('id');
        $accountsService = new Application_Service_Accounts();
        $account = $accountsService->get($accountName);
        $this->getResponse()->setBody($account->toJson());
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function spacesAction()
    {
        $accountName = $this->getParam('id');
        $spaceService = new Application_Service_Spaces();
        $spaceCollection = $spaceService->getCollection($accountName);
        $result = $spaceCollection->toArray();
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function categoriesAction()
    {
        $accountName = $this->getParam('id');
        $categoryService = new Application_Service_Categories();
        $categoryCollection = $categoryService->getCollection($accountName);
        $result = $categoryCollection->toArray();
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function itemsAction()
    {
        $accountName = $this->getParam('id');
        $itemService = new Application_Service_Items();
        $itemsCollection = $itemService->getCollection($accountName);
        $result = $itemsCollection->toArray();
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function headAction()
    {
        $accountName = $this->getParam('id');
        $accountsService = new Application_Service_Accounts();
        $account = $accountsService->get($accountName);
        $contentLength = strlen($account->toJson());
        $this->getResponse()->setHeader('content-length', $contentLength);
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function indexAction()
    {
        $this->getResponse()->setHttpResponseCode(501);
    }

    public function putAction()
    {
        $this->getResponse()->setHttpResponseCode(501);
    }

    public function optionsAction()
    {
        $this->getResponse()->setBody($this->_doc());
        $this->getResponse()->setHeader('Allow', 'OPTIONS, HEAD, INDEX, GET, POST');
        $this->getResponse()->setHttpResponseCode(200);
    }
}