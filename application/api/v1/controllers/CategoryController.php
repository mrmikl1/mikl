<?php

class V1_CategoryController extends Application_Abstract_RestController
{
    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->getResponse()->setHeader('accept', 'application/json');
    }

    public function getAction()
    {
        $categoryId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $language = $this->getRequest()->getHeader('accept-language');

        $categoryService = new Application_Service_Categories();
        $category = $categoryService->get($accountName, $categoryId, $language);
        $date = date_format(date_create($category->getEdited()), 'D, d M Y H:i:s O');

        $this->getResponse()->setHeader('last-modified', $date);
        $this->getResponse()->setBody($category->toJson());
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function itemsAction()
    {
        $accountName = $this->getParam('accountName');
        $categoryId = $this->getParam('id');

        $this->_forward('index', 'item', 'v1', array(
            'accountName'=>$accountName,
            'spaceId'=>NULL,
            'categoryId'=>$categoryId,
            ));
    }

    public function headAction()
    {
        $categoryId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $categoryService = new Application_Service_Categories();
        $category = $categoryService->get($accountName, $categoryId);
        $contentLength = strlen($category->toJson());
        $date = date_format(date_create($category->getEdited()), 'D, d M Y H:i:s O');

        $this->getResponse()->setHeader('last-modified', $date);
        $this->getResponse()->setHeader('content-length', $contentLength);
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function indexAction() {
        $accountName = $this->getParam('accountName');
        $spaceId = $this->getParam('id');

        $categoryService = new Application_Service_Categories();
        $language = $this->getRequest()->getHeader('accept-language');
        $categoryCollection = $categoryService->getCollection($accountName, $spaceId, $language);
        $categories = $categoryCollection->toArray();
        $body = array(
            'count' => $categoryCollection->count(),
            'categories' => $categories
        );
        $this->getResponse()->setBody(json_encode($body));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function postAction() {
        $accountName = $this->getParam('accountName');
        $data = json_decode($this->getRequest()->getRawBody(), true);

        if ($this->getParam('id'))
            $data['id'] = $this->getParam('id');

        $escapedData = $this->escapeData($data, 'description');

        $categoryService = new Application_Service_Categories();
        $category = $categoryService->save($accountName, $escapedData);
        if ($category instanceof Category_Model_Domain){
            $url = $this->_helper->url->url(array(
                    'accountName' => $accountName,
                    'id'          => $category->getId()
                ), 'rest');
            if ($this->getParam('id')) {
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->setHttpResponseCode(201);
            }
//            $this->getResponse()->setHeader('Location', $url);
            $this->getResponse()->setBody($category->toJson());
        } else {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->setBody(json_encode($category->getMessages()));
            }
    }

    public function putAction() {
        $this->forward('post');
    }

    public function deleteAction()
    {
        $categoryId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $categoryService = new Application_Service_Categories();
        $result = $categoryService->delete($accountName, $categoryId);

        if($result){
            $this->getResponse()->setHttpResponseCode(204);
        } else {
            $this->getResponse()->setHttpResponseCode(400);
        }

    }

    public function optionsAction()
    {
        $this->getResponse()->setBody($this->_doc());
        $this->getResponse()->setHeader('Allow', 'OPTIONS, HEAD, INDEX, GET, POST, PUT, DELETE');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function deleteTranslationAction()
    {
        $categoryId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $language = $this->getParam('language');
        $categoryService = new Application_Service_Categories();
        $result = $categoryService->delete($accountName, $categoryId, $language);

        if ($result === false) {
            $this->getResponse()->setHttpResponseCode(400);
        } else {
            $this->getResponse()->setHttpResponseCode(204);
        }
    }

}
