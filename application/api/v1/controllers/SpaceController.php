<?php

class V1_SpaceController extends Application_Abstract_RestController
{
    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setHeader('content-type', 'application/json;charset=UTF-8');
        $this->getResponse()->setHeader('accept', 'application/json');
    }

    public function getAction()
    {
        $accountName = $this->getParam('accountName');
        $spaceId = $this->getParam('id');
        $language = $this->getRequest()->getHeader('accept-language');

        $spaceService = new Application_Service_Spaces();
        $result = $spaceService->get($accountName, $spaceId, $language);
        $date = date_format(date_create($result->getEdited()), 'D, d M Y H:i:s O');

        $this->getResponse()->setHeader('last-modified', $date);
        $this->getResponse()->setBody($result->toJson());
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function categoriesAction()
    {
        $accountName = $this->getParam('accountName');
        $spaceId = $this->getParam('id');

        $this->_forward('index', 'category', 'v1', array(
                'accountName' => $accountName,
                'id'          => $spaceId,
            ));
    }

    public function itemsAction()
    {
        $accountName = $this->getParam('accountName');
        $spaceId = $this->getParam('id');

        $this->_forward('index', 'item', 'v1', array(
            'accountName'=>$accountName,
            'spaceId'=>$spaceId,
            ));
    }

    public function deleteAction()
    {
        $spaceId = $this->getParam('id');
        $accountName = $this->getParam('accountName');

        $spaceService = new Application_Service_Spaces();
        $result = $spaceService->delete($accountName, $spaceId);
        if ($result) {
            $this->getResponse()->setHttpResponseCode(204);
        } else {
            $this->getResponse()->setHttpResponseCode(400);
        }
    }

    public function headAction()
    {
        $spaceId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $spaceService = new Application_Service_Spaces();
        $space = $spaceService->get($accountName,$spaceId);
        $contentLength = strlen($space->toJson());
        $date = date_format(date_create($space->getEdited()), 'D, d M Y H:i:s O');

        $this->getResponse()->setHeader('last-modified', $date);
        $this->getResponse()->setHeader('content-length', $contentLength);
        $this->getResponse()->setHttpResponseCode(200);

        $this->getResponse()->setHeader('content-length', $contentLength);
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function indexAction()
    {
        $accountName = $this->getParam('accountName');

        $spaceService = new Application_Service_Spaces();
        $language = $this->getRequest()->getHeader('accept-language');
        $spaceCollection = $spaceService->getCollection($accountName, $language);

        if($spaceCollection instanceof Application_Collection_Generic) {
                $spaces = $spaceCollection->toArray();

            $body = array(
                'count' => $spaceCollection->count(),
                'spaces' => $spaces
            );

            $this->getResponse()->setBody(json_encode($body));
            $this->getResponse()->setHttpResponseCode(200);
        }
    }

    public function postAction()
    {
        /* @var $space Space_Model_Domain */

        $accountName = $this->getParam('accountName');

        $data = json_decode($this->getRequest()->getRawBody(), true);

        if ($this->getParam('id')){
            $data['id'] = $this->getParam('id');
        }

        $escapedData = $this->escapeData($data, 'description');


        $spaceService = new Application_Service_Spaces();
        $space = $spaceService->save($accountName, $escapedData);

        if ($space instanceof Space_Model_Domain) {
            $url = $this->_helper->url->url(array(
                'accountName' => $accountName,
                'id'          => $space->getId()
            ), 'rest');
            if ($this->getParam('id')) {
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->setHttpResponseCode(201);
            }
//            $this->getResponse()->setHeader('Location', $url);
            $this->getResponse()->setBody($space->toJson());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->setBody(json_encode($space->getMessages()));
        }
    }

    public function putAction()
    {
        $this->_forward('post');
    }

    public function optionsAction()
    {
        $this->getResponse()->setBody($this->_doc());
        $this->getResponse()->setHeader('Allow', 'OPTIONS, HEAD, INDEX, GET, POST, PUT, DELETE');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function deleteTranslationAction()
    {
        $spaceId = $this->getParam('id');
        $accountName = $this->getParam('accountName');
        $language = $this->getParam('language');
        $spaceService = new Application_Service_Spaces();
        $result = $spaceService->delete($accountName, $spaceId, $language);

        if ($result === false) {
            $this->getResponse()->setHttpResponseCode(400);
        } else {
            $this->getResponse()->setHttpResponseCode(204);
        }
    }

}