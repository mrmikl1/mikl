<?php

class Application_Abstract_Models
{
    public function __construct(array $options = NULL) {
        if(is_array($options))
        {
            foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            $this->$method($value);
            }
        }
        return $this;
    }

    public function toArray()
    {
        foreach ($this as $key => $value) {
            $toArray[trim($key, '_')] = $value;
        }

        return $toArray;
    }
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
