<?php

abstract class Application_Abstract_RestController extends Zend_Rest_Controller
{
    public function escapeData($data, $field = NULL)
    {
        foreach($data as $key => $value){
            if ($field == $key) {
                continue;
            }
            $value = $this->_escape($value);
            $data[$key] = $value;
        }
        return $data;
    }

    protected function _escape($data)
    {
        if (is_array($data)) {
            $res = array();
            foreach ($data as $key => $value) {
                $res[$key] = $this->_escape($value);
            }
            return $res;
        } else {
            return $this->view->escape($data);
        }
    }

    /**
     * Get doc in Rest view
     *
     *
     *
     * @return string returns rest doc
     */

    protected function _doc()
    {
        $service = $this->getRequest()->getControllerName();
        if ($service == 'category') {
            $services = ucfirst(str_replace('y', 'ies', $service));
        } else {
            $services = ucfirst($service) . 's';
        }

        $reflector = new ReflectionClass('Application_Service_' . $services);

        $docArray = array();
        if ($service == 'account') {
            $docArray['POST'] = $this->_post($service, $reflector);
            $docArray['GET'] = $this->_get($service, $reflector);

        } else {
            $docArray['POST'] = $this->_post($service, $reflector);
            $docArray['GET'] = $this->_get($service, $reflector);
            $docArray['PUT'] = $this->_put($service, $reflector);
            $docArray['DELETE'] = $this->_delete($service, $reflector);
        }

        return json_encode($docArray);
    }

    protected function _post($service, $reflector)
    {
        if ($service == 'account') {
            $docblockSave = $reflector->getMethod('create')->getDocComment();
        } else {
            $docblockSave = $reflector->getMethod('save')->getDocComment();
        }
        $docblockSave = str_replace(array('*', '<br />', '/'), '', $docblockSave);
        $docblock = explode('[', $docblockSave);
        $docblock1 = explode(']', $docblock[1]);
        $docblock1 = trim($docblock1[0]);
        $docblock1 = explode(';', $docblock1);

        $i = 0;
        foreach ($docblock1 as $doc) {
            $doc = trim($doc);
            $docArr[$i] = explode(' ', $doc, 4);
            $i++;
        }

        unset($docArr[$i-1]);
        $i=0;

        $paramPostArray = array();
        foreach ($docArr as $doc) {
            $paramPostArray[$doc[0]] = array();
            $paramPostArray[$doc[0]]['type'] = $doc[1];
            if ($doc[2] == 'required') {
                $paramPostArray[$doc[0]]['required'] = "true";
            } else {
                $paramPostArray[$doc[0]]['required'] = "false";
            }
            $i++;
            $paramPostArray[$doc[0]]['description'] = $doc[3];
        }

        $docblock2 = explode(']', $docblock[2]);
        $examplePost = '{' . $docblock2[0] . '}';

        $post = array();
        $post['description'] = 'Create ' . $service . '';
        $post['parametrs'] = $paramPostArray;
        $post['example'] = json_decode($examplePost);

        return $post;
    }

    protected function _put($service, $reflector)
    {
        $docblockPut = $reflector->getMethod('save')->getDocComment();
        $docblockPut = str_replace(array('*', '<br />', '/'), '', $docblockPut);
        $docblock = explode('[', $docblockPut);
        $docblock1 = explode(']', $docblock[1]);
        $docblock1 = trim($docblock1[0]);
        $docblock1 = explode(';', $docblock1);

        $i = 0;
        foreach ($docblock1 as $doc) {
            $doc = trim($doc);
            $docArr[$i] = explode(' ', $doc, 4);
            $i++;
        }

        unset($docArr[$i-1]);
        $i=0;

        $paramPutArray = array();
        foreach ($docArr as $doc) {
            $paramPutArray[$doc[0]] = array();
            $paramPutArray[$doc[0]]['type'] = $doc[1];
            if ($doc[2] == 'required') {
                $paramPutArray[$doc[0]]['required'] = "true";
            } else {
                $paramPutArray[$doc[0]]['required'] = "false";
            }
            $i++;
            $paramPutArray[$doc[0]]['description'] = $doc[3];
        }
        $paramPutArray['id']['type'] = "int";
        $paramPutArray['id']['required'] = "true";
        $paramPutArray['id']['description'] = 'id of ' . $service . ', you want to edit';

        $docblock2 = explode(']', $docblock[2]);
        $examplePut = '{' . $docblock2[0] . '}';

        $put = array();
        $put['description'] = 'Edit ' . $service;
        $put['parametrs'] = $paramPutArray;
        $put['example'] = json_decode($examplePut);

        return $put;
    }

    protected function _get($service, $reflector)
    {
        $docblockGet = $reflector->getMethod('get')->getDocComment();
        $docblockGet = str_replace(array('*', '<br />', '/'), '', $docblockGet);
        $docblockGet = explode('@', $docblockGet);
        if ($service == 'account') {
            $docblockGet = explode(' ', $docblockGet[1], 2);
        } else {
            $docblockGet = explode(' ', $docblockGet[2], 2);
        }
        $docblockGet = explode('Example', $docblockGet[1]);

        $paramGet = explode(' ', $docblockGet[0], 4);
        $paramGet[3] = trim($paramGet[3]);
        $paramGet[1] = str_replace('$', '', $paramGet[1]);

        $paramGetForArray = array();
        $paramGetForArray[$paramGet[1]] = array();
        $paramGetForArray[$paramGet[1]]['type'] = $paramGet[0];
        $paramGetForArray[$paramGet[1]]['required'] = "true";
        $paramGetForArray[$paramGet[1]]['description'] = $paramGet[3];

        $exampleGet = explode('[', $docblockGet[1]);
        $exampleGet = explode(']', $exampleGet[1]);
        $exampleGet = trim($exampleGet[0]);
        $exampleGet = '{' . $exampleGet . '}';

        $get = array();
        $get['description'] = 'Get ' . $service;
        $get['parametrs'] = $paramGetForArray;
        $get['example'] = json_decode($exampleGet);

        return $get;
    }

    protected function _delete($service, $reflector)
    {
        $docblockDelete = $reflector->getMethod('delete')->getDocComment();
        $docblockDelete = str_replace(array('*', '<br />', '/'), '', $docblockDelete);
        $docblockDelete = explode('@', $docblockDelete);
        $docblockDelete = explode(' ', $docblockDelete[2], 2);
        $docblockDelete = explode('Example', $docblockDelete[1]);

        $paramDelete = explode(' ', $docblockDelete[0], 4);
        $paramDelete[3] = trim($paramDelete[3]);
        $paramDelete[1] = str_replace('$', '', $paramDelete[1]);

        $paramDeleteForArray = array();
        $paramDeleteForArray[$paramDelete[1]] = array();
        $paramDeleteForArray[$paramDelete[1]]['type'] = $paramDelete[0];
        $paramDeleteForArray[$paramDelete[1]]['required'] = "true";
        $paramDeleteForArray[$paramDelete[1]]['description'] = $paramDelete[3];

        $exampleDelete = explode('[', $docblockDelete[1]);
        $exampleDelete = explode(']', $exampleDelete[1]);
        $exampleDelete = trim($exampleDelete[0]);
        $exampleDelete = '{' . $exampleDelete . '}';

        $delete = array();
        $delete['description'] = 'Delete ' . $service;
        $delete['parametrs'] = $paramDeleteForArray;
        $delete['example'] = json_decode($exampleDelete);

        return $delete;
    }
}
