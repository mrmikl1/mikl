<?php

abstract class Application_Abstract_Form extends Zend_Form
{
    /**
     * @var array
     */
    protected $_params;
    protected $_translate;

    public function __construct($params = array(), $options = null)
    {
        $this->setDisableLoadDefaultDecorators(true);
        parent::__construct($options);

        $this->_setParams($params);
        $this->_translate = Zend_Registry::get('Zend_Translate');
        $this->initForm();
        $this->initDecorator();
    }

    protected function _getElementDefaultOptions() {
        return array(
            'DisableLoadDefaultDecorators' => true,
            'Decorators' => array(
                array('ViewScript',
                    array(
                        'viewScript' => 'forms/element.phtml')
                    ),
                )
        );
    }
    public function initDecorator()
    {
        $this->addDecorator('FormElements')
             ->addDecorator('Form', array('class' => 'form-horizontal'));
    }

    protected function _getParams()
    {
        return $this->_params;
    }

    protected function _setParams($params)
    {
        $this->_params = $params;
        return $this->_params;
    }

    protected function _getParam($paramName)
    {
        if (array_key_exists($paramName, $this->_getParams())) {
            return $this->_params[$paramName];
        } else {
            return false;
        }
    }
}

?>
