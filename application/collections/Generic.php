<?php
/**
 * Description of Generic
 *
 * @author Igor Dobritskiy
 */

class Application_Collection_Generic extends ArrayObject
{
    public function setSelected($id)
    {
        foreach ($this as $domain) {
            if ($id == $domain->getId()) {
                $domain->setSelected(TRUE);
            } else {
                $domain->setSelected(FALSE);
            }
        }
    }

    public function getSelected()
    {
        foreach ($this as $domain) {
            if ($domain->isSelected()) {
                $selected = $domain;
            }
        }

        if (empty($selected)) {
            $selected = $this->getIterator()->current();
            $selected->setSelected(TRUE);
        }

        return $selected;
    }
    public function toArray()
    {
        $domains = array();
        foreach ($this as $domain) {
            $domains[] = $domain->toArray();
        }
        return $domains;
    }
}
