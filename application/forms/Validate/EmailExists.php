<?php
class Application_Form_Validate_EmailExists extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => "Incorrect email entered"
    );

    protected $_accountName;

    public function __construct(array $options = array())
    {
        $this->_accountName = $options['accountName'];
    }

    public function isValid($value)
    {

        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($this->_accountName);


        $validator = new Zend_Validate_Db_RecordExists(
            array(
                'table'   => 'users',
                'field'   => 'email'
            )
        );
        $clause = $validator->getAdapter()->quoteInto($validator->getAdapter()->quoteIdentifier('account_id') . ' = ?', $account->getId());
        $validator->setExclude($clause);

        if ($validator->isValid($value)) {
            return true;
        }

        $this->_error(self::NOT_MATCH);
        return false;
    }
}
