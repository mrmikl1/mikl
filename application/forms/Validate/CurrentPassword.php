<?php
class Application_Form_Validate_CurrentPassword extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => "Incorrect password entered"
    );

    protected $_accountName;
    protected $_id;

    public function __construct(array $options = array())
    {
        $this->_accountName = $options['accountName'];
        $this->_id = $options['id'];
    }

    public function isValid($value)
    {

        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($this->_accountName);

        $usersMapper = new User_Model_Mapper();
        $user = $usersMapper->findByAccountIdAndUserId($account, $this->_id);

        if ($user->getPassword() == md5($value)) {
            return true;
        }

        $this->_error(self::NOT_MATCH);
        return false;
    }
}
