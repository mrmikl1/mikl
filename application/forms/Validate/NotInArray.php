<?php

class Application_Form_Validate_NotInArray extends Zend_Validate_InArray
{
    const CONTAINS = 'contain';

    protected $_messageTemplates = array(
        self::CONTAINS => "Already exists"
    );

    public function isValid($value) {
        $this->_setValue($value);

        if (!in_array($value, $this->_haystack)) {
            return true;
        }

        $this->_error(self::CONTAINS);
        return false;
    }


}
