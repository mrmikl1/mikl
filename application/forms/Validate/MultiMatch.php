<?php

/**
 * This validator supports any number of regular expression patterns, each with its custom error message.
 * (Zend Form Elements allow only one Regex validator.)
 *
 * Only a single error is returned -- for the first non-matching pattern.
 * (Translation-compatible -- just use a token as the custom error message.)
 *
 * USAGE:
 * $inputElement->addValidator(new Validator_MultiMatch(array(
 * '/^[a-z]/i' => "Your username must begin with a letter."
 * , '/^[a-z_0-9]+$/i' => "The Username may contain letters and numbers only."
 * )));
 *
 * Or, for I18N support:
 * '/^[a-z]/i' => 'login.form.username.errFirstChar',
 * '/^[a-z_0-9]+$/i' => 'login.form.username.errInvalidChar'
 *
 * @author bfoust
 */
class Application_Form_Validate_MultiMatch extends Zend_Validate_Abstract
{
        protected $_messageTemplates = array(
                'errMsg' => "Invalid."
        );

    /**
     * @param array $patternMsgArray (see usage in class comment)
     */
    public function __construct ($patternMsgArray)
    {
        $this->_patternMsgArray = $patternMsgArray;
    }

    /**
     * Validate a string by matching each of our patterns against it.
     * Return an error for the first pattern that doesn't match.
     *
     * @param  string $value to run validation regular expressions against
     * @throws Zend_Validate_Exception if there is a fatal error in pattern matching
     * @return boolean false if any tests fail, true otherwise
     */
    public function isValid ($value)
    {
        $checkVal = (string) $value;

        foreach ($this->_patternMsgArray as $pattern => $errMsg)
        {
                $status = @preg_match($pattern, $checkVal);

                if (!$status)
                {
                    $this->_messageTemplates['errMsg'] = $errMsg; // store for use by _error()
                    $this->_error(NULL);
                    return false;
                }
        }

        return true;
    }
}
