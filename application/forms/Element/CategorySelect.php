<?php

class Application_Form_Element_CategorySelect extends Zend_Form_Element_Select
{
    /**
     * @param string $accountName
     * @param int $spaceId
     *
     * @return array Options for category select
     */
    public function getOptions($accountName, $spaceId)
    {
        $spaceService = new Application_Service_Spaces();
        $space        = $spaceService->get($accountName, $spaceId);

        if (!$space) {
            return array();
        }

        $categoryService = new Application_Service_Categories();
        $categoriesCollection = $categoryService->getCollection($accountName, $spaceId);

        $selectOptions = array();
        foreach ($categoriesCollection as $category) {
            /* @var $category Category_Model_Domain */
            $selectOptions[] = array('key' => $category->getId(), 'value' => $category->getName());
        }

        return $selectOptions;
    }
}
?>
