<?php

class Application_Form_Element_SpaceSelect extends Zend_Form_Element_Select
{

    /**
     * @param string $accountName
     *
     * @return array Options for space select
     */
    public function getOptions($accountName)
    {
        $spaceService = new Application_Service_Spaces();
        $spacesCollection = $spaceService->getCollection($accountName);

        foreach ($spacesCollection as $space) {
            /* @var $space Space_Model_Domain */
            $selectOptions[] = array('key' => $space->getId(), 'value' => $space->getName());
        }

        return $selectOptions;
    }
}
?>
