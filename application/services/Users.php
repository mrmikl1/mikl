<?php

class Application_Service_Users
{
    public function getByAccountName($accountName)
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);
        $userMapper = new User_Model_Mapper();
        return $userMapper->findByAccountId($account->getId());
    }
}