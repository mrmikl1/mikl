<?php
/**
 * Items Service
 * This service controls operations on the items
 *
 * @author SparklySoft
 * @link http://sparklysoft.com/
 * @version 1.0
 */
class Application_Service_Items
{
    /**
     * Item save
     *
     * @param string $accountName required Name of account, you want to save item in
     * @param array $data Array with information to save item <br />
     * [ <br />
     * spaceId int required Space's id, in which you want save the item; <br />
     * categoryId int required Category's id, in which you want save the item; <br />
     * title string required Items's title; <br />
     * content string non-required Item's content; <br />
     * position int non-required Item's position; <br />
     * ] <br /><br />
     * Example of item's data <br />
     * [ <br />
     * "spaceId": "1", <br />
     * "categoryId": "1", <br />
     * "title": "Question?", <br />
     * "content": "Answer",
     * "position": "1" <br />
     * ]
     * @return Item_Model_Domain
     */
    public function save($accountName, $data)
    {
        $createItemForm = new Item_Form_Edit(array(
            'accountName'=>$accountName,
            'spaceId'=>$data['spaceId'],
            'categoryId'=>$data['categoryId']
            )
        );

        if(!$createItemForm->isValid($data)) {
            return $createItemForm;
        }

        $accountMapper = new Account_Model_Mapper();
        $itemMapper    = new Item_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        $data['accountId'] = $account->getId();

        if(isset($data['newTranslation'])) {
            $newLang = $data['newTranslation'];
            unset($data['newTranslation']);
            $item = new Item_Model_Domain($data);
            $languages = $item->getTranslations();
            if(array_key_exists($newLang, $languages) && $languages[$newLang]) {
                $createItemForm->setErrorMessages(array("Current language is already exists!"));
                return $createItemForm;
            }
            $itemMapper->changeTranslationLanguage($item, $newLang);
            $item->setLanguage($newLang);
        } else {
            $item = new Item_Model_Domain($data);
            $itemMapper->save($item);
            $item->getTranslations();
        }

        if ($item) {
           return $item;
        }
    }

    /**
     * Item get
     *
     * @param string $accountName required Name of account, you want to get item
     * @param int $itemId required Item's id, you want to get<br />
     * Example of item's data <br />
     * [ <br />
     * "id": "1", <br />
     * ]
     * @return Item_Model_Domain
     */

    public function get($accountName, $itemId, $language = 'en')
    {
        $accountMapper = new Account_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        if (!$account) {
            return false;
        }

        $itemMapper = new Item_Model_Mapper();
        $item       = $itemMapper->find($itemId, $account, $language);
        if($item) {
            $item->getTranslations();
        }

        return $item;
    }

    /**
     * Item delete
     *
     * @param string $accountName required Name of account, you want to delete item
     * @param int $itemId required Item's id, you want to delete<br />
     * Example of item's data <br />
     * [ <br />
     * "id": "1", <br />
     * ]
     * @return int
     */

    public function delete($accountName, $itemId, $language = NULL)
    {
        $accountMapper = new Account_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        $item = new Item_Model_Domain();
        $item->setId($itemId);
        $item->setAccountId($account->getId());

        $itemMapper = new Item_Model_Mapper();
        $result     = $itemMapper->delete($item, $account, $language);

        return $result;
    }

    public function changePublishingStatus($id, $accountName, $published) {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);
        $itemMapper = new Item_Model_Mapper();
        return $itemMapper->changePublishingStatus($id, $account, $published);
    }
    /**
     * Item getCollection
     *
     *
     * @param string $accountName required Name of account, you want to get item's collection
     * @param int $spaceId required Space's id, you want to get Collection of<br />
     * @param int $categoryId required Category's id, you want to get Collection of<br />
     *
     * @return Application_Collection_Generic returns collection of items
     */

    public function getCollection($accountName, $spaceId = NULL, $categoryId = NULL, $language = NULL, $status = Item_Model_Domain::STATUS_ANSWERED, $published = Item_Model_Domain::PUBLISHED)
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);
        $itemMapper = new Item_Model_Mapper();
        $itemsCollection = $itemMapper->getCollection($account->getId(), $spaceId, $categoryId, $language, $status, $published);
        return $itemsCollection;
    }
}
