<?php
class Application_Service_Languages
{
    public function getCollection($accountName)
    {
        $languageMapper = new Language_Model_Mapper();
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);
        if($account) {
            return $languageMapper->getCollection($account);
        } else {
            return $languageMapper->getAllLanguages();
        }
    }
}
