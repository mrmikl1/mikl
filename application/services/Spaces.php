<?php
/**
 * Spaces Service
 * This service controls operations on the spaces
 *
 * @author SparklySoft
 * @link http://sparklysoft.com/
 * @version 1.0
 */
class Application_Service_Spaces
{
    /**
     * Space save
     *
     * @param string $accountName required Name of account, you want to save space in
     * @param array $data required Array with information to save space <br />
     * [ <br />
     * name string required Space's name; <br />
     * description string non-required Space's description; <br />
     * position int non-required Space's position; <br />
     * ] <br /><br />
     * Example of space's data <br />
     * [ <br />
     * "name": "SpaceName", <br />
     * "description": "SpaceDescription",
     * "position": "1" <br />
     * ]
     * @return Space_Model_Domain
     */
    public function save($accountName, $data)
    {
        $createSpaceForm = new Space_Form_Edit();
        if (!$createSpaceForm->isValid($data)) {
            return $createSpaceForm;
        }
        $accountMapper = new Account_Model_Mapper();
        $spaceMapper   = new Space_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        $data['accountId'] = $account->getId();

        if(isset($data['newTranslation'])) {
            $newLang = $data['newTranslation'];
            unset($data['newTranslation']);
            $space = new Space_Model_Domain($data);
            $languages = $space->getTranslations();
            if(array_key_exists($newLang, $languages) && $languages[$newLang]) {
                $createSpaceForm->setErrorMessages(array("Current language is already exists!"));
                return $createSpaceForm;
            }
            $spaceMapper->changeTranslationLanguage($space, $newLang);
            $space->setLanguage($newLang);
        } else {
            $space = new Space_Model_Domain($data);
            $spaceMapper->save($space);
            $space->getTranslations();
            if (empty($data['id'])) {
                $categoryService = new Application_Service_Categories();
                $category        = new Category_Model_Domain();

                $dataCategory = array(
                    'name'     => $category->setName('Default Category'),
                    'spaceId'  => $category->setSpaceId($space->getId()),
                    'language' => $space->getLanguage(),
                    'default'  => $category->setDefault(TRUE)
                );

                $categoryService->save($accountName, $dataCategory);
            }
        }
        if ($space) {
            return $space;
        }
    }

    /**
     * Space get
     *
     * @param string $accountName required Name of account, you want to get space
     * @param int $spaceId required Space's id, you want to get<br />
     * Example of space's data <br />
     * [ <br />
     * "id": "1" <br />
     * ]
     * @return Space_Model_Domain
     */

    public function get($accountName, $spaceId, $language = 'en')
    {
        $accountMapper = new Account_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        if (!$account) {
            return false;
        }

        $spaceMapper = new Space_Model_Mapper();
        $space       = $spaceMapper->find($spaceId, $account, $language);
        if($space) {
            $space->getTranslations();
        }
        return $space;
    }

    /**
     * Space delete
     *
     * @param string $accountName required Name of account, you want to delete space
     * @param int $spaceId required Space's id, you want to delete<br />
     * Example of space's data <br />
     * [ <br />
     * "id": "1" <br />
     * ]
     * @return int
     */

    public function delete($accountName, $spaceId, $language = NULL)
    {
        $accountMapper = new Account_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        $space = new Space_Model_Domain();
        $space->setId($spaceId);
        $space->setAccountId($account->getId());
        $spaceMapper = new Space_Model_Mapper();
        $result      = $spaceMapper->delete($space, $account, $language);

        return  $result;
    }


    /**
     * Space getCollection
     *
     *
     * @param string $accountName required Name of account, you want to get space's collection
     *
     * @return Application_Collection_Generic returns collection of spaces
     */

    public function getCollection($accountName, $language = NULL)
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);
        if (!$account instanceof Account_Model_Domain) {
            return false;
        }
        $spaceMapper = new Space_Model_Mapper();
        return $spaceMapper->getCollection($account->getId(), $language);
    }
}

