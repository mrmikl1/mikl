<?php
/**
 * Permissions Service
 * This service controls permissions
 *
 * @author SparklySoft
 * @link http://sparklysoft.com/
 * @version 1.0
 */
class Application_Service_Permissions
{
    /**
     * Permission checks
     *
     * @param string $accountName Name of account
     * @param User_Model_Domain $user User's data
     * @return TRUE|Application_Exception_403
     */
    public function hasAccess($accountName, $user)
    {
        $this->_translate = Zend_Registry::get('Zend_Translate');
        $accountMapper = new Account_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        if (!(Zend_Auth::getInstance()->hasIdentity() && $account->getId() == $user->getAccountId())) {
            throw new Application_Exception_401 ();
        }

        return TRUE;
    }
}