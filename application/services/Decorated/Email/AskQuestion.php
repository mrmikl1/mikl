<?php

class Application_Service_Decorated_Email_AskQuestion extends Application_Service_Decorated_Abstract_Items
{
    function save($accountName, $data)
    {
        $item = $this->_component->save($accountName, $data);

        if (APPLICATION_ENV != 'testing' && $item) {
             $config = Zend_Registry::get('config');
             $configEmail = array(
                 'auth'      => $config->get('email')->auth,
                 'username'  => $config->get('email')->username,
                 'password'  => $config->get('email')->password,
                 'ssl'       => $config->get('email')->ssl,
                 'port'      => $config->get('email')->port,
             );

             $transport = new Zend_Mail_Transport_Smtp($config->get('email')->hostname, $configEmail);
             $mail = new Zend_Mail('UTF-8');
             $urlHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('url');

             $editItemUrl = $urlHelper->url(array(
                         'id' => $item->getId()
                     ), 'editItem'
                 );
             $spaceService      = new Application_Service_Spaces();
             $space             = $spaceService->get($accountName, $item->getSpaceId());

             $categoryService   = new Application_Service_Categories();
             $category          = $categoryService->get($accountName, $item->getCategoryId());

             $userService       = new Application_Service_Users();
             $user              = $userService->getByAccountName($accountName);

             $mail->setBodyText(
                 "Space: " . $space->getName() . ", \n" .
                 "Category: " . $category->getName() . ", \n" .
                 "Question: " . $item->getTitle() . ", \n" .
                 "Description: " . strip_tags($item->getContent()) . ", \n" .
                 "Activation URL: " . $editItemUrl
             );
             $mail->setFrom($config->get('email')->fromEmail, $config->get('email')->fromFullName);
             $mail->addTo($user->getEmail(), $user->getFirstName() . " " . $user->getLastName());
             $mail->setSubject('New question was asked');
             $mail->send($transport);
        }
        return $item;
    }
}