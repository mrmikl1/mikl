<?php

class Application_Service_Decorated_Email_AnswerQuestion extends Application_Service_Decorated_Abstract_Items
{
    function save($accountName, $data)
    {
        $item = $this->_component->save($accountName, $data);

        if (APPLICATION_ENV != 'testing' && $item && $item->getEmail()) {
            $config = Zend_Registry::get('config');
            $configEmail = array(
                'auth'      => $config->get('email')->auth,
                'username'  => $config->get('email')->username,
                'password'  => $config->get('email')->password,
                'ssl'       => $config->get('email')->ssl,
                'port'      => $config->get('email')->port,
            );

            $transport = new Zend_Mail_Transport_Smtp($config->get('email')->hostname, $configEmail);
            $mail = new Zend_Mail('UTF-8');
            $urlHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('url');

            $answerLink = $urlHelper->url(array(
                        'id' => $item->getSpaceId()
                    ), 'viewSpaceFrontend'
                ) . "#i" . $item->getId();

            $mail->setBodyText(
                "Question: " . $item->getTitle() . ", \n" .
                "URL: " . $answerLink
            );
            $mail->setFrom($config->get('email')->fromEmail, $config->get('email')->fromFullName);
            $mail->addTo($data['email'], $data['fullName'] ? $data['fullName'] : "Dear user");
            $mail->setSubject('Your question has bin answered!');
            $mail->send($transport);
        }
        return $item;
    }
}