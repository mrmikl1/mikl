<?php

class Application_Service_Decorated_Exceptions_404_Accounts extends Application_Service_Decorated_Abstract_Accounts
{
    function get($name)
    {
        $account = $this->_component->get($name);

        if (!$account) {
            throw new Application_Exception_404();
        }

        return $account;
    }
}