<?php

class Application_Service_Decorated_Exceptions_404_Items extends Application_Service_Decorated_Abstract_Items
{
    function get($accountName, $itemId)
    {
        $item = $this->_component->get($accountName, $itemId, NULL);
        $authorizationFlag = Zend_Auth::getInstance()->hasIdentity();

        if (!$item || !$authorizationFlag && $item->getStatus() == Item_Model_Domain::STATUS_UNANSWERED) {
            throw new Application_Exception_404();
        }

        return $item;
    }
}