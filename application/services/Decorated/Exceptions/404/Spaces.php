<?php

class Application_Service_Decorated_Exceptions_404_Spaces extends Application_Service_Decorated_Abstract_Spaces
{
    function get($accountName, $spaceId)
    {
        $space = $this->_component->get($accountName, $spaceId, NULL);

        if (!$space) {
            throw new Application_Exception_404();
        }

        return $space;
    }
}