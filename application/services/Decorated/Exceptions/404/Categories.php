<?php

class Application_Service_Decorated_Exceptions_404_Categories extends Application_Service_Decorated_Abstract_Categories
{
    function get($accountName, $categoryId)
    {
        $category = $this->_component->get($accountName, $categoryId, NULL);

        if (!$category) {
            throw new Application_Exception_404();
        }

        return $category;
    }
}