<?php

class Application_Service_Decorated_Validation_AskQuestion extends Application_Service_Decorated_Abstract_Items
{
    function save($accountName, $data)
    {
        $createItemForm = new Default_Form_AskQuestion(
                array(
                    'accountName' => $accountName,
                    'spaceId'     => $data['spaceId'],
                    'categoryId'  => $data['categoryId'],
                )
            );

        if (!$createItemForm->isValid($data)) {
            return $createItemForm;
        }
        return $this->_component->save($accountName, $data);
    }
}