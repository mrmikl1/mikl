<?php

abstract class Application_Service_Decorated_Abstract_Categories extends Application_Service_Categories
{
    protected $_component;
    public function __construct(Application_Service_Categories $categoryService)
    {
        $this->_component = $categoryService;
    }
    public function get($accountName, $spaceId)
    {
        return $this->_component->get($accountName, $spaceId);
    }
}