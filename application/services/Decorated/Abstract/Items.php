<?php

abstract class Application_Service_Decorated_Abstract_Items extends Application_Service_Items
{
    protected $_component;
    public function __construct(Application_Service_Items $itemService)
    {
        $this->_component = $itemService;
    }

    public function save($accountName, $data)
    {
        return $this->_component->save($accountName, $data);
    }
    
    public function get($accountName, $itemId)
    {
        return $this->_component->get($accountName, $itemId);
    }
}
