<?php

abstract class Application_Service_Decorated_Abstract_Accounts extends Application_Service_Accounts
{
    protected $_component;
    public function __construct(Application_Service_Accounts $accountService)
    {
        $this->_component = $accountService;
    }
    public function get($name)
    {
        return $this->_component->get($name);
    }
}