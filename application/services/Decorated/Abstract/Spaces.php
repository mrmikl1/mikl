<?php

abstract class Application_Service_Decorated_Abstract_Spaces extends Application_Service_Spaces
{
    protected $_component;
    public function __construct(Application_Service_Spaces $spaceService)
    {
        $this->_component = $spaceService;
    }
    public function get($accountName, $spaceId)
    {
        return $this->_component->get($accountName, $spaceId);
    }
}