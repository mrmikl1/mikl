<?php
/**
 * Auth Service
 * This service controls Authorization
 *
 * @author SparklySoft
 * @link http://sparklysoft.com/
 * @version 1.0
 */
class Application_Service_Auth
{
    /**
     * Account log in
     *
     * @param string $accountName required Name of account, you want to log in
     * @param array $data Array with information to log in <br />
     * [ <br />
     * email string required User's email; <br />
     * password string required User's password; <br />
     * ] <br /><br />
     * Example of log in data <br />
     * [ <br />
     * "email": "john@gmail.com", <br />
     * "password": "pass" <br />
     * ]
     * @return User_Model_Domain|Auth_Form_Login
     */
    public function login($data, $accountName)
    {
        $loginForm = new Auth_Form_Login();

        if ($loginForm->isValid($data)) {
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();

            $authAdapter = new ZendX_Auth_Adapter_DbTable($dbAdapter);

            $accountMapper = new Account_Model_Mapper();

            $account = $accountMapper->findByName($accountName);

            $authAdapter
                    ->setAccountId($account->getId())
                    ->setEmail($data['email'])
                    ->setCredential(md5($data['password']));
            $result = $authAdapter->authenticate($authAdapter);
            if ($result->isValid()) {
                $user = new User_Model_Domain();
                $userData = $authAdapter->getResultRowObject();
                $user->setEmail($userData->email);
                $user->setPassword($userData->password);
                $user->setAccountId($userData->account_id);
                $user->setActivationHash($userData->activation_hash);
                $user->setActive($userData->active);
                $user->setFirstName($userData->first_name);
                $user->setLastName($userData->last_name);
                $user->setId($userData->user_id);

                $auth   = Zend_Auth::getInstance();
                $storage = $auth->getStorage();
                $storage->write($user);
                return $user;
            } else {
                $loginForm->getElement('email')->markAsError();
                $loginForm->getElement('password')->markAsError();
                return $loginForm;
            }
        } else {
            return $loginForm;
        }
    }
}
