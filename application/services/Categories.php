<?php
/**
 * Categories Service
 * This service controls operations on the categories
 *
 * @author SparklySoft
 * @link http://sparklysoft.com/
 * @version 1.0
 */
class Application_Service_Categories
{
    /**
     * Category save
     *
     * @param string $accountName required Name of account, you want to save category in
     * @param array $data required Array with information to save category <br />
     * [ <br />
     * spaceId int required Space's id, in which you want save the category; <br />
     * name string required Category's name; <br />
     * description string non-required Category's description; <br />
     * position int non-required Category's position; <br />
     * ] <br /><br />
     * Example of category's data <br />
     * [ <br />
     * "spaceId": "1", <br />
     * "name": "CategoryName", <br />
     * "description": "CategoryDescription",
     * "position": "1" <br />
     * ]
     * @return Category_Model_Domain
     */

    public function save($accountName, $data)
    {
        $createCategoryForm = new Category_Form_Edit(array('accountName' => $accountName));
        if (!$createCategoryForm->isValid($data)) {
            return $createCategoryForm;
        }
        $accountMapper  = new Account_Model_Mapper();
        $categoryMapper = new Category_Model_Mapper();
        $account        = $accountMapper->findByName($accountName);

        $data['accountId'] = $account->getId();

        if(isset($data['newTranslation'])) {
            $newLang = $data['newTranslation'];
            unset($data['newTranslation']);
            $category = new Category_Model_Domain($data);
            $languages = $category->getTranslations();
            if(array_key_exists($newLang, $languages) && $languages[$newLang]) {
                $createCategoryForm->setErrorMessages(array("Current language is already exists!"));
                return $createCategoryForm;
            }
            $categoryMapper->changeTranslationLanguage($category, $newLang);
            $category->setLanguage($newLang);
        } else {
            $category = new Category_Model_Domain($data);
            $categoryMapper->save($category);
            $category->getTranslations();
        }

        if ($category) {
           return $category;
        }
    }

    /**
     * Category get
     *
     * @param string $accountName required Name of account, you want to get category
     * @param int $categoryId required Category's id, you want to get<br />
     * Example of category's data <br />
     * [ <br />
     * "id": "1", <br />
     * ]
     * @return Category_Model_Domain
     */

    public function get($accountName, $categoryId, $language = 'en')
    {
        $accountMapper = new Account_Model_Mapper();
        $account       = $accountMapper->findByName($accountName);

        if (!$account) {
            return false;
        }

        $categoryMapper = new Category_Model_Mapper();
        $category       = $categoryMapper->find($categoryId, $account, $language);
        if($category){
            $category->getTranslations();
        }

        return $category;
    }

    /**
     * Category delete
     *
     * @param string $accountName required Name of account, you want to delete category
     * @param int $categoryId required Category's id, you want to delete<br />
     * Example of category's data <br />
     * [ <br />
     * "id": "1", <br />
     * ]
     * @return int
     */

    public function delete($accountName, $categoryId, $language = NULL)
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);

        $category = new Category_Model_Domain();
        $category->setId($categoryId);
        $category->setAccountId($account->getId());

        $categoryMapper = new Category_Model_Mapper();
        $result         = $categoryMapper->delete($category, $account, $language);

        return $result;
    }

    /**
     * Category getCollection
     *
     *
     * @param string $accountName required Name of account, you want to get category's collection
     * @param int $spaceId required Space's id, you want to get Collection of<br />
     *
     * @return Application_Collection_Generic returns collection of categories
     */
    public function getCollection($accountName, $spaceId = NULL, $language = 'en')
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($accountName);

        $categoryMapper = new Category_Model_Mapper();

        if(!$spaceId){
            $categoriesCollection = $categoryMapper->getCollection($account, NULL, $language);
        } else {
            $spaceMapper = new Space_Model_Mapper();
            $space = $spaceMapper->find($spaceId, $account, $language);
            $categoriesCollection = $categoryMapper->getCollection($account, $space, $language);
        }

        return $categoriesCollection;
    }
}