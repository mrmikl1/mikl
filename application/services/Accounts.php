<?php
/**
 * Accounts Service
 * This service controls creating and getting account
 *
 * @author SparklySoft
 * @link http://sparklysoft.com/
 * @version 1.0
 */

class Application_Service_Accounts
{
    /**
     * Account creation
     *
     * @param array $data Array with information to create account <br />
     * [ <br />
     * firstName string required User's first name; <br />
     * lastName string required User's last name; <br />
     * email string required User's email; <br />
     * password string required User's password; <br />
     * passwordConfirm string required User's password, have to be similar with password; <br />
     * accountName string required User's namespace; <br />
     * ] <br /><br />
     * Example of creation data <br />
     * [ <br />
     * "firstName": "John", <br />
     * "lastName": "Admin", <br />
     * "email": "john@gmail.com", <br />
     * "password": "pass", <br />
     * "passwordConfirm": "pass", <br />
     * "accountName": "john" <br />
     * ]
     * @return Account_Model_Domain
     */
    public function create($data)
    {
        $accountRegisterForm = new Account_Form_Register();
            if (!$accountRegisterForm->isValid($data)) {
                return $accountRegisterForm;
            } else {
                $account = new Account_Model_Domain();
                $account->setName($data['accountName']);
                $accountMapper = new Account_Model_Mapper();
                $accountMapper->save($account);

                //User

                $user = new User_Model_Domain();
                $user->setEmail($data['email']);
                $user->setFirstName($data['firstName']);
                $user->setLastName($data['lastName']);
                $user->setPassword(md5($data['password']));
                $user->setAccountId($account->getId());
                $user->setActivationHash(md5(time()));
                $userMapper = new User_Model_Mapper();
                $userMapper->save($user);

                //Space

                $dataSpace['name'] = 'Default Space';
                $dataSpace['default'] = TRUE;
                $dataSpace['language'] = 'en';
                $spaceService = new Application_Service_Spaces();
                $spaceService->save($account->getName(), $dataSpace);

                if (APPLICATION_ENV != 'testing' && $data['email'] != "OikUj@grofaq.com") {
                    $config = Zend_Registry::get('config');
                    $configEmail = array(
                        'auth'      => $config->get('email')->auth,
                        'username'  => $config->get('email')->username,
                        'password'  => $config->get('email')->password,
                        'ssl'       => $config->get('email')->ssl,
                        'port'      => $config->get('email')->port
                    );

                    $transport = new Zend_Mail_Transport_Smtp($config->get('email')->hostname, $configEmail);

                    $mail = new Zend_Mail('UTF-8');

                    $urlHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('url');

                    $activationUrl = $urlHelper->url(array(
                            'accountName'  => $account->getName(),
                            'hash'         => $user->getActivationHash()),
                            'activation'
                        );

                    $adminUrl = $urlHelper->url(array('accountName' => $account->getName()), 'admin');
                    $frontUrl = $urlHelper->url(array('accountName' => $account->getName()), 'viewSpaceFrontend');

                    $mail->setBodyText(
                        "Email: " . $user->getEmail() . ", \n" .
                        "Password: " . $data['password'] . ", \n" .
                        "Name: " . $user->getFirstName() . " " . $user->getLastName() . ", \n" .
                        "Front area: " . $frontUrl . ", \n" .
                        "Admin area: " . $adminUrl . ", \n" .
                        "Activation URL: " . $activationUrl
                    );
                    $mail->setFrom($config->get('email')->fromEmail, $config->get('email')->fromFullName);
                    $mail->addTo($data['email'], $user->getFirstName() . " " . $user->getLastName());
                    $mail->setSubject('You\'ve been successfully registered');
                    $mail->send($transport);
                }
            }
        return $account;
    }
    /**
     * Account get
     *
     * @param string $name required Name of account that you want to view <br />
     * Example of account's data <br />
     * [ <br />
     * "id": "john" <br />
     * ]
     * @return Account_Model_Domain
     */
    public function get($name)
    {
        $accountMapper = new Account_Model_Mapper();
        $account = $accountMapper->findByName($name);
        return $account;
    }
}
