define([
    'jquery',
    'underscore',
    'backbone',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!templates/items/list.html',
    'views/items/element'
], function($, _, Backbone, ItemsListTemplate, ItemElementView){
    var ItemsListView = Backbone.View.extend({
        el: $('#items-list'),
        events: {
            'click a[type="button"]': 'createItem',
        },
        initialize: function(data) {
            this.collection = data.collection;
            this.render();
        },
        createItem: function(){
            Backbone.history.navigate("create/item/", {trigger: true});
        },

        render: function() {
            var compiledTemplate = _.template(ItemsListTemplate);
            $(this.el).html(compiledTemplate);
            this.collection.each(function(item) {
                var itemElementView = new ItemElementView({model: item});
                $('#'+ $(this.el).attr('id') +'>.list-group').append(itemElementView.render().el);
            }, this);
            return this;
        },
    });
    return ItemsListView;
});