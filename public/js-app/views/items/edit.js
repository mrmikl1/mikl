define([
  'jquery',
  'underscore',
  'backbone',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!templates/items/edit.html',
  'views/items/categorySelector',
  'views/translationsControl',
  'globals'
], function($, _, Backbone, FormTemplate, CategorySelectorView, TranslationsView, App){
    var FormView = Backbone.View.extend({
        el: $('#forms-container'),
        events: {
            'change #itemFormData #spaceId': 'spaceChange',
            'change #itemFormData #categoryId': 'categoryChange',
            'submit #itemFormData': 'saveData',
            'click #itemFormData * *[data-action="changeStatus"]': 'changeStatus',
            'click #itemFormData * *[data-action="changePublishing"]': 'changePublishing',
            'click #itemFormData * #delete': 'deleteItem',
        },
        initialize: function(data) {
            this.set(data);
        },
        saveData: function(e){
            e.preventDefault();

            var data = $("#itemFormData").serializeArray(),
                newData = {},
                self = this;
            for(i=0; i<data.length; i++) {
                newData[data[i].name] = data[i].value;
            }
            if(this.model.get('newTranslation')) {
                newData['newTranslation'] = this.model.get('newTranslation');
            }
            this.model.save(newData, {
                headers: App.getHeaders(),
                success: function(){
                    alert("Item saved successfully!");
                    if(self.model.get('newLang')){
                        var translations = self.model.get('translations');
                        translations[self.model.get('newLang').toString()] = true;
                        self.model.set('translations', translations);
                    }
                    self.model.set({selected: false});
                    self.model.set({selected: true});
                    Backbone.history.navigate("view/item/" + self.model.get('spaceId') + "-" + self.model.get('categoryId') + "-" + self.model.get('id'), {trigger: true});
                    self.render();
                },
            });
        },
        deleteItem: function(){
            var confirmation = confirm('Are you sure?');
            if(confirmation){
                var spaceId = this.model.get('spaceId'),
                    categoryId = this.model.get('categoryId');
                this.model.destroy({
                    headers:App.getHeaders(),
                    success:function(){
                        alert('Item deleted successfully!');
                        Backbone.history.navigate("view/category/" + spaceId + "-" + categoryId, {trigger: true});
                    }
                });
            }
        },
        getTranslation: function(code){
            var self = this,
                headers = _.clone(App.getHeaders());
            headers['Accept-language'] = code;

            this.model.fetch({
                headers: headers,
                success: function(){
                    self.model.set({selected: true}, {silent: true});
                    self.render();
                },
                silent: true
            });
        },
        deleteTranslation: function(){
            var confirmation = confirm('Are you sure?');
            if(confirmation){
                var self = this;
                this.model.deleteTranslation({
                    headers:App.getHeaders(),
                    success:function(){
                        alert('Item translation deleted successfully!');
                        var translations = self.model.get('translations');
                        translations[self.model.get('language')] = false;
                        self.model.set({selected: true});
                        self.render();
                    },
                });
            }
        },
        spaceChange:function(e){
            this.model.set('spaceId', $(e.target).val());
            var self = this;
            this.categories.setSpaceId($(e.target).val());
            this.categories.fetch({
                headers: App.getHeaders(),
                success: function(){
                    self.model.set('categoryId', self.categories.at(0).get('id'));
                    self._renderCategorySelector(self.categories);
                },
            });
        },
        categoryChange: function(e) {
            this.model.set('categoryId',$(e.target).val());
        },
        changeStatus: function(e){
            this.model.set('status', $(e.target).children('input').val());
        },
        changePublishing: function(e){
            this.model.set('published', $(e.target).children('input').val());
        },
        set: function(data){
            this.model      = data.model;
            this.spaces     = data.spaces;
            this.categories = data.categories;
        },
        render: function (){
            var compiledTemplate = _.template(FormTemplate, {
                item:       this.model,
                spaces:     this.spaces,
                languages:  App.getLanguagesCollection()
            });
            $(this.el).html(compiledTemplate);
            this._translationsListener();
            this._renderCategorySelector(this.categories);
        },
        _translationsListener: function(){
            var self = this;
            this.translations = new TranslationsView({model: this.model});
            $('#translations').html(this.translations.render().el);
            this.listenTo(this.translations, 'changeTranslation', function(data){
                self.getTranslation(data.code);
            });
            this.listenTo(this.translations, 'deleteTranslation', function(){
                self.deleteTranslation();
            });
        },
        _renderCategorySelector: function(collection){
            if(_.isUndefined(this.selectorView)) {
                this.selectorView = new CategorySelectorView({collection: collection});
            } else {
                this.selectorView.set({collection: collection});
            }
            $('#category-selector').html(this.selectorView.render().el);
        }
    });
    return FormView;
});