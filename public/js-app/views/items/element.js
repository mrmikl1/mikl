// Filename: views/project/list
define([
  'jquery',
  'underscore',
  'bootstrap',
  'backbone',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!templates/items/element.html',
  'text!templates/languages/translations-list.html',
  'globals'
], function($, _, Bootstrap, Backbone, ItemElementTemplate, PopoverTemplate, App){
    var SpaceElementView = Backbone.View.extend({
        tagName: 'a',
        className: 'list-group-item',
        events: {
            'click': 'itemClick',
            'mouseenter *[data-action="showPopover"]': 'showPopover',
            'mouseleave *[data-action="showPopover"]': 'hidePopover',
        },
        initialize: function(data) {
            this.model = data.model;
            this.listenTo(this.model, 'change:selected', this.render);
        },

        itemClick: function() {
            Backbone.history.navigate(
                "view/item/" +
                    this.model.get('spaceId') + '-' +
                    this.model.get('categoryId') + '-' +
                    this.model.get('id'),
            {trigger: true});
        },
        showPopover: function(e){
            var compiledTemplate = _.template(PopoverTemplate, {
                languages: this.model.get('translations'),
                languagesCollection: App.getLanguagesCollection()
            });
            $(e.currentTarget).popover({
                html: true,
                title: 'Translations',
                content: compiledTemplate,
                container: 'body',
                placement: 'right',
                trigger: 'manual'
            });
            $(e.currentTarget).popover('show');
        },
        hidePopover: function(e){
            $(e.currentTarget).popover('hide');
        },
        render: function() {
            if(this.model.get('selected') === true) {
                $(this.el).addClass('active');
            } else {
                $(this.el).removeClass('active');
            }
            $(this.el).html(_.template(ItemElementTemplate, {item: this.model}));
            return this;
        }
    });
    return SpaceElementView;
});