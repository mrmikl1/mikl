define([
    'jquery',
    'underscore',
    'backbone',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!templates/items/categorySelector.html'
], function($, _, Backbone, SelectorTemplate){
    var SelectorView = Backbone.View.extend({
        initialize: function(data){
            this.set(data);
        },
        set: function(data){
            this.collection = data.collection;
        },
        render: function() {
            $(this.el).html(_.template(SelectorTemplate, {categories: this.collection}));
            return this;
        },
    });
    return SelectorView;
});