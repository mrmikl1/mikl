define([
  'jquery',
  'underscore',
  'backbone',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!templates/translationsControl.html',
  'globals'
], function($, _, Backbone, FormTemplate, App){
    var TranslationsControlView = Backbone.View.extend({
        events: {
            'click *[data-action="getTranslation"][data-code]': 'getTranslation',
            'click *[data-action="createTranslation"][data-code]': 'createTranslation',
            'click *[data-action="changeTranslationLanguage"][data-code]': 'changeTranslationLanguage',
            'click *[data-action="switchWithinTranslations"][data-code]': 'switchWithinTranslations',
            'click #deleteTranslation': 'deleteTranslation',
        },
        initialize: function(data) {
            this.set(data);
        },
        getTranslation: function(e){
            this.trigger('changeTranslation', {code: $(e.currentTarget).data('code')});
            this.model.set({language: $(e.currentTarget).data('code')}, {silent: true});
            this.render();
        },
        createTranslation: function(e) {
            var code = $(e.currentTarget).data('code'),
                translations = this.model.get('translations'),
                languageToSetAsFirstInSelectors = App.getLanguagesCollection().findWhere({code: code});
            App.getLanguagesCollection().remove(languageToSetAsFirstInSelectors); // removing selected from collection
            App.getLanguagesCollection().unshift(languageToSetAsFirstInSelectors); // setting selected as first

            translations[code] = false;
            this.model.set('translations', translations);
            this.model.set('newLang', code);

            this.switchWithinTranslations(e);
        },
        switchWithinTranslations: function (e){
            this.model.set('language', $(e.currentTarget).data('code'));
            this.render();
        },
        changeTranslationLanguage: function(e){
            this.model.set("newTranslation", $(e.currentTarget).data('code'));
            var translations = this.model.get('translations');
            delete(translations[this.model.get('language')]);
            translations[this.model.get('newTranslation')] = false;
            this.render();
        },
        deleteTranslation: function(){
            this.trigger('deleteTranslation');
        },
        set: function(data){
            this.model = data.model;
        },
        render: function (){
            var compiledTemplate = _.template(FormTemplate, {
                model:      this.model,
                languages:  App.getLanguagesCollection()
            });
            $(this.el).html(compiledTemplate);
            return this;
        },
    });
    return TranslationsControlView;
});