define([
  'jquery',
  'underscore',
  'backbone',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!templates/spaces/edit.html',
  'views/translationsControl',
  'globals'
], function($, _, Backbone, FormTemplate, TranslationsView, App){
    var FormView = Backbone.View.extend({
        el: $('#forms-container'),
        events: {
            'submit #spaceFormData': 'saveData',
            'click #spaceFormData * #delete': 'deleteSpace',
        },
        initialize: function(data){
            this.set(data);
        },
        saveData: function(e){
            e.preventDefault();
            var data = $("#spaceFormData").serializeArray(),
                self = this,
                newData = {};
            for(i=0; i<data.length; i++) {
                newData[data[i].name] = data[i].value;
            }
            if(this.model.get('newTranslation')) {
                newData['newTranslation'] = this.model.get('newTranslation');
            }
            this.model.save(newData, {
                headers: App.getHeaders(),
                success: function(){
                    alert("Space saved successfully!");
                    if(self.model.get('newLang')){
                        var translations = self.model.get('translations') || {};
                        translations[self.model.get('newLang').toString()] = true;
                        self.model.set('translations', translations);
                    }
                    Backbone.history.navigate("view/space/"+ self.model.get('id'), {trigger: true});
                    self.render();
                },
                silent: true
            });
        },
        deleteSpace: function(){
            var confirmation = confirm('Are you sure?');
            if(confirmation){
                this.model.destroy({
                    headers:App.getHeaders(),
                    success:function(){
                        alert('Space deleted successfully!');
                        Backbone.history.navigate("", {trigger: true});
                    }
                });
            }
        },
        getTranslation: function(code){
            var self = this,
                headers = _.clone(App.getHeaders());
            headers['Accept-language'] = code;

            this.model.fetch({
                headers: headers,
                success: function(){
                    self.render();
                },
                silent: true
            });
        },
        deleteTranslation: function(){
            var confirmation = confirm('Are you sure?');
            if(confirmation){
                var self = this;
                this.model.deleteTranslation({
                    headers:App.getHeaders(),
                    success:function(){
                        alert('Space translation deleted successfully!');
                        var translations = self.model.get('translations');
                        translations[self.model.get('language')] = false;
                        self.render();
                    },
                });
            }
        },
        set: function(data){
            this.model = data.model;
        },
        render: function (){
            var compiledTemplate = _.template(FormTemplate, {
                space:     this.model,
                languages: App.getLanguagesCollection()
                }
            );
            $(this.el).html(compiledTemplate);
            this._translationsListener();
        },
        _translationsListener: function(){
            var self = this;
            this.translations = new TranslationsView({model: this.model});
            $('#translations').html(this.translations.render().el);
            this.listenTo(this.translations, 'changeTranslation', function(data){
                self.getTranslation(data.code);
            });
            this.listenTo(this.translations, 'deleteTranslation', function(){
                self.deleteTranslation();
            });
        }
    });
    return FormView;
});