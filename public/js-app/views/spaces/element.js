// Filename: views/project/list
define([
  'jquery',
  'underscore',
  'backbone',
  'bootstrap',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!templates/spaces/element.html',
  'text!templates/languages/translations-list.html',
  'globals'
], function($, _, Backbone, Bootstrap, SpaceElementTemplate, PopoverTemplate, App){
    var SpaceElementView = Backbone.View.extend({
        tagName: "li",
        events: {
            'click': 'spaceClick',
            'mouseenter *[data-action="showPopover"]': 'showPopover',
            'mouseleave *[data-action="showPopover"]': 'hidePopover',
        },
        initialize: function(data) {
            this.model = data.model;
        },
        spaceClick: function(){
            Backbone.history.navigate("view/space/" + this.model.get('id'), {trigger: true});
        },
        showPopover: function(e){
            var compiledTemplate = _.template(PopoverTemplate, {
                languages: this.model.get('translations'),
                languagesCollection: App.getLanguagesCollection()
            });
            $(e.currentTarget).popover({
                html: true,
                title: 'Translations',
                content: compiledTemplate,
                container: 'body',
                placement: 'right',
                trigger: 'manual'
            });
            $(e.currentTarget).popover('show');
        },
        hidePopover: function(e){
            $(e.currentTarget).popover('hide');
        },
        render: function() {
            $(this.el).html(_.template(SpaceElementTemplate, {space: this.model}));
            return this;
        }
    });
    return SpaceElementView;
});