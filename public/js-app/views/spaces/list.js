// Filename: views/project/list
define([
  'jquery',
  'underscore',
  'backbone',
//  'bootstrap',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!templates/spaces/list.html',
  'views/spaces/element'
], function($, _, Backbone,  SpacesListTemplate, SpaceElementView){
    var SpacesListView = Backbone.View.extend({
        el: $('#space-selector'),
        events: {
            'click #createSpace': 'spaceCreate'
        },
        initialize: function(data) {
            this.collection = data.collection;
            this.render();
        },
        spaceCreate: function(){
            Backbone.history.navigate("create/space/", {trigger: true});
        },
        render: function() {
            var compiledTemplate = _.template(SpacesListTemplate);
            $(this.el).html(compiledTemplate);
            this.collection.each(function(space) {
                var spaceElementView = new SpaceElementView({model: space});
                $(this.el).append(spaceElementView.render().el);
            }, this);

            return this;
        }
    });
    return SpacesListView;
});