// Filename: views/project/list
define([
  'jquery',
  'underscore',
  'backbone',
  // Using the Require.js text! plugin, we are loaded raw text
  // which will be used as our views primary template
  'text!templates/categories/list.html',
  'views/categories/element'
], function($, _, Backbone, CategoriesListTemplate, CategoryElementView){
    var CategoriesListView = Backbone.View.extend({
        el: $('#sidebar'),
        initialize: function(data) {
            this.collection = data.collection;
            this.render();
        },
        events: {
            'click #createCategory': 'createCategory',
        },
        createCategory: function(){
            Backbone.history.navigate("create/category/", {trigger: true});
        },
        render: function() {
            var compiledTemplate = _.template(CategoriesListTemplate);
            $(this.el).html(compiledTemplate);
            this.collection.each(function(category) {
                var categoryElementView = new CategoryElementView({model: category});
                $('#'+ $(this.el).attr('id') +'>.list-group').append(categoryElementView.render().el);
            }, this);
            return this;
        }
    });
    return CategoriesListView;
});