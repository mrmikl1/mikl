define([
  'jquery',
  'backbone',
  'bootstrap',
  'jqueryCookie',
], function($, Backbone, Bootstrap, Cookie){
    var IntegrateView = Backbone.View.extend({
        el: $('body'),
        events: {
            'click #integrate': 'showInformer',
            'change select[name="language"]': 'changeLanguage',
            'click #page-guide': 'pageGuide',
        },
        showInformer: function(){
            var reg = /\d+/,
            spaceId = reg.exec(window.location.pathname),
            link = window.integrationLink;
            if (spaceId) {
                link = link + spaceId;
            }
            var script = "<script src='" + window.baseUrl + 'js/integrate.js' + "'></script>\n" +
                     "<script>\nGROFAQ.init({\n    url: '" + link + "',\n    assetsUrl: '" + window.baseUrl + "',\n    " +
                     "integrationType: 'tab',\n    tabTitle: 'FAQ',\n    tabPosition: 'left',\n    tabBgColor: '#eeeeee',\n    " +
                     "tabTextColor: '#ffffff'\n});\n</script>";
            var s = "<p>Insert this script into your head.</p>" +
                '<textarea id="textarea" type="text" style="width: 350px; height: 266px;">' + script + '</textarea>';
            $("#integrate").popover({trigger: 'manual', placement:'bottom', content:s, title:'Integration', html:true});
            $("#integrate").popover('toggle');
        },
        changeLanguage: function(e){
            var partsUrl = document.URL.replace(window.baseUrl,"").split('/');
            partsUrl.shift();
            location.replace(window.baseUrl + $(e.currentTarget).val() + '/' + partsUrl.join('/'));
        },
    });
    return IntegrateView;
});