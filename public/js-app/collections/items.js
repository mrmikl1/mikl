define([
  'underscore',
  'models/item',
  'collections/abstract'
], function(_, ItemModel, AbstractCollection) {
    var ItemCollection = AbstractCollection.extend({
        initialize: function(){
            AbstractCollection.prototype.initialize.call(this, {model: ItemModel, response: 'items'});
        },
        url: function() {
            var url;
            if(_.isUndefined(this.categoryId) && _.isUndefined(this.spaceId)) {
                url = '/api/v1/item';
            } else if(!_.isUndefined(this.categoryId)) {
                url = '/api/v1/category/' + this.categoryId + '/items';
            } else {
                url = '/api/v1/space/' + this.spaceId + '/items';
            }

            return url;
        },
    });
    return ItemCollection;
});