define([
    'models/space',
    'collections/abstract'
], function(SpaceModel, AbstractCollection){
    var SpacesCollection = AbstractCollection.extend({
        initialize: function(){
            AbstractCollection.prototype.initialize.call(this, {model: SpaceModel, response: 'spaces'});
        },
        url: '/api/v1/space',
    });
    return SpacesCollection;
});

