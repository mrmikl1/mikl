define([
  'underscore',
  'models/category',
  'collections/abstract'
], function(_, CategoryModel, AbstractCollection){
    var CategoriesCollection = AbstractCollection.extend({
        initialize: function(){
            AbstractCollection.prototype.initialize.call(this, {model: CategoryModel, response: 'categories'});
        },
        url: function() {
            if(_.isUndefined(this.spaceId)) {
                alert("You have to spacify space id using method setSpaceId(id) if you want to fetch a categories collection in space.");
            } else {
                return '/api/v1/space/' + this.spaceId + "/categories";
            }
        },
    });
    return CategoriesCollection;
});