define([
  'underscore',
  'backbone',
], function(_, Backbone){
    var AbstractCollection = Backbone.Collection.extend({
        initialize: function(params){// model
            var self = this;
            _.each(params, function(val, key){self[key] = val;});
            this.bind("error", AbstractCollection.errorHandler);
        },
        setSelected: function(id){
            var selectedModel = this.findWhere({selected: true});
            if(selectedModel && id != selectedModel.get('id')) {
                selectedModel.set({selected: false});
                this.get(id).set({selected: true});
            } else {
                this.get(id).set({selected: true});
            }
        },
        setCategoryId: function(id) {
            this.categoryId = id;
        },
        setSpaceId: function(id) {
            this.spaceId = id;
        },
        parse: function(response){
            return response[this.response];
        },
    },
    {
        errorHandler: function(model, response) {
            var error = response.status + "\n";
            switch(response.status){
                case 400:
                    var message = JSON.parse(response.responseText);
                    _.each(message, function(mes, field){
                        error += field + ": ";
                        _.each(mes, function(value, key){
                            error += value + "\n";
                        });
                    });
                    break;
                case 401:
                case 403:
                case 404:
                case 415:
                    var message = JSON.parse(response.responseText);
                    error += message.error;
                    break;
                default:
                    error += "Unprocessed error! Since, it's problem with server";
            }
            alert(error);
        }
    });
    return AbstractCollection;
});