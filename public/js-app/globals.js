define([
    'jquery', 'jqueryCookie'
],function($, Cookie) {
    var spacesCollection = {},
        categoriesCollection = {},
        itemsCollection = {},
        languagesCollection = {};
    var views = {};
    var current = {};
    var headers = {};
    var routes = {};
    return {
        getCookie: function(name) {
            return $.cookie(name);
        },
        addHeader: function(name, value){
            headers[name] = value;
        },
        addHeaders: function(data){
            for(var name in data) {
                headers[name] = data[name];
            }
        },
        setRoutes: function(data){
            routes = data;
        },
        setRoute: function(key, value){
            routes[key] = value;
        },
        setHeaders: function(data){
            headers = data;
        },
        setSpacesCollection: function(collection){
            spacesCollection = collection;
        },
        setCategoriesCollection: function(collection){
            categoriesCollection = collection;
        },
        setItemsCollection: function(collection){
            itemsCollection = collection;
        },
        setLanguagesCollection: function(collection){
            languagesCollection = collection;
        },
        setView: function(type, view){
            views[type.toLowerCase()] = view;
        },
        setCurrent: function(type,id){
            current[type.toLowerCase()] = id;
        },
        getHeaders: function(){
            return headers;
        },
        getSpacesCollection: function(){
            return spacesCollection;
        },
        getCategoriesCollection: function(){
            return categoriesCollection;
        },
        getItemsCollection: function(){
            return itemsCollection;
        },
        getLanguagesCollection: function(){
            return languagesCollection;
        },
        getLanByCode: function(code){
            return languagesCollection.findWhere({code: code});
        },
        getView: function(type){
            return views[type.toLowerCase()];
        },
        getCurrent: function(type){
            return current[type.toLowerCase()];
        },
        getRoutes: function(){
            return routes;
        },
        getRoute: function(key){
            return routes[key];
        },
        unsetHeader: function(name){
            delete(headers[name]);
        },
        unsetHeaders: function(){
            delete(headers);
        },
        unsetSpacesCollection: function(){
            delete(spacesCollection);
        },
        unsetCategoriesCollection: function(){
            delete(categoriesCollection);
        },
        unsetItemsCollection: function(){
            delete(itemsCollection);
        },
        unsetView: function(type){
            delete(views[type.toLowerCase()]);
        },
        unsetCurrent: function(type){
            delete(current[type.toLowerCase()]);
        },
        getAllCurrent: function(){
            return current;
        },
        unsetCurrentAll: function(){
            delete(current);
        },
    };
});