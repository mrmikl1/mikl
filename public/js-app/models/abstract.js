define([
  'backbone'
], function(Backbone){
    var AbstractModel = Backbone.Model.extend({
        initialize: function(params){
            var self = this;
            _.each(params, function(val, key){self[key] = val;});
            this.bind("error", AbstractModel.errorHandler);
        },
        defaults: {
            "selected": false
        },
        save: function(attrs, options) {
            options || (options = {});
            // Filter the data to send to the server
            attrs = attrs || attrs.unset('translations', {silent: true});

            options.data = JSON.stringify(attrs);

            // Proxy the call to the original save function
            Backbone.Model.prototype.save.call(this, attrs, options);
        },
        deleteTranslation: function(options){
            options.url = this.urlRoot + "/" + this.get('id') + "/translation/" + this.get('language');
            Backbone.Model.prototype.destroy.call(this, options);
        }
    },
    {
        errorHandler: function(model, response) {
            var error = response.status + "\n";
            switch(response.status){
                case 400:
                    var message = JSON.parse(response.responseText);
                    _.each(message, function(mes, field){
                        error += field + ": ";
                        _.each(mes, function(value, key){
                            error += value + "\n";
                        });
                    });
                    break;
                case 401:
                case 403:
                case 404:
                case 415:
                    var message = JSON.parse(response.responseText);
                    error += message.error;
                    break;
                default:
                    error += "Unprocessed error! Since, it's problem with server";
            }
            alert(error);
        }
    });
    return AbstractModel;
});