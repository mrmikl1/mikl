define([
  'models/abstract'
], function(AbstractModel){
    var CategoryModel = AbstractModel.extend({
        initialize: function(){
            AbstractModel.prototype.initialize.call(this, {urlRoot: '/api/v1/category'});
        }
    });
    return CategoryModel;
});