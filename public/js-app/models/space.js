define([
  'models/abstract'
], function(AbstractModel){
    var SpaceModel = AbstractModel.extend({
        initialize: function(){
            AbstractModel.prototype.initialize.call(this, {urlRoot: '/api/v1/space'});
        }
    });
    return SpaceModel;
});