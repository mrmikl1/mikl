define([
  'models/abstract'
], function(AbstractModel){
    var ItemModel = AbstractModel.extend({
        initialize: function(){
            AbstractModel.prototype.initialize.call(this, {urlRoot: '/api/v1/item'});
        }
    });
    return ItemModel;
});