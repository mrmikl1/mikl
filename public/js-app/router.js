define([
    'underscore', 'backbone', //general
    'collections/spaces', 'collections/categories', 'collections/items', //collections
    'views/spaces/list', 'views/categories/list', 'views/items/list', //list views
    'views/spaces/edit', 'views/categories/edit', 'views/items/edit', //edit views
    'models/space', 'models/category', 'models/item', //models
    'globals'
], function(
    _, Backbone,
    SpacesCollection, CategoriesCollection, ItemsCollection,
    SpacesListView, CategoriesListView, ItemsListView,
    SpaceEditView, CategoryEditView, ItemEditView,
    SpaceModel, CategoryModel, ItemModel,
    App)
{
    var AppRouter = Backbone.Router.extend({
        initialize: function(){
            var routes = {
                viewDefault: 'viewDefault',
                viewSpace: 'viewSpace',
                viewCategory: 'viewCategory',
                viewItem: 'viewItem',
                createSpace: 'createSpace',
                createCategory: 'createCategory',
                createItem: 'createItem'
            }, self = this;

            _.each(routes, function(func, route){
                self.route(App.getRoute(route), func);
            });
        },
        viewDefault: function(){
            var self = this;
            this._loadSpaces({func: function(){
                var spaceId = self.spacesCollection.at(0).get('id');
                self.spacesCollection.setSelected(spaceId);
                self._loadCategories({func: function(){
                    self._loadItems({sid: spaceId});
                }, sid: spaceId});
            }});
        },
        viewSpace: function(spaceId) {
            var self = this;
            if(_.isUndefined(this.spacesCollection)) {
                this._loadSpaces({edit: spaceId, selected: spaceId});
            } else {
                this.spacesCollection.setSelected(spaceId);
                this._showSpaceEditView(spaceId);
            }
            this._loadCategories({func: function(){
                self._loadItems({sid: spaceId});
            }, sid: spaceId});
        },
        viewCategory: function(spaceId, categoryId) {
            if(_.isUndefined(this.spacesCollection)) {
                this._loadSpaces({selected: spaceId});
            }
            if(_.isUndefined(this.categoriesCollection)) {
                this._loadCategories({edit: categoryId, sid: spaceId, selected: categoryId});
            } else {
                this.categoriesCollection.setSelected(categoryId);
                this._showCategoryEditView(categoryId);
            }
            this._loadItems({cid: categoryId});
        },
        viewItem: function(spaceId, categoryId, itemId) {
            if(_.isUndefined(this.spacesCollection)) {
                this._loadSpaces({selected: spaceId});
            }
            if(_.isUndefined(this.categoriesCollection)) {
                this._loadCategories({sid: spaceId, selected: categoryId});
                this._loadItems({edit: itemId, cid: categoryId, selected: itemId});
            } else {
                this.categoriesCollection.setSelected(categoryId);
                if(_.isUndefined(this.itemsCollection)) {
                    this._loadItems({edit: itemId, cid: categoryId, selected: itemId});
                } else {
                    this.itemsCollection.setSelected(itemId);
                    this._showItemEditView(itemId);
                }
            }
        },
        createSpace: function() {
            if(_.isUndefined(this.spacesCollection)){
                this.viewDefault();
                return;
            }
            var lang = App.getCookie('locale');
            if(_.isUndefined(this.spaceEditView)) {
                this.spaceEditView = new SpaceEditView({model: (new SpaceModel).set({'language': lang})});
            } else {
                this.spaceEditView.set({model: (new SpaceModel).set({'language': lang})});
            }
            this.spaceEditView.render();
        },
        createCategory: function(){
            if(_.isUndefined(this.spacesCollection)){
                this.viewDefault();
                return;
            }
            var lang = App.getCookie('locale');
            if(_.isUndefined(this.categoryEditView)) {
                this.categoryEditView = new CategoryEditView({model: (new CategoryModel).set({'language': lang}), spaces: this.spacesCollection});
            } else {
                this.categoryEditView.set({model: (new CategoryModel).set({'language': lang}), spaces: this.spacesCollection});
            }
            this.categoryEditView.render();
        },
        createItem: function(){
            if(_.isUndefined(this.spacesCollection)){
                this.viewDefault();
                return;
            }
            var lang = App.getCookie('locale');
            if(_.isUndefined(this.itemEditView)) {
                this.itemEditView = new ItemEditView({model: (new ItemModel).set({'language': lang}), categories: this.categoriesCollection, spaces: this.spacesCollection});
            } else {
                this.itemEditView.set({model: (new ItemModel).set({'language': lang}), categories: this.categoriesCollection, spaces: this.spacesCollection});
            }
            this.itemEditView.render();
        },
        _loadSpaces: function(params){
            var self = this;
            this.spacesCollection = new SpacesCollection();
            self.spacesCollection.fetch({
                headers: App.getHeaders()
            }).done(function() {
                if(params && params.func){
                    params.func();
                }
                if(params && params.selected){
                    self.spacesCollection.setSelected(params.selected);
                }
                if(params && params.edit){
                    self._showSpaceEditView(params.edit);
                }
                new SpacesListView({collection: self.spacesCollection});
            });
        },
        _loadCategories: function(params){
            var self = this;
            this.categoriesCollection = new CategoriesCollection();
            if(params && params.sid) {
                self.categoriesCollection.setSpaceId(params.sid);
            }
            self.categoriesCollection.fetch({
                headers:App.getHeaders()
            }).done(function() {
                if(params && params.selected) {
                    self.categoriesCollection.setSelected(params.selected);
                }
                if(params && params.func){
                    params.func();
                }
                if(params && params.edit){
                    self._showCategoryEditView(params.edit);
                }
                new CategoriesListView({collection: self.categoriesCollection});
            });
        },
        _loadItems: function(params){
            var self = this;
            this.itemsCollection = new ItemsCollection();
            if(params && params.sid) {
                self.itemsCollection.setSpaceId(params.sid);
            }
            if(params && params.cid) {
                self.itemsCollection.setCategoryId(params.cid);
            }
            self.itemsCollection.fetch({
                headers:App.getHeaders()
            }).done(function() {
                if(params && params.selected) {
                    self.itemsCollection.setSelected(params.selected);
                }
                if(params && params.func){
                    params.func();
                }
                if(params && params.edit){
                    self._showItemEditView(params.edit);
                }
                new ItemsListView({collection: self.itemsCollection});
            });
        },
        _showSpaceEditView: function(spaceId){
            if(_.isUndefined(this.spaceEditView)) {
                this.spaceEditView = new SpaceEditView({model: this.spacesCollection.get(spaceId)});
            } else {
                this.spaceEditView.set({model: this.spacesCollection.get(spaceId)});
            }
            this.spaceEditView.render();
        },
        _showCategoryEditView: function(categoryId){
            if(_.isUndefined(this.categoryEditView)) {
                this.categoryEditView = new CategoryEditView({model: this.categoriesCollection.get(categoryId), spaces: this.spacesCollection});
            } else {
                this.categoryEditView.set({model: this.categoriesCollection.get(categoryId), spaces: this.spacesCollection});
            }
            this.categoryEditView.render();
        },
        _showItemEditView: function(itemId){
            if(_.isUndefined(this.itemEditView)) {
                this.itemEditView = new ItemEditView(
                    {model: this.itemsCollection.get(itemId), categories: this.categoriesCollection, spaces: this.spacesCollection});
            } else {
                this.itemEditView.set(
                    {model: this.itemsCollection.get(itemId), categories: this.categoriesCollection, spaces: this.spacesCollection});
            }
            this.itemEditView.render();
        },
    });

    var initialize = function() {
        var router = new AppRouter;
        Backbone.history.start();
    };
    return {
        initialize: initialize
    };
});