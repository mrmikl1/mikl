require.config({
    shim: {
        underscore: {
          exports: '_'
        },
        backbone: {
          deps: ['underscore', 'jquery'],
          exports: 'Backbone'
        }
        },
    paths: {
      jquery: 'libs/jquery/jquery',
      jqueryCookie: 'libs/jquery/jquery.cookie',
      bootstrap: 'libs/bootstrap/bootstrap',
      underscore: 'libs/underscore/underscore',
      backbone: 'libs/backbone/backbone'
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require([
    'underscore',
    'backbone',
    'views/general',
    'globals',
], function(_, Backbone, General, App) {
    var collection = [];
    _.each(window.languagesCollection, function(val){
        collection.push(val);
    });
    App.setHeaders({"Content-type":"application/json;charset=UTF-8",
                    "Accept":"application/json",
                    "Accept-language":App.getCookie('locale')});
    App.setLanguagesCollection(new Backbone.Collection(collection));
    App.setRoutes({
        viewDefault:    '',
        viewSpace:      'view/space/:spaceId',
        viewCategory:   'view/category/:spaceId-:categoryId',
        viewItem:       'view/item/:spaceId-:categoryId-:itemId',
        createSpace:    'create/space/',
        createCategory: 'create/category/',
        createItem:     'create/item/',
    });
    new General();
});

require([
    'app'
], function(App){
    App.initialize();
});