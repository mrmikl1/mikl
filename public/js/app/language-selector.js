$(document).ready(function(){
    $("select[name='language']").change(function(){
        var partsUrl = document.URL.replace(window.baseUrl,"").split('/');
        partsUrl.shift();
        location.replace(window.baseUrl + $(this).val() + '/' + partsUrl.join('/'));
    });
});