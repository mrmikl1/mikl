<?php

class ZendX_Controller_Plugin_404 extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch($request)
    {
        $this->_translate = Zend_Registry::get('Zend_Translate');
        try {
            $accountName = $request->getParam('accountName');
            $moduleName = $request->getParam('module');
            $controllerName = $request->getParam('controller');
            $actionName = $request->getParam('action');
            $id = $request->getParam('id');

            switch ($moduleName) {
                case 'default':
                    $this->_findAccountByName($accountName);
                    if ($id) {
                        $spaceService = new Application_Service_Spaces();
                        $spaceService = new Application_Service_Decorated_Exceptions_404_Spaces($spaceService);
                        $spaceService->get($accountName, $id);
                    }
                    break;
                case 'admin':
                    $this->_findAccountByName($accountName);
                    break;
                case 'account':
                    switch ($actionName) {
                        case 'activate':
                            $this->_findAccountByName($accountName);
                            break;
                    }
                    break;
                case 'space':
                    switch ($actionName) {
                        case 'create':
                            break;
                        case 'view':
                            if (!$id) {
                                $this->_findAccountByName($accountName);
                            } else {
                                $spaceService = new Application_Service_Spaces();
                                $spaceService = new Application_Service_Decorated_Exceptions_404_Spaces($spaceService);
                                $spaceService->get($accountName, $id);
                            }
                            break;
                        default:
                            $spaceService = new Application_Service_Spaces();
                            $spaceService = new Application_Service_Decorated_Exceptions_404_Spaces($spaceService);
                            $spaceService->get($accountName, $id);
                            break;
                    }
                    break;
                case 'category':
                    switch ($actionName) {
                        case 'create':
                            break;
                        default:
                            $categoryService = new Application_Service_Categories();
                            $categoryService = new Application_Service_Decorated_Exceptions_404_Categories($categoryService);
                            $categoryService->get($accountName, $id);
                            break;
                    }
                    break;
                case 'item':
                    switch ($actionName) {
                        case 'create':
                            break;
                        default:
                            $itemService = new Application_Service_Items();
                            $itemService = new Application_Service_Decorated_Exceptions_404_Items($itemService);
                            $itemService->get($accountName, $id);
                            break;
                    }
                    break;
                case 'user':
                    $account = $this->_findAccountByName($accountName);
                    $userMapper = new User_Model_Mapper();
                    if (!$userMapper->findByAccountIdAndUserId($account, $id)) {
                        throw new Application_Exception_404();
                    }
                    break;
                case 'auth':
                    switch ($actionName) {
                        case 'resetPassword':
                            $hash = $request->getParam('hash');
                            $account = $this->_findAccountByName($accountName);
                            $userMapper = new User_Model_Mapper();
                            if (!$userMapper->findByAccountAndPasswordHash($account, $hash)){
                                throw new Application_Exception_404();
                            }
                            break;
                        default:
                            $this->_findAccountByName($accountName);
                    }
                    break;
                case 'v1':
                    switch ($controllerName) {
                        case 'space':
                            switch ($actionName) {
                                case 'post':
                                case 'index':
                                case 'options':
                                    break;
                                default:
                                    $spaceService = new Application_Service_Spaces();
                                    $spaceService = new Application_Service_Decorated_Exceptions_404_Spaces($spaceService);
                                    $spaceService->get($accountName, $id);
                                    break;
                            }
                            break;
                        case 'category':
                            switch ($actionName) {
                                case 'post':
                                case 'index':
                                case 'options':
                                    break;
                                default:
                                    $categoryService = new Application_Service_Categories();
                                    $categoryService = new Application_Service_Decorated_Exceptions_404_Categories($categoryService);
                                    $categoryService->get($accountName, $id);
                                    break;
                            }
                            break;
                        case 'item':
                            switch ($actionName) {
                                case 'post':
                                case 'index':
                                case 'options':
                                    break;
                                default:
                                    $itemService = new Application_Service_Items();
                                    $itemService = new Application_Service_Decorated_Exceptions_404_Items($itemService);
                                    $itemService->get($accountName, $id);
                                    break;
                            }
                            break;
                        case 'account':
                            switch ($actionName) {
                                case 'post':
                                case 'index':
                                case 'options':
                                    break;
                                default:
                                    $this->_findAccountByName($id);
                                    break;
                            }
                            break;
                    }
                    break;
                default:
                    $this->_findAccountByName($accountName);
                    break;
            }
        } catch(Exception $e) {
            if ($e instanceof Application_Exception_404) {
                if ($this->getRequest()->getHeader('content-type') == 'application/json;charset=UTF-8') {
                    $this->getResponse()->setHttpResponseCode(404);
                    $this->getRequest()->setModuleName('v1')
                            ->setControllerName('error')
                            ->setActionName('error404');
                } else {
                    $this->getResponse()->setHttpResponseCode(404);
                    $request->setModuleName('error')
                            ->setControllerName('index')
                            ->setActionName('error404')
                            ->setParam('error_handler', $e);
                    if ($request->getParam('module') == 'default') {
                        $request->setParam('currentModule', 'default');
                    }
                }
            }
        }
    }
    protected function _findAccountByName($accountName)
    {
        $accountService = new Application_Service_Accounts();
        $accountService = new Application_Service_Decorated_Exceptions_404_Accounts($accountService);
        return $accountService->get($accountName);
    }
}
