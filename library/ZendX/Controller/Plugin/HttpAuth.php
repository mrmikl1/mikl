<?php

class ZendX_Controller_Plugin_HttpAuth extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch($request)
    {
        $accountName = $request->getParam('accountName');
        if (!empty($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_PW'])) {
            $dataLogin = array('email' => $_SERVER['PHP_AUTH_USER'], 'password' => $_SERVER['PHP_AUTH_PW']);
        } else {
            return FALSE;
        }

        $authService = new Application_Service_Auth();
        $authService->login($dataLogin, $accountName);
    }
}
