<?php

class ZendX_Controller_Plugin_401 extends Zend_Controller_Plugin_Abstract
{
    protected $_flashMessenger = null;
    public function preDispatch($request)
    {
        if ($this->getResponse()->getHttpResponseCode() == 404) {
            return;
        }
        try {
            $accountName = $request->getParam('accountName');
            $moduleName = $request->getParam('module');
            $actionName = $request->getParam('action');
            $controllerName = $request->getParam('controller');

            $user = Zend_Auth::getInstance()->getStorage()->read();
            $permissionsService = new Application_Service_Permissions();
            switch ($moduleName) {
                case 'account':
                    if ($actionName == 'account-preferences') {
                        $permissionsService->hasAccess($accountName, $user);
                    }
                case 'default':
                    break;
                case 'v1':
                    switch($controllerName):
                        case 'account':
                            break;
                        case 'item':
                            switch ($actionName):
                                case 'delete':
                                    $permissionsService->hasAccess($accountName, $user);
                                    break;
                                default:
                                    break;
                            endswitch;
                            break;
                        default:
                            switch ($actionName):
                       		case 'head':
                                case 'get':
                                case 'index':
                                case 'categories':
                                case 'items':
                                    break;
                                default:
                                    $permissionsService->hasAccess($accountName, $user);
                            endswitch;
                    endswitch;
                    break;
                case 'auth':
                    break;
                default:
                    if ($actionName == 'integrate')
                        break;
                    $permissionsService->hasAccess($accountName, $user);
                    break;
            }
        } catch(Exception $e) {
            if ($e instanceof Application_Exception_401) {
                $this->getResponse()->setHttpResponseCode(401);
                if ($this->getRequest()->getHeader('content-type') == 'application/json;charset=UTF-8') {
                    $request->setModuleName('v1')
                            ->setControllerName('error')
                            ->setActionName('error401');
                } else {
                    $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
                    if (!($this->getRequest()->getModuleName() == 'admin')) {
                        $flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
                        $flashMessenger->addMessage($e->getMessage(), 'error');
                        $view->flashMessenger = $flashMessenger;
                    }
                    $redirect = $this->getRequest()->getParams();
                    $urlRedirect = Zend_View_Helper_Url::url($redirect);
                    setcookie('redirect', $urlRedirect, time()+3600, '/');

                    $url = Zend_View_Helper_Url::url($this->getRequest()->getParams(), 'login');
                    $this->getResponse()->setRedirect($url);
                }
            }
        }
    }
}
