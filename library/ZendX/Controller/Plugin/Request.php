<?php

class ZendX_Controller_Plugin_Request extends Zend_Controller_Plugin_Abstract
{
    protected $_view;

    public function preDispatch(\Zend_Controller_Request_Abstract $request)
    {
    $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
    $this->_view = $viewRenderer->view;

    $getParams = $request->getParams();
    foreach($getParams as $key => $value) {
        if (in_array($key, array('description', 'content', 'module', 'controller', 'action', 'error_handler'))) {
            continue;
        }
        $value = $this->_escape($value);
        $request->setParam($key, $value);
    }

    $postParams = $request->getPost();

    $safePostParams = array();
    foreach($postParams as $key => $value) {
        if (in_array($key, array('description', 'content'))) {
            continue;
        }
        $value = $this->_escape($value);
        $safePostParams[$key] = $value;
    }

    $request->setPost($safePostParams);

    return parent::preDispatch($request);
    }

    protected function _escape($data)
    {
        if (is_array($data)) {
            $res = array();
            foreach ($data as $key => $value) {
                $res[$key] = $this->_escape($value);
            }
            return $res;
        } else {
            return $this->_view->escape($data);
        }
    }
}