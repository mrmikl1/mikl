<?php
/**
 * i18n
 * This plugin set language from URL to Zend_Translate_Adapter
 *
 * @author Max Natalchishin
 * @version 1.0
 */

class ZendX_Controller_Plugin_i18n extends Zend_Controller_Plugin_Abstract
{
    /**
     *
     * @var Application_Collection_Generic
     */
    protected $_languagesCollection;

    /**
     *
     * @var array
     */
    protected $_languageCodes;

    /**
     *
     * @param Zend_Controller_Request_Abstract $request descr
     * @param string $locale descr
     * @param string $browserLang descr
     */

    public function preDispatch($request)
    {
        if ($this->getResponse()->getHttpResponseCode() == 404) {
            return;
        }
        $languagesService = new Application_Service_Languages();
        $this->_languagesCollection = $languagesService->getCollection($request->getParam('accountName'));

        foreach($this->_languagesCollection as $language) {
            $languageCodes[] = $language['code'];
        }

        $this->_languageCodes = $languageCodes;

        $this->_defaultLocale = 'en';
        /**
         * @todo delete settings for admin and front areas
         */
        $acceptLanguage = substr(Locale::acceptFromHttp($this->getRequest()->getHeader('accept-language')), 0, 2);
        $browserLang = $acceptLanguage;
        $locale = $request->getParam('locale');

        if (empty($locale)) {
            if(empty($_COOKIE['locale'])) {
                $locale = $acceptLanguage;
            } else {
                $locale = $_COOKIE['locale'];
            }

            $this->setLocale($locale, $request->getParam('module') == 'v1');
        }

        // update locale cookie on each page refresh
        setcookie('locale', $locale, time()+60*60*24*365, '/');

        $translate = new Zend_Translate(
            array(
                'adapter' => 'gettext',
                'content' => APPLICATION_PATH . '/languages/',
                'locale'  => $this->_defaultLocale,
                'scan'    => Zend_Translate::LOCALE_FILENAME
            )
        );

        $formTranslate = new Zend_Translate(
            array(
                'adapter' => 'array',
                'content' => APPLICATION_PATH . '/languages/forms/',
                'locale'  => $this->_defaultLocale,
                'scan'    => Zend_Translate::LOCALE_DIRECTORY
            )
        );

        if (in_array($locale, $translate->getList())) {
            $translate->setLocale($locale);
        }

        if (in_array($locale, $formTranslate->getList())) {
            $formTranslate->setLocale($locale);
        }

        Zend_Validate_Abstract::setDefaultTranslator($formTranslate);

        Zend_Registry::set('Zend_Translate', $translate);
        Zend_Registry::set('locale', $locale);

        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->translate = $translate;
        $view->locale = $locale;
        $view->browserLang = $browserLang;
        $view->languagesCollection = $this->_languagesCollection;
    }

    private function setLocale($locale, $api = false)
    {
        if (!in_array(strtolower($locale), $this->_languageCodes)) {
            $locale = $this->_defaultLocale;
        }

        if($api === true) {
            $this->getResponse()->setHeader('accept-language', $locale);
        } else {
            $this->getRequest()->setParam('locale', $locale);

            $currentRouteName = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
            var_dump($currentRouteName);
            $url = Zend_View_Helper_Url::url($this->getRequest()->getParams(), str_replace("NoLocale", '', $currentRouteName));
            $this->getResponse()->setRedirect($url);
        }
    }
}
