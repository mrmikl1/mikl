<?php

class ZendX_Controller_Plugin_Cache extends Zend_Controller_Plugin_Abstract
{
    protected $_translate;

    public function preDispatch($request)
    {
        if (APPLICATION_ENV != 'testing' && APPLICATION_ENV != 'development') {
            $accountName = $request->getParam('accountName');
            $moduleName = $request->getParam('module');
            $actionName = $request->getParam('action');
            $method = $this->_request->getMethod();
            Zend_Controller_Front::getInstance()->setParam('disableOutputBuffering',true);
            $frontendOptions = array(
               'lifetime' => NULL,
            'content_type_memorization' => true,
                'memorize_headers'  => array(
                    'content-type',
                    'last-modified',
                    'content-length'
                ),
    //           'debug_header' => true, // for debugging
                'automatic_serialization' => true,
                'default_options' => array(
                    'cache_with_get_variables' => true,
                    'cache_with_post_variables' => true,
                    'cache_with_session_variables' => true,
                    'cache_with_files_variables' => true,
                    'cache_with_cookie_variables' => true,
                    'make_id_with_get_variables' => true,
                    'make_id_with_post_variables' => true,
                    'make_id_with_session_variables' => true,
                    'make_id_with_files_variables' => true,
                    'make_id_with_cookie_variables' => false
                ),
            );

            if (!file_exists(APPLICATION_PATH. '/../tmp/')) {
                mkdir(APPLICATION_PATH. '/../tmp/');
            }

            if (!file_exists(APPLICATION_PATH. '/../tmp/cache/')) {
                mkdir(APPLICATION_PATH. '/../tmp/cache/');
            }

            if (!file_exists(APPLICATION_PATH. '/../tmp/cache/' . $accountName)) {
                mkdir(APPLICATION_PATH. '/../tmp/cache/' . $accountName);
            }
            $backendOptions = array(
                'cache_dir' =>  APPLICATION_PATH. '/../tmp/cache/' . $accountName
            );

            $cache = Zend_Cache::factory(
                                 'Page',
                                 'File',
                                 $frontendOptions,
                                 $backendOptions);

            Zend_Registry::set('cache', $cache);
            switch ($moduleName) {
                case 'default':
                    $cache->start();
                    break;
                default :
                    switch ($actionName) {
                        case 'get':
                        case 'index':
                        case 'items':
                        case 'categories':
                        case 'spaces':
                            if ($moduleName == 'v1') {
                                $cache->start();
                            }
                            break;
                        case 'create':
                        case 'edit':
                        case 'put':
                        case 'post':
                            if ($method == 'POST' || $method == 'PUT') {
                                if (Zend_Registry::isRegistered('cache')) {
                                    $cache = Zend_Registry::get('cache');
                                    $cache->clean();
                                }
                            }
                            break;
                        case 'delete':
                            if (Zend_Registry::isRegistered('cache')) {
                                $cache = Zend_Registry::get('cache');
                                $cache->clean();
                            }
                            break;
                        default :
                            break;
                    }
                    break;
            }
        }
    }
}
