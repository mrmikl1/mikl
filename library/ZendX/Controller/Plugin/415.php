<?php

class ZendX_Controller_Plugin_415 extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch($request)
    {
        $this->_translate = Zend_Registry::get('Zend_Translate');
        $moduleName = $request->getParam('module');
        $contentTypeHeader = str_replace(" ", "", $this->getRequest()->getHeader('content-type'));
        $acceptHeader = str_replace(" ", "", $this->getRequest()->getHeader('accept'));
        if($moduleName == 'v1'){
            if ($contentTypeHeader !== 'application/json;charset=UTF-8' || $acceptHeader !== 'application/json') {
                $this->getResponse()->setHttpResponseCode(415);
                $this->getResponse()->setBody(
                    json_encode(array(
                        'error' => $this->_translate->_('Incorrect header: header "content-type" must be "application/json;charset=UTF-8" and header "accept" must be "application/json"')
                    ))
                );
                $request->setModuleName('v1')
                        ->setControllerName('error')
                        ->setActionName('error415');
            }
        }
    }
}
