<?php

class ZendX_Controller_Plugin_Layout extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch($request)
    {
        $this->_locale = Zend_Registry::get('locale');
        if ($this->getRequest()->getHeader('content-type') != 'application/json;charset=UTF-8'){
            $accountName = $request->getParam('accountName');
            switch($request->getParam('module')){
                case 'v1':
                    break;
                case 'default':
                    if (!$accountName) {
                        $this->getResponse()->setHttpResponseCode(404);
                        $request->setModuleName('error')
                                ->setControllerName('index')
                                ->setActionName('error404')
//                                ->setParam('error_handler', $e)
                                ->setParam('currentModule', 'default');
                    } else {
                        $spaceService = new Application_Service_Spaces();$this->_locale = Zend_Registry::get('locale');
                        $spaceCollection = $spaceService->getCollection($accountName, $this->_locale);
                        if ($spaceCollection) {
                            $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
                            $spaceCollection->setSelected($request->getParam('id'));
                            $view->spaces = $spaceCollection;
                            $this->_response->insert('header', $view->render('general/front/header.phtml'));
                        }
                    }
                    break;
                default :
                    switch ($request->getParam('action')) {
                        case 'register':
                        case 'registered':
                        case 'login':
                        case 'activate':
                        case 'integrate':
                            break;
                        default:
                    	    if (!$accountName) {
                        	$this->getResponse()->setHttpResponseCode(404);
	                        $request->setModuleName('error')
	                                ->setControllerName('index')
	                                ->setActionName('error404')
//                                        ->setParam('error_handler', $e)
                                        ->setParam('currentModule', 'default');
	                    } else {
                                $spaceService = new Application_Service_Spaces();
                                $spaceCollection = $spaceService->getCollection($accountName);
                                $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
                                $view->spaces = $spaceCollection;
                                $this->_response->insert('header', $view->render('general/header.phtml'));
		            }
                            break;
                    }
            }

        }
    }
}

