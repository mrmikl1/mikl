<?php

class ZendX_Auth_Adapter_DbTable extends Zend_Auth_Adapter_DbTable
{

    const NO_TABLE              = 'A table must be supplied for the Zend_Auth_Adapter_DbTable authentication adapter.';
    const NO_IDENTITY_COLUMN    = 'An identity column must be supplied for the Zend_Auth_Adapter_DbTable authentication adapter.';
    const NO_CREDENTIAL_COLUMN  = 'A credential column must be supplied for the Zend_Auth_Adapter_DbTable authentication adapter.';
    const NO_ACCOUNT_ID_COLUMN  = 'An account id column must be supplied for the Zend_Auth_Adapter_DbTable authentication adapter.';
    const NO_EMAIL_COLUMN       = 'An email column must be supplied for the Zend_Auth_Adapter_DbTable authentication adapter.';
    const NO_ACCOUNT_ID         = 'A value for the account id was not provided prior to authentication with Zend_Auth_Adapter_DbTable.';
    const NO_EMAIL              = 'A value for the email was not provided prior to authentication with Zend_Auth_Adapter_DbTable.';
    const NO_CREDENTIAL         = 'A credential value was not provided prior to authentication with Zend_Auth_Adapter_DbTable.';
    const MSG_AUTH_SUCCESSFUL   = 'Authentication successful.';
    const MSG_AUTH_FAILED       = 'Supplied credential is invalid.';

    /**
     * $_tableName - the table name to check
     *
     * @var string
     */
    protected $_tableName = "users";

    /**
     * $_accountIdColumn - the column to use as the user's account id
     *
     * @var string
     */
    protected $_accountIdColumn = "account_id";

    /**
     * $_identityColumn - the column to use as the identity
     *
     * @var string
     */
    protected $_identityColumn = "user_id";

    /**
     * $_emailColumn - the column to use as the user's email
     *
     * @var string
     */
    protected $_emailColumn = "email";

    /**
     * $_credentialColumns - columns to be used as the credentials
     *
     * @var string
     */
    protected $_credentialColumn = "password";

    /**
     * $_credentialTreatment - Treatment applied to the credential, such as MD5() or PASSWORD()
     *
     * @var string
     */
    protected $_credentialTreatment = '? AND active = "Y"';

    /**
     * $_email - the column to use as the user's username
     *
     * @var string
     */
    protected $_email = null;

    /**
     * $_accountId - the column to use as the user's account id
     *
     * @var string
     */
    protected $_accountId = null;

    /**
     * __construct() - Sets configuration options
     *
     * @param  Zend_Db_Adapter_Abstract $zendDb If null, default database adapter assumed
     * @param  string                   $credentialTreatment
     * @return void
     */
    public function __construct()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        parent::__construct($dbAdapter, $this->_tableName, $this->_identityColumn, $this->_credentialColumn, $this->_credentialTreatment);
    }

    /**
     * setAccountIdColumn() - set the column name to be used as the account id column
     *
     * @param  string $accountIdColumn
     * @return ExtZend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setAccountIdColumn($accountIdColumn)
    {
        $this->_accountIdColumn = $accountIdColumn;
        return $this;
    }

    /**
     * setEmailColumn() - set the column name to be used as the email column
     *
     * @param  string $emailColumn
     * @return ExtZend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setEmailColumn($emailColumn)
    {
        $this->_emailColumn = $emailColumn;
        return $this;
    }

    /**
     * setEmail() - set the value to be used as the username
     *
     * @param  string $value
     * @return ExtZend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setEmail($value)
    {
        $this->_email = $value;
        return $this;
    }

    /**
     * setAccountId() - set the value to be used as the account id
     *
     * @param  string $value
     * @return ExtZend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setAccountId($value)
    {
        $this->_accountId = $value;
        return $this;
    }

    /**
     * _authenticateSetup() - This method abstracts the steps involved with
     * making sure that this adapter was indeed setup properly with all
     * required pieces of information.
     *
     * @throws Zend_Auth_Adapter_Exception - in the event that setup was not done properly
     * @return true
     */
    protected function _authenticateSetup()
    {
        $exception = null;

        if ($this->_tableName == '') {
            $exception = self::NO_TABLE;
        } elseif ($this->_identityColumn == '') {
            $exception = self::NO_IDENTITY_COLUMN;
        } elseif ($this->_credentialColumn == '') {
            $exception = self::NO_CREDENTIAL_COLUMN;
        } elseif ($this->_accountIdColumn == '') {
            $exception = self::NO_ACCOUNT_ID_COLUMN;
        } elseif ($this->_emailColumn == '') {
            $exception = self::NO_EMAIL_COLUMN;
        } elseif ($this->_accountId == '') {
            $exception = self::NO_ACCOUNT_ID;
        } elseif ($this->_email == '') {
            $exception = self::NO_EMAIL;
        } elseif ($this->_credential === null) {
            $exception = self::NO_CREDENTIAL;
        }

        if (null !== $exception) {
            /**
             * @see Zend_Auth_Adapter_Exception
             */
            require_once 'Zend/Auth/Adapter/Exception.php';
            throw new Zend_Auth_Adapter_Exception($exception);
        }

        $this->_authenticateResultInfo = array(
            'code'     => Zend_Auth_Result::FAILURE,
            'identity' => $this->_email,
            'messages' => array()
        );

        return true;
    }

    /**
     * _authenticateCreateSelect() - This method creates a Zend_Db_Select object that
     * is completely configured to be queried against the database.
     *
     * @return Zend_Db_Select
     */
    protected function _authenticateCreateSelect()
    {
        // build credential expression
        if (empty($this->_credentialTreatment) || (strpos($this->_credentialTreatment, '?') === false)) {
            $this->_credentialTreatment = '?';
        }

        $credentialExpression = new Zend_Db_Expr(
            '(CASE WHEN ' .
            $this->_zendDb->quoteInto(
                $this->_zendDb->quoteIdentifier($this->_credentialColumn, true)
                . ' = ' . $this->_credentialTreatment, $this->_credential
                )
            . ' THEN 1 ELSE 0 END) AS '
            . $this->_zendDb->quoteIdentifier(
                $this->_zendDb->foldCase('zend_auth_credential_match')
                )
            );

        // get select
        $dbSelect = clone $this->getDbSelect();
        $dbSelect->from($this->_tableName, array('*', $credentialExpression))
                 ->where($this->_zendDb->quoteIdentifier($this->_emailColumn, true)
                             . ' = ?', $this->_email)
                 ->where($this->_zendDb->quoteIdentifier($this->_accountIdColumn, true)
                             . ' = ?', $this->_accountId);
        return $dbSelect;
    }

    /**
     * _authenticateValidateResult() - This method attempts to validate that
     * the record in the resultset is indeed a record that matched the
     * identity provided to this adapter.
     *
     * @param array $resultIdentity
     * @return Zend_Auth_Result
     */
    protected function _authenticateValidateResult($resultIdentity)
    {
        $zendAuthCredentialMatchColumn = $this->_zendDb->foldCase('zend_auth_credential_match');

        if ($resultIdentity[$zendAuthCredentialMatchColumn] != '1') {
            $this->_authenticateResultInfo['code'] = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
            $this->_authenticateResultInfo['messages'][] = self::MSG_AUTH_FAILED;
            return $this->_authenticateCreateAuthResult();
        }

        unset($resultIdentity[$zendAuthCredentialMatchColumn]);
        $this->_resultRow = $resultIdentity;


        $this->_authenticateResultInfo['code'] = Zend_Auth_Result::SUCCESS;
        $this->_authenticateResultInfo['messages'][] = self::MSG_AUTH_SUCCESSFUL;
        return $this->_authenticateCreateAuthResult();
    }
}