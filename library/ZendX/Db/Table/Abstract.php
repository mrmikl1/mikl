<?php

class ZendX_Db_Table_Abstract extends Zend_Db_Table_Abstract
{
    /**
     * Specify if compound primary key doesn't include autoincrement field
     * @var string
     */
    protected $_autoincrementColName;

    /**
     * force insert() return unique row id within account
     *
     * @param array $data
     * @param string $resultColumn column that will be returned as a result
     */
    public function insert(array $data)
    {
        $id = parent::insert($data);

        if ($this->_autoincrementColName) {
            $autoincrementId = $id[$this->_primary[(int)$this->_identity]];

            $row = $this->fetchRow($this->_autoincrementColName . ' = ' . $autoincrementId);

            //unique id within account
            $id = $row->{$this->_primary[(int)$this->_identity]};
        }

        return $id;
    }
}
?>
